From Coq.ssr Require Export ssreflect.
From fri.prelude Require Export list compact.
From stdpp Require Export prelude mapset.
From stdpp Require Export propset natmap fin_sets sets fin_maps.
From stdpp Require propset.
From Coq Require Classical_Pred_Type.

Lemma subseteq_union_decompose:
  ∀ (A B C: natset), A ⊆ B ∪ C → ∃ B' C', B' ⊆  B ∧ C' ⊆ C ∧ A ≡ B' ∪ C'.
Proof.
  intros A B C Hsub_union.
  exists (A ∩ B), (A ∩ C).
  split_and!.
  - eapply intersection_subseteq_r.
  - eapply intersection_subseteq_r.
  - rewrite -(@intersection_union_l _ _ _ _ _).
    symmetry. apply subseteq_intersection_1; auto.
Qed.

Lemma subseteq_singleton (A: natset) n:
  A ⊆ {[n]} → A ≡ ∅ ∨ A ≡ {[n]}.
Proof.
  case (decide (n ∈ A)).
  - intros Hin Hsub. right. split; auto. apply elem_of_subseteq_singleton; auto.
  - intros Hnin Hsub. left. split; auto. 
    * intros HinA.
      exfalso. 
      assert (x = n). eapply (proj1 (elem_of_subseteq A {[n]}) Hsub) in HinA.
      apply elem_of_singleton_1 in HinA. auto.
      subst. eapply Hnin. eauto.
    * set_solver.
Qed.



Lemma natset_powerset_finite:
  ∀ (A: natset), set_finite {[ B | B ⊆ A ]}.
Proof.
  intros A.
  eapply (set_ind (λ A, set_finite {[ B | B ⊆ A]})).
  * intros A' A'' Hequiv. by rewrite leibniz_equiv_iff in Hequiv * => ->.
  * exists [∅]. intros B Hin. 
    cut (B = ∅); intros; subst; first left.
    apply mapset_eq.
    intros b. split. 
    ** assert (B ⊆ ∅) by set_solver. auto.
    ** set_solver.
  * intros x X Hnin IHfin.
    destruct IHfin as (l&Hinl).
    exists (map (union {[x]}) l ++ l).
    intros C. intros Hsub'.
    assert (C ⊆ {[x]} ∪ X) by set_solver.
    edestruct subseteq_union_decompose as (B'&C'&Hsub1&Hsub2&Hequiv); eauto.
    edestruct subseteq_singleton as [Hempty|Hsingle]; eauto.
    ** rewrite Hempty in Hequiv *. rewrite left_id; intros Hequiv.
       apply elem_of_app. right. eapply Hinl. 
         by rewrite -Hequiv in Hsub2 *. 
    ** apply elem_of_app. left.
       rewrite Hsingle in Hequiv *. rewrite leibniz_equiv_iff.
       intros; subst.
       eapply list.elem_of_map. eapply Hinl. eauto.
Qed.

 Lemma app_eq_disjoint_set:
   ∀ is1 is2 is1' is2' (tset: natset), 
     is1 ++ is2 = is1' ++ is2' →
     (∀ i, i ∈ is1 → i ∉ tset) →
     (∀ i, i ∈ is1' → i ∉ tset) →
     (∀ i, i ∈ is2 → i ∈ tset) →
     (∀ i, i ∈ is2' → i ∈ tset) →
     is1 = is1'.
 Proof.
   induction is1 as [| i is1].
   - induction is1' as [| i' is1']; auto.
     intros is2' tset Heq.
     destruct is2 as [| i is2]; first (simpl in *; congruence).
     simpl in *. assert (i = i') as -> by (inversion Heq; auto).
     intros ? Hnint Hint ?. 
     exfalso. 
     apply (Hnint i'); first (left).
     apply Hint. left.
   - induction is1' as [| i' is1']; auto.
     * intros is2' tset Heq.
       destruct is2' as [| i' is2']; first (simpl in *; congruence).
       simpl in *. assert (i = i') as -> by (inversion Heq; auto).
       intros Hnint ? ? Hint. 
       exfalso. 
       apply (Hnint i'); first (left).
       apply Hint. left.
     * intros is2' tset Heq Hnin Hnin' Hin Hin'.
       simpl in Heq. inversion Heq. 
       f_equal.
       edestruct (IHis1 is2 is1' is2'); eauto.
       ** intros. eapply Hnin. right. auto.
       ** intros. eapply Hnin'. right. auto.
 Qed.

  Lemma set_finite_flatten A (X: propset A) (Y: list (propset A)):
    (∀ X', X' ∈ Y → set_finite X') →
    (∀ x, x ∈ X → ∃ X', X' ∈ Y ∧ x ∈ X') →
    set_finite X.
  Proof.
    intros Hall_fin Hin.
    assert (∃ Xrest, set_finite Xrest 
                     ∧ (∀ x : A, x ∈ X → x ∈ Xrest 
                                         ∨ ∃ X' : propset A, X' ∈ Y ∧ x ∈ X' ∪ Xrest)) as Hin'.
    {
      exists ∅. 
      split. 
      - apply empty_finite.
      - intros x HinX. edestruct (Hin x) as (X'&?&?); eauto. 
        right. exists X'. rewrite right_id. auto.
    }
    clear Hin. revert X Hall_fin Hin'.
    induction Y as [| X' Y IHY].
    - intros X ? Hin.
      edestruct Hin as (Xrest&?&Hin').
      apply (set_finite_subseteq Xrest); auto.
      intros x HinX. 
      edestruct (Hin' x) as [Hin_rest|(X'&Hfalse&?)]; eauto. 
      set_solver.
    - intros X Hall_fin Hin.
      eapply IHY.
      * intros X'' HinY. eapply Hall_fin. right; auto.
      * destruct Hin as (Xrest&Hfin_rest&Hcover).
        exists (X' ∪ Xrest); split.
        ** apply union_finite; auto. apply Hall_fin. left.
        ** intros x HinX. edestruct Hcover as [Hin_rest|(X''&HinY&HinU)]; eauto.
           *** left. clear -Hin_rest. set_solver.
           *** inversion HinY; subst; auto.
               right. exists X''. split; auto. clear -HinU. set_solver.
  Qed.

Section compact_setoid.
  
  Import stdpp.propset Classical_Pred_Type.

  Definition set_finite_setoid `{Equiv A} {B} {H : ElemOf A B} (X : B) : Prop :=
    ∃ l : list A, ∀ x : A, x ∈ X → ∃ x' : A, x ≡ x' ∧ x' ∈ l.

  Instance set_finite_setoid_subseteq `{Equiv A} `{Equiv B} 
           {Equ: @Equivalence A equiv} `{ElemOf A B}:
    Proper (subseteq --> impl) (@set_finite_setoid A _ B _).
  Proof.
    intros X Y. rewrite /flip. intros Hsub (l&Hfin_in).
    exists l. intros x HinY.
    edestruct Hfin_in; eauto.
  Qed.

  Lemma set_finite_setoid_inj `{Equiv A, Equiv A'}
        {Equiv: Equivalence ((≡): relation A')}
        (X: propset A) (f: A → A') (f_proper: ∀ x y, x ∈ X → x ≡ y → f x ≡ f y)
        (f_inj: ∀ x y, x ∈ X → y ∈ X → f x ≡ f y → x ≡ y):
        set_finite_setoid (fmap f X) →
        set_finite_setoid X.
  Proof.
    intros (l&Hfin_in).
    assert (∀ x, x ∈ X → ∃ a, a ∈ l ∧ f x ≡ a) as Hcover.
    {
      intros. edestruct Hfin_in as (a&?&?).
      eapply elem_of_fmap_2; eauto. exists a; split; auto.
    }
    assert (∃ l : list A, (∀ a,  a ∈ l → a ∈ X) ∧
                  (∀ a',  a' ∈ fmap f X → ∃ a, a ∈ l ∧ a' ≡ f a)) as (lx&Hxs&Hall).
    {
      clear -f_proper f_inj Hfin_in Equiv.
      revert X f_inj Hfin_in f_proper.
      induction l; intros.
      - exists [].
        split; set_solver.
      - destruct (Classical_Prop.classic (∃ x, x ∈ X ∧ f x ≡ a)) as [(x&HinX&Heq)|Hno_match].
        *  edestruct (IHl {[ x' | x' ∈ X ∧ ¬ (x' ≡ x)]}) as (l'&Hinl'&HinXminus).
           {
             intros x' y' (Hin&_) (Hin'&_).
             eapply f_inj; eauto.
           }
           { 
             intros a' (x'&?&(?&Hneq))%elem_of_fmap_1. subst.
             edestruct (Hfin_in (f x')) as (a'&Hequiv&Hin'); eauto.
             { subst. apply elem_of_fmap_2; eauto. }
             inversion Hin' as [|Hin]; subst.
             ** exfalso. eapply Hneq. eapply f_inj; eauto.
              rewrite Heq Hequiv. eauto.
             ** eexists; split; eauto.
           }
           {
             intros x' y' (Hin&_) Hequiv.
             eapply f_proper; eauto.
           }
           exists (x :: l'). split; eauto.
           ** intros a'; inversion 1; subst; eauto.
              edestruct (Hinl' a') as (?&?); eauto.
           ** intros ? (x'&->&?)%elem_of_fmap_1.
              destruct (Classical_Prop.classic (f x' ≡ a)).
              *** exists x; split; eauto. left. 
                  etransitivity; eauto.
              *** edestruct (HinXminus (f x')) as (a''&?&?).
                  { 
                    eapply elem_of_fmap_2. econstructor; eauto.
                    intros Hequiv. apply (f_proper) in Hequiv; auto. rewrite Heq in Hequiv *. 
                    auto.
                  }
                  exists a''. split; auto; right; auto.
        * eapply (IHl X); eauto.
          { 
            intros a' HinfX.
            edestruct Hfin_in as (x'&?&Hin); eauto.
            eapply elem_of_fmap_1 in HinfX as (x&->&?).
            inversion Hin; subst. 
            ** exfalso. eapply Hno_match. exists x; split; eauto.
            ** eexists; split; eauto.
          }
    }
    exists lx. intros x HinX.
    edestruct (Hall (f x)) as (x0&?Hinlx&Hequiv); first eapply elem_of_fmap_2; eauto.
  Qed.

  Context `{Equiv A, !Equivalence (≡)}.
  Context (P: nat → A → Prop).
  Context (P_proper: ∀ n, Proper ((≡) ==> iff) (P n)).
  Context (P_le: ∀ x n n', P n x → n' ≤ n → P n' x).
  Implicit Type n: nat.
  Implicit Type x: A.
  
  Lemma not_all_all_stop_common_setoid:
    set_finite_setoid  {[ x | ∃ n, P n x ]} →
    (∀ x : A, ¬ (∀ n : nat, P n x)) →
    (∃ n, ∀ x, ∀ n', n ≤ n' → ¬ P n' x).
  Proof.
    intros Hfin Hnf.
    destruct Hfin as (l&Hin).
    cut (∃ n, ∀ x n', n ≤ n' → x ∈ l → ¬ P n' x).
    {
      intros (np&Hnp).
      exists np. intros x n' Hle HP.
      destruct (Hin x) as (x'&Hequiv&Hin'); first (econstructor; eauto).
      eapply Hnp; eauto. 
      rewrite -Hequiv; eauto.
    }
    clear -Hnf P_le.
    induction l as [| a l IHl]; intros.
    * exists O. intros x n' Hle Hin. inversion Hin.
    * edestruct IHl as (nl&Hnl).
      specialize (@not_all_all_stop A P P_le Hnf a). intros (na&Hna).
      exists (na + nl).
      intros x n' Hle Hin. 
      inversion Hin; subst. 
      ** eapply Hna; eauto; lia.
      ** eapply Hnl; eauto; lia.
  Qed.

  Lemma compact_forall_setoid:
    set_finite_setoid  {[ x | ∃ n, P n x ]} →
    (∀ n, ∃ x, P n x) →
    (∃ x, ∀ n, P n x).
  Proof. 
    intros Hfin Halln. apply not_all_not_ex.
    intro Hnf.
    edestruct (not_all_all_stop_common_setoid Hfin Hnf) as (np&Hnp).
    edestruct (Halln np) as (xnp&HP).
    eapply Hnp; eauto.
  Qed.
End compact_setoid.

Lemma union_finite_setoid `{Equiv A, Equivalence A} (X Y: propset A):
  set_finite_setoid X →
  set_finite_setoid Y →
  set_finite_setoid (X ∪ Y).
Proof.
  intros (l&Hinl) (k&Hink).
  exists (l ++ k). intros x [HinX|HinY].
  - edestruct Hinl as (x'&?&?); eauto.
    exists x'; split; eauto. apply elem_of_app; left; auto.
  - edestruct Hink as (x'&?&?); eauto.
    exists x'; split; eauto. apply elem_of_app; right; auto.
Qed.

Lemma set_finite_setoid_flatten `{Equiv A, Equivalence A (≡)} (X: propset A) (Y: list (propset A)):
  (∀ X', X' ∈ Y → set_finite_setoid X') →
  (∀ x, x ∈ X → ∃ X', X' ∈ Y ∧ x ∈ X') →
  set_finite_setoid X.
Proof.
  intros Hall_fin Hin.
  assert (∃ Xrest, set_finite_setoid Xrest 
                   ∧ (∀ x : A, x ∈ X → x ∈ Xrest ∨ ∃ X' : propset A, X' ∈ Y ∧ x ∈ X' ∪ Xrest)) as Hin'.
  {
    exists ∅. 
    split. 
    - exists []; intros x; set_solver+.
    - intros x HinX. edestruct (Hin x) as (X'&?&?); eauto. 
      right. exists X'. rewrite right_id. auto.
  }
  clear Hin. revert X Hall_fin Hin'.
  induction Y as [| X' Y IHY].
  - intros X ? Hin.
    edestruct Hin as (Xrest&?&Hin').
    apply (set_finite_setoid_subseteq Xrest); auto.
    intros x HinX. 
    edestruct (Hin' x) as [Hin_rest|(X'&Hfalse&?)]; eauto. 
    set_solver.
  - intros X Hall_fin Hin.
    eapply IHY.
    * intros X'' HinY. eapply Hall_fin. right; auto.
    * destruct Hin as (Xrest&Hfin_rest&Hcover).
      exists (X' ∪ Xrest); split.
      ** apply union_finite_setoid; auto. apply Hall_fin. left.
      ** intros x HinX. edestruct Hcover as [Hin_rest|(X''&HinY&HinU)]; eauto.
         *** left. clear -Hin_rest. set_solver.
         *** inversion HinY; subst; auto.
             right. exists X''. split; auto. clear -HinU. set_solver.
Qed.

Lemma set_finite_quotient (A A': Type) (f: A → A')
      (X: propset A) (Y: propset A') (f_in: ∀ x, x ∈ X → f x ∈ Y)
      (f_blocks: ∀ y, ∃ (X' : propset A), set_finite X' ∧ ∀ x, x ∈ X → f x = y → x ∈ X'):
      set_finite Y →
      set_finite X.
Proof.
  intros (l&Hfin_in).
  edestruct (exists_list_choice' l (λ y (X' : propset A), set_finite X' ∧
                                            (∀ x, x ∈ X → f x  = y → x ∈ X'))
                                 set_finite)
            as (l'&Hin_l'&Hin_l'2).
  { intros y Hin. edestruct (f_blocks y); eauto. }
  { simpl. intros ? ? (?&?); auto. }
  { exists ∅; apply empty_finite. }
  eapply (set_finite_flatten _ _ (map snd l')).
  - intros X'. 
    intros (ab&?&?)%elem_of_map_inv.
    eapply Hin_l'2. destruct ab. simpl in *. subst. eexists; eauto.
  - intros x HinX.
    specialize (f_in x HinX).
    specialize (Hfin_in _ f_in).
    edestruct (Hin_l' (f x)) as (X'&Hsnd&Hfin&Hin_block); eauto.
    exists X'. split.
    * replace X' with (snd (f x, X')); last auto. eapply list.elem_of_map.
      eauto.
    * eapply Hin_block; eauto.
Qed.

Lemma set_finite_setoid_quotient `{Equiv A, Equivalence A (≡), Equiv A', Equivalence A (≡)}
      (f: A → A') (X: propset A) (Y: propset A') (f_in: ∀ x, x ∈ X → f x ∈ Y)
      (f_blocks: ∀ y, ∃ X' : propset A, set_finite_setoid X'
                            ∧ ∀ x, x ∈ X → f x ≡ y → x ∈ X'):
      set_finite_setoid Y →
      set_finite_setoid X.
Proof.
  intros (l&Hfin_in).
  edestruct (exists_list_choice' l (λ y (X' : propset A), set_finite_setoid X'
                                            ∧ (∀ x, x ∈ X → f x ≡ y → x ∈ X')) set_finite_setoid)
            as (l'&Hin_l'&Hin_l'2).
  { intros y Hin. edestruct (f_blocks y); eauto. }
  { simpl. intros ? ? (?&?); auto. }
  { exists ∅. exists []. intros x. set_solver+. }
  eapply (set_finite_setoid_flatten _ (map snd l')).
  - intros X'. 
    intros (ab&?&?)%elem_of_map_inv.
    eapply Hin_l'2. destruct ab. simpl in *. subst. eexists; eauto.
  - intros x HinX.
    specialize (f_in x HinX).
    specialize (Hfin_in _ f_in).
    edestruct Hfin_in as (x'&?&?).
    edestruct (Hin_l' x') as (X'&Hsnd&Hfin&Hin_block); eauto.
    exists X'. split.
    * replace X' with (snd (x', X')); last auto. eapply list.elem_of_map.
      eauto.
    * eapply Hin_block; eauto.
Qed.

  Lemma set_finite_setoid_quotient_strong `{Equiv A, Equivalence A (≡), Equiv A', Equivalence A (≡)}
        (f: A → option A') (X: propset A) (Y: propset A') (f_in: ∀ x, x ∈ X → ∃ y, f x = Some y ∧ y ∈ Y)
        (f_blocks: ∀ y, ∃ (X' :propset A), set_finite_setoid X' ∧ ∀ x, x ∈ X → f x ≡ Some y → x ∈ X'):
        set_finite_setoid Y →
        set_finite_setoid X.
  Proof.
    intros (l&Hfin_in).
    edestruct (exists_list_choice' l (λ y (X' : propset A), set_finite_setoid X'
                                              ∧ (∀ x, x ∈ X → f x ≡ Some y → x ∈ X'))
                                   set_finite_setoid)
              as (l'&Hin_l'&Hin_l'2).
    { intros y Hin. edestruct (f_blocks y); eauto. }
    { simpl. intros ? ? (?&?); auto. }
    { exists ∅. exists []. intros x. set_solver+. }
    eapply (set_finite_setoid_flatten _ (map snd l')).
    - intros X'. 
      intros (ab&?&?)%elem_of_map_inv.
      eapply Hin_l'2. destruct ab. simpl in *. subst. eexists; eauto.
    - intros x HinX.
      specialize (f_in x HinX).
      destruct f_in  as (y & Heq & HyIn).
      specialize (Hfin_in _ HyIn).
      edestruct Hfin_in as (x'&?&?).
      edestruct (Hin_l' x') as (X'&Hsnd&Hfin&Hin_block); eauto.
      exists X'. split.
      * replace X' with (snd (x', X')); last auto. eapply list.elem_of_map.
        eauto.
      * eapply Hin_block; eauto.
        rewrite Heq. econstructor; eauto.
  Qed.


  Lemma set_finite_product {X Y} (A: propset X) (B: propset Y):
    set_finite A →
    set_finite B →
    set_finite {[ AB | fst AB ∈ A ∧ snd AB ∈ B]}.
  Proof.
    intros HfinA HfinB.
    apply (set_finite_quotient _ _ snd _ B).
    - intros x. inversion 1. auto.
    - intros y. exists ({[ ab | fst ab ∈ A ∧ snd ab = y]}).
      split.
      * destruct HfinA as (l&Hinl).
        exists (map (λ a, (a, y)) l).
        intros x. inversion 1. subst.
        destruct x; subst. simpl.
        eapply (list.elem_of_map (λ a:X, (a, y)) _ x).
        eauto.
      * intros x. inversion 1. subst. intros.
        subst. split; auto.
    - auto.
  Qed.

  (* I don't understand why this isn't being found. *)
  Existing Instance prod_equiv.
  Instance pair_equivalence `{Equiv A, @Equivalence A (≡), Equiv B, @Equivalence B (≡)} : 
    @Equivalence (A * B)%type (≡).
  Proof. eapply prod_relation_equiv; apply _. Qed.

  Lemma set_finite_setoid_product `{Equiv X, @Equivalence X (≡), Equiv Y, @Equivalence Y (≡)}
        (A: propset X) (B: propset Y):
    set_finite_setoid A →
    set_finite_setoid B →
    @set_finite_setoid (X*Y) prod_equiv _ _ {[ AB | fst (AB: X * Y) ∈ A ∧ snd AB ∈ B]}.
  Proof.
    intros HfinA HfinB.
    apply (set_finite_setoid_quotient snd _ B).
    - intros x. inversion 1. auto.
    - intros y. exists ({[ ab | fst ab ∈ A ∧ snd ab ≡ y]}).
      split.
      * destruct HfinA as (l&Hinl).
        exists (map (λ a, (a, y)) l).
        intros x. inversion 1. subst.
        destruct x; subst. simpl.
        edestruct (Hinl x) as (x'&Hequiv&Hin); eauto.
        exists (x', y). split; auto.
        ** econstructor; auto.
        ** eapply (list.elem_of_map (λ a:X, (a, y)) _ x'). eauto.
      * intros x. inversion 1. subst. intros.
        subst. split; auto.
    - auto.
  Qed.

  Lemma natmap_split_finite:
    ∀ (A: natset), set_finite {[ BC | fst BC ∪ snd BC = A]}.
  Proof.
    intros A.
    eapply (set_finite_subseteq {[ AB | fst AB ∈  {[ B | B ⊆ A ]} ∧ snd AB ∈ {[ B | B ⊆ A ]}]}).
    - intros BC. inversion 1; subst.
      split; set_solver+. 
    - apply set_finite_product; apply natset_powerset_finite.
  Qed.

  Lemma set_finite_square {X} (A: propset X):
    set_finite A →
    set_finite {[ AA | fst AA ∈ A ∧ snd AA ∈ A]}.
  Proof.
    intros; apply set_finite_product; auto.
  Qed.

  Lemma set_finite_product3 {X Y Z} (A: propset X) (B: propset Y) (C: propset Z):
    set_finite A → 
    set_finite B →
    set_finite C →
    set_finite {[ ABC | fst (fst ABC) ∈ A ∧ snd (fst ABC) ∈ B
                        ∧ snd (ABC) ∈ C]}.
  Proof.
    intros. 
    apply (set_finite_subseteq {[ ABC | (fst ABC) ∈ {[ AB | fst AB ∈ A ∧ snd AB ∈ B]} ∧
                                        (snd ABC) ∈ C]}).
    - intros ABC (Hin1&Hin2&Hin3).
      split; auto.
      split; auto.
    - apply set_finite_product; auto.
      apply set_finite_product; auto.
  Qed.

  Lemma set_finite_setoid_product3 
        `{Equiv X, @Equivalence X (≡), Equiv Y, @Equivalence Y (≡), Equiv Z, @Equivalence Z (≡)}
        (A: propset X) (B: propset Y) (C: propset Z):
    set_finite_setoid A → 
    set_finite_setoid B →
    set_finite_setoid C →
    @set_finite_setoid (X * Y * Z) _ _ _ {[ ABC | fst (fst ABC) ∈ A ∧ snd (fst ABC) ∈ B
                        ∧ snd (ABC) ∈ C]}.
  Proof.
    intros. 
    apply (set_finite_setoid_subseteq {[ ABC | (fst ABC) ∈ {[ AB | fst AB ∈ A ∧ snd AB ∈ B]} ∧
                                        (snd ABC) ∈ C]}).
    - intros ABC (Hin1&Hin2&Hin3).
      split; auto.
      split; auto.
    - apply set_finite_setoid_product; auto.
      apply set_finite_setoid_product; auto.
  Qed.

  Lemma prefix_set_finite A (l: list A):
    set_finite {[ l' | l' `prefix_of` l]}.
  Proof.
    induction l as [| a l].
    - exists [[]]. intros l. 
      inversion 1 as (lext&Heq). 
      symmetry in Heq.
      apply app_eq_nil in Heq as (?&?).
      subst. econstructor.
    - destruct IHl as (L&Hin_L).
      exists ([] :: (map (cons a) L)).
      intros l'.
      inversion 1 as (lext&Heq).
      destruct l' as [|a' l''].
      ** left. 
      ** rewrite -app_comm_cons in Heq.
         inversion Heq; subst.
         right. eapply list.elem_of_map.
         apply Hin_L. exists lext. eauto.
  Qed.
  

  Lemma set_finite_implies_setoid `{Equiv A, Equivalence A (≡)} (X: propset A):
    set_finite X → set_finite_setoid X.
  Proof.
    intros (l&Hin).
    exists l. intros x HinX. exists x. split; eauto.
  Qed.

  Lemma set_finite_list_cons {A: Type} (X: propset A) (Y: propset (list A)):
    set_finite X →
    set_finite Y →
    set_finite {[ xs | ∃ x xs', xs = x :: xs' ∧
                                x ∈ X ∧
                                xs' ∈  Y]}.
  Proof.
    intros HfinX HfinY.
    edestruct HfinX as [l  Hinl].
    destruct l as [| x l].
    - exists [].
      intros xs. set_unfold. intros (x&?&?&?&?). eapply Hinl; eauto.
    - eapply (set_finite_quotient _ _ (λ xs, match xs with
                                             | [] => (x, [])
                                             | x' :: tl => (x', tl)
                                             end)
                                  _
                                  {[ ab | fst ab ∈ X ∧ snd ab ∈ Y]});
      last eapply set_finite_product; eauto.
      * intros xs (?&?&?&?&?).
        subst. set_unfold; auto.
      * intros (x', xs').
        exists {[x' :: xs']}. split; first apply singleton_finite.
        intros xs'' (?&?&?&?&?). subst. inversion 1. set_unfold. auto.
  Qed.
  
  Definition setoid_closure `{Equiv A} (X: propset A) : propset A := {[ x | ∃ x', x' ∈ X ∧ x ≡ x']}.
  Lemma setoid_closure_pres_fin `{Equiv A, Equivalence A (≡)} (X: propset A):
    set_finite_setoid (setoid_closure X) ↔ set_finite_setoid X.
  Proof.
    split.
    - intros (l&Hinl).
      exists l. intros x HinX. 
      edestruct (Hinl x) as (x'&?&?). 
      { unfold setoid_closure; set_unfold. eexists; split; eauto. }
      exists x'; split; auto.
    - intros (l&Hinl).
      exists l. intros x HinX.
      inversion HinX as (x'&?&?).
      edestruct (Hinl x') as (x''&?&?). 
      { unfold setoid_closure; set_unfold; eauto. }
      exists x''. split; eauto. etransitivity; eauto.
  Qed.
      
  Existing Instances list_equiv list_equivalence.
  Lemma set_finite_setoid_list_cons `{Equiv A, Equivalence A (≡)} (X: propset A) (Y: propset (list A)):
    set_finite_setoid X →
    set_finite_setoid Y →
    set_finite_setoid {[ xs | ∃ x xs', (xs: list A) ≡ x :: xs' ∧
                                x ∈ X ∧
                                xs' ∈  Y]}.
  Proof.
    intros HfinX HfinY.
    edestruct HfinX as [l  Hinl].
    destruct l as [| x l].
    - exists [].
      intros xs. set_unfold. intros (x&?&?&?&?). exfalso; edestruct Hinl as (?&?&?); eauto.
    - eapply (set_finite_setoid_quotient (λ xs, match xs with
                                             | [] => (x, [])
                                             | x' :: tl => (x', tl)
                                             end)
                                  _
                                  (setoid_closure {[ ab | fst ab ∈ X ∧ snd ab ∈ Y]}));
      last eapply setoid_closure_pres_fin, set_finite_setoid_product; eauto.
      * intros xs (x0&x1&Hequiv&?&?).
        subst. set_unfold; auto.
        destruct xs as [|x0' x1']; inversion Hequiv; subst.
        exists (x0, x1); split_and!; eauto.
        econstructor; eauto.
      * intros (x', xs').
        exists (setoid_closure {[x' :: xs']}). split. 
        ** exists [x' :: xs'].  intros xs'' (xin&Hin&?). 
           apply elem_of_singleton_1 in Hin; subst. eexists; split; eauto. left.
        ** intros xs'' (x0&x1&Hequiv&?&?). inversion Hequiv; subst. inversion 1; subst. set_unfold.
           exists (x' :: xs'); split; auto. 
           econstructor; eauto.
  Qed.

  Lemma suffix_set_finite A (l: list A):
    set_finite {[ l' | l' `suffix_of` l]}.
  Proof.
    induction l as [| a l] using rev_ind.
    - exists [[]]. intros l. 
      inversion 1 as (lext&Heq). 
      symmetry in Heq.
      apply app_eq_nil in Heq as (?&?).
      subst. econstructor.
    - destruct IHl as (L&Hin_L).
      exists ([] :: (map (λ l', l' ++ [a]) L)).
      intros l'.
      inversion 1 as (lext&Heq).
      destruct l' as [|a' l''] using rev_ind.
      ** left. 
      ** rewrite app_assoc in Heq. apply app_inj_2 in Heq as (Heq1&Heq2); auto.
         inversion Heq2; subst.
         right. 
         assert (l'' ++ [a'] = (λ l', l' ++ [a']) l'') as Heq; auto.
         eapply (list.elem_of_map (λ l', l' ++ [a']) _ l'').
         eapply Hin_L.
         exists lext. eauto.
  Qed.


  Lemma set_finite_app1 {A: Type} (l1 l2: list A):
    set_finite {[ l | l1 ++ l = l2 ]}.
  Proof.
    eapply set_finite_subseteq; last (eapply (suffix_set_finite _ l2)).
    intros l. inversion 1. subst. eexists. eauto.
  Qed.
    
  Lemma set_finite_app2 {A: Type} (l1: list A) (Lext : propset (list A)):
    set_finite Lext →
    set_finite {[ l2 | ∃ l, l ∈ Lext ∧ l1 ++ l = l2 ]}.
  Proof.
    eapply (set_finite_quotient _ _ (λ l, skipn (length l1) l) _ Lext).
    - intros l. inversion 1 as (l' & Hin & Heq).
      subst. rewrite drop_app. auto.
    - intros l. exists {[l1 ++ l]}. split; eauto.
      * eapply singleton_finite.
      * intros l'. inversion 1 as (l'' & Hin & Heq).
        subst. rewrite drop_app. intros ->. set_solver+.
  Qed.

  Lemma set_finite_powerlist {A: Type} (L: propset A) (n: nat):
    set_finite L →
    set_finite {[ l' | length l' = n ∧ ∀ a, a ∈ l' → a ∈  L]}.
  Proof.
    induction n.
    - simpl. exists [[]]. intros l'. set_unfold. destruct l' as [| ? ?]; simpl; intros; last lia.
      left. auto.
    - intros. eapply (set_finite_subseteq
                {[ l | ∃ a l', l = a :: l' ∧ a ∈ L 
                             ∧ l' ∈ {[ l' | length l' = n ∧ (∀ a: A, a ∈ l' → a ∈ L)]}]}).
      
      * set_unfold. intros l (Hlen&Hcontents).
        destruct l as [| a l'] ; simpl in *; first lia.
        exists a, l'.
        split_and!; auto.
        ** eapply Hcontents. left; eauto.
        ** intros. eapply Hcontents; right; auto.
      * eapply set_finite_list_cons; eauto.
  Qed.

  Lemma set_finite_powerlist_upto {A: Type} (L: propset A) (n: nat):
    set_finite L →
    set_finite {[ l' | length l' ≤ n ∧ ∀ a, a ∈ l' → a ∈  L]}.
  Proof.
    induction n.
    - simpl. exists [[]]. intros l'. set_unfold. destruct l' as [| ? ?]; simpl; intros; last lia.
      left. auto.
    - intros. eapply (set_finite_subseteq
                ({[[]]} ∪ {[ l | ∃ a l', l = a :: l' ∧ a ∈ L 
                             ∧ l' ∈ {[ l' | length l' ≤ n ∧ (∀ a: A, a ∈ l' → a ∈ L)]}]})).
      * set_unfold. intros l (Hlen&Hcontents).
        destruct l as [| a l']; first left; auto.
        right.
        exists a, l'.
        split_and!; auto.
        ** eapply Hcontents. left; eauto.
        ** simpl in *. lia.
        ** intros. eapply Hcontents; right; auto.
      * eapply union_finite.
        ** apply singleton_finite.
        ** eapply set_finite_list_cons; eauto.
  Qed.
  
  Lemma set_finite_permutation {A: Type} (l: list A):
    set_finite {[ l' | Permutation l' l]}.
  Proof.
    eapply (set_finite_subseteq ({[ l' | length l' = length l ∧ ∀ a, a ∈ l' → a ∈ {[ a | a ∈  l]}]})).
    - set_unfold. intros l' Hperm; split.
      ** apply Permutation_length; auto.
      ** intros x. rewrite Hperm. auto.
    - apply set_finite_powerlist. exists l. eauto.
  Qed.

  Lemma set_finite_setoid_option `{Equiv A, Equivalence A (≡)} (X: propset A):
          @set_finite_setoid A _ _ _ X →
          @set_finite_setoid (option A) _ _ _ 
                             {[ mx | match mx with | None => True | Some x => x ∈ X end ]}.
  Proof.
    intros Hfin.
    eapply (set_finite_setoid_subseteq  ({[None]} ∪
       {[ mx | match mx with | None => False | Some x => x ∈ X end]})). 
    - set_unfold. intros [x|]; auto.
    - apply union_finite_setoid.
      * eapply set_finite_implies_setoid, singleton_finite.
      * eapply (set_finite_setoid_quotient_strong id _ X).
        ** intros [x|];  set_unfold.
           *** intros; exists x; split; auto.
           *** intros; exfalso; auto.
        ** intros x. exists (setoid_closure {[Some x]}).
           split; first eapply setoid_closure_pres_fin, set_finite_implies_setoid, singleton_finite.
           set_unfold. intros [x'|] Hin Hequiv.
           *** eexists; split; auto.
           *** exfalso; auto.
        ** auto.
  Qed.
