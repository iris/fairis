From stdpp Require Import propset.
From Coq Require Import Classical_Pred_Type.

Section compact.
  
  Context {A: Type}.
  Context (P: nat → A → Prop).
  Context (P_le: ∀ x n n', P n x → n' ≤ n → P n' x).
  Implicit Type n: nat.
  Implicit Type x: A.
  
  Lemma not_all_all_stop:
    (∀ x : A, ¬ (∀ n : nat, P n x)) →
    (∀ x, ∃ n, ∀ n', n ≤ n' → ¬ P n' x).
  Proof.
    intros Hnf x. specialize (Hnf x). 
    apply not_all_not_ex.
    intro Hnf'.
    eapply all_not_not_ex in Hnf'.
    eapply Hnf'.
    eapply not_all_ex_not in Hnf.
    destruct Hnf as (nb&Hnb).
    exists nb.
    intros n' Hgt. 
    intro Hn'. eapply Hnb. eapply P_le; eauto.
  Qed.

  Lemma not_all_all_stop_common:
    set_finite  {[ x | ∃ n, P n x ]} →
    (∀ x : A, ¬ (∀ n : nat, P n x)) →
    (∃ n, ∀ x, ∀ n', n ≤ n' → ¬ P n' x).
  Proof.
    intros Hfin Hnf.
    destruct Hfin as (l&Hin).
    cut (∃ n, ∀ x n', n ≤ n' → x ∈ l → ¬ P n' x).
    {
      intros (np&Hnp).
      exists np. intros x n' Hle HP.
      eapply Hnp; eauto. eapply Hin. econstructor; eauto.
    }
    
    clear -Hnf P_le.
    induction l as [| a l IHl]; intros.
    * exists O. intros x n' Hle Hin. inversion Hin.
    * edestruct IHl as (nl&Hnl).
      eapply not_all_all_stop with a in Hnf.
      edestruct Hnf as (na&Hna).
      exists (na + nl).
      intros x n' Hle Hin. 
      inversion Hin; subst. 
      ** eapply Hna; eauto. lia.
      ** eapply Hnl; eauto. lia.
  Qed.

  Lemma compact_forall:
    set_finite  {[ x | ∃ n, P n x ]} →
    (∀ n, ∃ x, P n x) →
    (∃ x, ∀ n, P n x).
  Proof. 
    intros Hfin Halln. apply not_all_not_ex.
    intro Hnf.
    edestruct (not_all_all_stop_common Hfin Hnf) as (np&Hnp).
    edestruct (Halln np) as (xnp&HP).
    eapply Hnp; eauto.
  Qed.
End compact.