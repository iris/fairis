(* Copyright (c) 2012-2015, Robbert Krebbers. *)
(* This file is distributed under the terms of the BSD license. *)
(** This file collects general purpose definitions and theorems on lists that
are not in the Coq standard library. *)
From Coq.ssr Require Import ssreflect.
From Coq Require Export Permutation.
From stdpp Require Export list propset.

Inductive Forall4 {A B C D} (P : A → B → C → D → Prop) :
     list A → list B → list C → list D → Prop :=
  | Forall4_nil : Forall4 P [] [] [] []
  | Forall4_cons x y z a l k k' k'' :
     P x y z a → Forall4 P l k k' k'' → 
     Forall4 P (x :: l) (y :: k) (z :: k') (a :: k'').

Inductive Forall5 {A B C D E} (P : A → B → C → D → E → Prop) :
     list A → list B → list C → list D → list E → Prop :=
  | Forall5_nil : Forall5 P [] [] [] [] []
  | Forall5_cons x y z a b l k k' k'' k''' :
     P x y z a b → Forall5 P l k k' k'' k''' → 
     Forall5 P (x :: l) (y :: k) (z :: k') (a :: k'') (b :: k''').

Section Forall4.
  Context {A B C D} (P : A → B → C → D → Prop).
  Hint Extern 0 (Forall4 _ _ _ _ _) => constructor.
  Lemma Forall4_app l1 l2 k1 k2 k1' k2' j1 j2:
    Forall4 P l1 k1 k1' j1 → Forall4 P l2 k2 k2' j2 →
    Forall4 P (l1 ++ l2) (k1 ++ k2) (k1' ++ k2') (j1 ++ j2).
  Proof. induction 1; simpl; auto. Qed.
  Lemma Forall4_cons_inv_l x l k k' j :
    Forall4 P (x :: l) k k' j → ∃ y k2 z k2' a j2,
      k = y :: k2 ∧ k' = z :: k2' ∧ j = a :: j2 ∧
      P x y z a ∧ Forall4 P l k2 k2' j2.
  Proof. inversion_clear 1; naive_solver. Qed.
  Lemma Forall4_app_inv_l l1 l2 k k' j:
    Forall4 P (l1 ++ l2) k k' j → ∃ k1 k2 k1' k2' j1 j2,
     k = k1 ++ k2 ∧ k' = k1' ++ k2' ∧ j = j1 ++ j2 ∧
     Forall4 P l1 k1 k1' j1 ∧ Forall4 P l2 k2 k2' j2.
  Proof.
    revert k k' j. induction l1 as [|x l1 IH]; simpl; inversion_clear 1.
    - by repeat eexists; eauto.
    - by repeat eexists; eauto.
    - edestruct IH as (?&?&?&?&?&?&?&?&?&?); eauto; naive_solver.
  Qed.
  (*
  Lemma Forall4_cons_inv_m l y k k' j j':
    Forall4 P l (y :: k) k' j j' → ∃ x l2 z k2',
      l = x :: l2 ∧ k' = z :: k2' ∧ P x y z ∧ Forall4 P l2 k k2'.
  Proof. inversion_clear 1; naive_solver. Qed.
  Lemma Forall4_app_inv_m l k1 k2 k' :
    Forall4 P l (k1 ++ k2) k' → ∃ l1 l2 k1' k2',
     l = l1 ++ l2 ∧ k' = k1' ++ k2' ∧ Forall4 P l1 k1 k1' ∧ Forall4 P l2 k2 k2'.
  Proof.
    revert l k'. induction k1 as [|x k1 IH]; simpl; inversion_clear 1.
    - by repeat eexists; eauto.
    - by repeat eexists; eauto.
    - edestruct IH as (?&?&?&?&?&?&?&?); eauto; naive_solver.
  Qed.
  Lemma Forall4_cons_inv_r l k z k' :
    Forall4 P l k (z :: k') → ∃ x l2 y k2,
      l = x :: l2 ∧ k = y :: k2 ∧ P x y z ∧ Forall4 P l2 k2 k'.
  Proof. inversion_clear 1; naive_solver. Qed.
  Lemma Forall4_app_inv_r l k k1' k2' :
    Forall4 P l k (k1' ++ k2') → ∃ l1 l2 k1 k2,
      l = l1 ++ l2 ∧ k = k1 ++ k2 ∧ Forall4 P l1 k1 k1' ∧ Forall4 P l2 k2 k2'.
  Proof.
    revert l k. induction k1' as [|x k1' IH]; simpl; inversion_clear 1.
    - by repeat eexists; eauto.
    - by repeat eexists; eauto.
    - edestruct IH as (?&?&?&?&?&?&?&?); eauto; naive_solver.
  Qed.
   *)
  Lemma Forall4_impl (Q : A → B → C → D → Prop) l k k' j:
    Forall4 P l k k' j → (∀ x y z a, P x y z a → Q x y z a) → Forall4 Q l k k' j.
  Proof. intros Hl ?; induction Hl; auto. Defined.
  Lemma Forall4_length_lm l k k' j : Forall4 P l k k' j→ length l = length k.
  Proof. by induction 1; f_equal/=. Qed.
  Lemma Forall4_length_lr l k k' j : Forall4 P l k k' j → length l = length k'.
  Proof. by induction 1; f_equal/=. Qed.
  Lemma Forall4_length_lrr l k k' j : Forall4 P l k k' j → length l = length j.
  Proof. by induction 1; f_equal/=. Qed.
  (*
  Lemma Forall4_lookup_lmr l k k' i x y z :
    Forall4 P l k k' →
    l !! i = Some x → k !! i = Some y → k' !! i = Some z → P x y z.
  Proof.
    intros H. revert i. induction H; intros [|?] ???; simplify_eq/=; eauto.
  Qed.
   *)
  Lemma Forall4_lookup_l l k k' j i x :
    Forall4 P l k k' j → l !! i = Some x →
    ∃ y z a, k !! i = Some y ∧ k' !! i = Some z ∧ j !! i = Some a ∧
               P x y z a.
  Proof.
    intros H. revert i. induction H; intros [|?] ?; simplify_eq/=; eauto 15.
  Qed.
  (*
  Lemma Forall4_lookup_m l k k' i y :
    Forall4 P l k k' → k !! i = Some y →
    ∃ x z, l !! i = Some x ∧ k' !! i = Some z ∧ P x y z.
  Proof.
    intros H. revert i. induction H; intros [|?] ?; simplify_eq/=; eauto.
  Qed.
  Lemma Forall4_lookup_r l k k' i z :
    Forall4 P l k k' → k' !! i = Some z →
    ∃ x y, l !! i = Some x ∧ k !! i = Some y ∧ P x y z.
  Proof.
    intros H. revert i. induction H; intros [|?] ?; simplify_eq/=; eauto.
  Qed.
  Lemma Forall4_alter_lm f g l k k' i :
    Forall4 P l k k' →
    (∀ x y z, l !! i = Some x → k !! i = Some y → k' !! i = Some z →
      P x y z → P (f x) (g y) z) →
    Forall4 P (alter f i l) (alter g i k) k'.
  Proof. intros Hl. revert i. induction Hl; intros [|]; auto. Qed.
   *)

Lemma Forall4_Forall_extra_impl:
  ∀ (A B C D : Type) (P: A → B → C → D → Prop) Q R S T
    (U : D → Prop)
    (l : list A) (k : list B) (k' : list C) (j : list D),
    Forall4 P l k k' j →
    Forall Q l →
    Forall R k  →
    Forall S k'  →
    Forall T j  →
    (∀ (x : A) (y : B) (z : C) (a : D), P x y z a → Q x → R y → S z → T a → U a) →
    Forall U j.
Proof.
  induction 1; auto.
  do 4 inversion 1. subst.
  intros Himpl.
  econstructor. eapply Himpl; eauto. eapply IHForall4; eauto.
Qed.
End Forall4.

Section Forall5.
  Context {A B C D E} (P : A → B → C → D → E → Prop).
  Hint Extern 0 (Forall5 _ _ _ _ _ _) => constructor.
  Lemma Forall5_app l1 l2 k1 k2 k1' k2' j1 j2 j1' j2':
    Forall5 P l1 k1 k1' j1 j1' → Forall5 P l2 k2 k2' j2 j2' →
    Forall5 P (l1 ++ l2) (k1 ++ k2) (k1' ++ k2') (j1 ++ j2) (j1' ++ j2').
  Proof. induction 1; simpl; auto. Qed.
  Lemma Forall5_cons_inv_l x l k k' j j' :
    Forall5 P (x :: l) k k' j j' → ∃ y k2 z k2' a j2  b j2',
      k = y :: k2 ∧ k' = z :: k2' ∧ j = a :: j2 ∧
      j' = b :: j2' ∧ P x y z a b ∧ Forall5 P l k2 k2' j2 j2'.
  Proof. inversion_clear 1; naive_solver. Qed.
  Lemma Forall5_app_inv_l l1 l2 k k' j j' :
    Forall5 P (l1 ++ l2) k k' j j' → ∃ k1 k2 k1' k2' j1 j2 j1' j2',
     k = k1 ++ k2 ∧ k' = k1' ++ k2' ∧ j = j1 ++ j2 ∧ j' = j1' ++ j2' ∧ 
     Forall5 P l1 k1 k1' j1 j1' ∧ Forall5 P l2 k2 k2' j2 j2'.
  Proof.
    revert k k' j j'. induction l1 as [|x l1 IH]; simpl; inversion_clear 1.
    - by repeat eexists; eauto.
    - by repeat eexists; eauto.
    - edestruct IH as (?&?&?&?&?&?&?&?&?&?&?&?); eauto; naive_solver.
  Qed.
  (*
  Lemma Forall5_cons_inv_m l y k k' j j':
    Forall5 P l (y :: k) k' j j' → ∃ x l2 z k2',
      l = x :: l2 ∧ k' = z :: k2' ∧ P x y z ∧ Forall5 P l2 k k2'.
  Proof. inversion_clear 1; naive_solver. Qed.
  Lemma Forall5_app_inv_m l k1 k2 k' :
    Forall5 P l (k1 ++ k2) k' → ∃ l1 l2 k1' k2',
     l = l1 ++ l2 ∧ k' = k1' ++ k2' ∧ Forall5 P l1 k1 k1' ∧ Forall5 P l2 k2 k2'.
  Proof.
    revert l k'. induction k1 as [|x k1 IH]; simpl; inversion_clear 1.
    - by repeat eexists; eauto.
    - by repeat eexists; eauto.
    - edestruct IH as (?&?&?&?&?&?&?&?); eauto; naive_solver.
  Qed.
  Lemma Forall5_cons_inv_r l k z k' :
    Forall5 P l k (z :: k') → ∃ x l2 y k2,
      l = x :: l2 ∧ k = y :: k2 ∧ P x y z ∧ Forall5 P l2 k2 k'.
  Proof. inversion_clear 1; naive_solver. Qed.
  Lemma Forall5_app_inv_r l k k1' k2' :
    Forall5 P l k (k1' ++ k2') → ∃ l1 l2 k1 k2,
      l = l1 ++ l2 ∧ k = k1 ++ k2 ∧ Forall5 P l1 k1 k1' ∧ Forall5 P l2 k2 k2'.
  Proof.
    revert l k. induction k1' as [|x k1' IH]; simpl; inversion_clear 1.
    - by repeat eexists; eauto.
    - by repeat eexists; eauto.
    - edestruct IH as (?&?&?&?&?&?&?&?); eauto; naive_solver.
  Qed.
   *)
  Lemma Forall5_impl (Q : A → B → C → D → E→ Prop) l k k' j j':
    Forall5 P l k k' j j' → (∀ x y z a b, P x y z a b → Q x y z a b) → Forall5 Q l k k' j j'.
  Proof. intros Hl ?; induction Hl; auto. Defined.
  Lemma Forall5_length_lm l k k' j j' : Forall5 P l k k' j j' → length l = length k.
  Proof. by induction 1; f_equal/=. Qed.
  Lemma Forall5_length_lr l k k' j j' : Forall5 P l k k' j j' → length l = length k'.
  Proof. by induction 1; f_equal/=. Qed.
  Lemma Forall5_length_lrr l k k' j j' : Forall5 P l k k' j j' → length l = length j.
  Proof. by induction 1; f_equal/=. Qed.
  Lemma Forall5_length_lrrr l k k' j j' : Forall5 P l k k' j j' → length l = length j'.
  Proof. by induction 1; f_equal/=. Qed.
  (*
  Lemma Forall5_lookup_lmr l k k' i x y z :
    Forall5 P l k k' →
    l !! i = Some x → k !! i = Some y → k' !! i = Some z → P x y z.
  Proof.
    intros H. revert i. induction H; intros [|?] ???; simplify_eq/=; eauto.
  Qed.
   *)
  Lemma Forall5_lookup_l l k k' j j' i x :
    Forall5 P l k k' j j' → l !! i = Some x →
    ∃ y z a b, k !! i = Some y ∧ k' !! i = Some z ∧ j !! i = Some a ∧
               j' !! i = Some b ∧ P x y z a b.
  Proof.
    intros H. revert i. induction H; intros [|?] ?; simplify_eq/=; eauto 15.
  Qed.
  (*
  Lemma Forall5_lookup_m l k k' i y :
    Forall5 P l k k' → k !! i = Some y →
    ∃ x z, l !! i = Some x ∧ k' !! i = Some z ∧ P x y z.
  Proof.
    intros H. revert i. induction H; intros [|?] ?; simplify_eq/=; eauto.
  Qed.
  Lemma Forall5_lookup_r l k k' i z :
    Forall5 P l k k' → k' !! i = Some z →
    ∃ x y, l !! i = Some x ∧ k !! i = Some y ∧ P x y z.
  Proof.
    intros H. revert i. induction H; intros [|?] ?; simplify_eq/=; eauto.
  Qed.
  Lemma Forall5_alter_lm f g l k k' i :
    Forall5 P l k k' →
    (∀ x y z, l !! i = Some x → k !! i = Some y → k' !! i = Some z →
      P x y z → P (f x) (g y) z) →
    Forall5 P (alter f i l) (alter g i k) k'.
  Proof. intros Hl. revert i. induction Hl; intros [|]; auto. Qed.
   *)
End Forall5.


Lemma map_app_inv {A B: Type} (f: A → B) (l: list A) (l1 l2: list  B):
  map f l = l1 ++ l2 → ∃ l1' l2', l = l1' ++ l2' ∧ map f l1' = l1 ∧ map f l2' = l2.
Proof.
  revert l1 l2. induction l; simpl.
  - intros l1 l2 Heq. symmetry in Heq. apply app_eq_nil in Heq as (->&->).
    exists [], []; split; auto.
  - intros l1 l2 Heq; destruct l1; simpl in *.
    * exists [], (a :: l). split_and!; auto.
    * edestruct (IHl l1 l2) as (l1'&l2'&Heqapp&?&?).
      { inversion Heq; auto. }
      rewrite Heqapp.
      exists (a :: l1'), l2'; split_and!; auto.
      simpl. inversion Heq; subst. auto.
Qed.

Lemma map_cons_inv {A B: Type} (f: A → B) (l: list A) (b: B) (l': list  B):
  map f l = b :: l' → ∃ a l0, l = a :: l0 ∧ f a = b ∧ map f l0 = l'.
Proof.
  destruct l as [| a l0]. 
  - simpl; congruence.
  - simpl. intros Heq. exists a, l0. 
    split_and!; inversion Heq; auto.
Qed.

Lemma last_non_empty: ∀ {A: Type} (l: list A), l ≠ [] → last l ≠ None.
Proof.
  induction l as [| x ?] using rev_ind; rewrite ?last_snoc; congruence.
Qed.
  
Instance map_proper `{Equiv A, !Equivalence (≡), Equiv B, !Equivalence (≡)}:
  Proper (((≡) ==> (≡)) ==> (≡) ==> (≡)) (@map A B).
Proof.
  intros f f' Heqf l l' Heql.
  induction Heql; simpl; auto.
  constructor; eauto.
Qed.

Lemma map_proper' `{Equiv A, !Equivalence (≡), Equiv B, !Equivalence (≡)}:
  ∀ (f: A → B) l l', l ≡ l' → (∀ x y, x ∈ l → y ∈ l' → x ≡ y → f x ≡ f y) →  map f l ≡ map f l'.
Proof.
  intros f l l' Heql Heqf.
  induction Heql; simpl; auto.
  constructor; eauto.
  - eapply Heqf; try left. eauto.
  - eapply IHHeql. intros x' y' Hinl Hink Hequiv. eapply Heqf; try (right; auto).
    eauto.
Qed.

Lemma permutation_hd {A: Type} (l: list A) x:
  x ∈ l → ∃ l', Permutation (x :: l') l.
Proof.
  induction l.
  - inversion 1.
  - inversion 1 as [| Hin_rec]; subst. 
    * exists l; eauto.
    * edestruct IHl as (l'&HPerm); auto.
      exists (a :: l').
      eapply perm_trans.
      ** eapply perm_swap.
      ** econstructor; eauto.
Qed.

Lemma elem_of_equiv_list `{Equiv A} a (l l': list A):
  l ≡ l' → a ∈ l → ∃ a', a' ∈ l' ∧ a ≡ a'.
Proof.
  revert a. induction 1.
  - set_solver+.
  - intros [Heqx|Hinl]%elem_of_cons.
    * subst. eexists; split; first left; eauto.
    * edestruct IHlist_equiv as (a'&?&?); eauto.
      exists a'. split; first right; eauto.
Qed.
  

Lemma elem_of_In {A: Type}: ∀ (l: list A) a, a ∈ l ↔ In a l.
Proof.
  induction l as [| a' l]; first set_solver.
  split.
  - intros [->| Hin]%elem_of_cons.
    * left. auto.
    * right. eapply IHl; eauto.
  - simpl. intros [->|Hin].
    * left. 
    * right. eapply IHl; eauto.
Qed.

Lemma elem_of_map  {A B: Type} (f: A → B) (l: list A) x:
  x ∈ l → f x ∈ map f l.
Proof.
  induction l.
  - simpl. set_solver+.
  - simpl. inversion 1; subst.
    * left.
    * right. eauto.
Qed.

Lemma elem_of_map_inv  {A B: Type} (f: A → B) (l: list A) x:
  x ∈ map f l → ∃ y, y ∈ l ∧ f y = x.
Proof.
  induction l.
  - simpl. set_solver+.
  - simpl. inversion 1; subst.
    * eexists. split; eauto. left.
    * edestruct IHl as (?&?&?); eauto. eexists; split; eauto. right. eauto. 
Qed.

  
Lemma exists_list_choice {A B} (l: list A) (P: A → B → Prop):
  (∀ a, a ∈ l → ∃ b, P a b) →
  ∃ (l': list (A*B)), ∀ a, a ∈ l → ∃ b, (a, b) ∈ l' ∧ P a b.
Proof.
  revert P.
  induction l.  intros.
  - exists []. intros a. inversion 1.
  - intros P Hin. 
    edestruct IHl as (l'&Hinl').
    { intros a' Hin'. eapply Hin. right. eauto. }
    edestruct (Hin a) as (b&?); first (left).
    exists ((a, b) :: l').
    intros a'. inversion 1; subst; auto.
    * eexists. split; first left; eauto. 
    * edestruct Hinl' as (?&?&?); eauto. 
      eexists. split; first right; eauto.
Qed.

Lemma exists_list_choice' {A B} (l: list A) (P: A → B → Prop) (Q: B → Prop):
  (∀ a, a ∈ l → ∃ b, P a b) →
  (∀ a b,  P a b → Q b) →
  (∃ b, Q b) →
  ∃ (l': list (A*B)), (∀ a, a ∈ l → ∃ b, (a, b) ∈ l' ∧ P a b)
   ∧ ∀ b, (∃ a, (a, b) ∈ l') → Q b.
Proof.
  induction l.  intros.
  - exists []. split.
    * intros a. inversion 1.
    * intros b (a&Hin). inversion Hin.
  - intros Hin HPQ Hsomeb. 
    edestruct IHl as (l'&Hinl'&Hinl'2); eauto.
    { intros a' Hin'. eapply Hin. right. eauto. }
    edestruct (Hin a) as (b&?); first (left).
    exists ((a, b) :: l'); split.
    * intros a'. inversion 1; subst; auto.
      ** eexists. split; first left; eauto. 
      ** edestruct Hinl' as (?&?&?); eauto. 
         eexists. split; first right; eauto.
    * intros b' (a'&Hin').
      inversion Hin'; subst; eauto.
Qed.

Lemma map_compose {A B C: Type} (f: A → B) (g: B → C) (l: list A):
  map (g ∘ f) l = map g (map f l).
Proof.
  induction l; auto.
  simpl. rewrite IHl; auto.
Qed.
  