From fri.algebra Require Export upred agree.
From fri.program_logic Require Export ghost_ownership.
From iris.proofmode Require Import tactics.
Import uPred.

Class savedPropG (Λ : language) (Σ : gFunctors) (F : oFunctor) :=
  saved_prop_inG :> inG Λ Σ (gmapUR gname (agreeR (laterO (F (iPreProp Λ (globalF Σ)) _)))).
Definition savedPropGF (F : oFunctor) : gFunctor :=
  GFunctor (gmapURF gname (agreeRF (▶ F))).
Instance inGF_savedPropG  `{inGF Λ Σ (savedPropGF F)} : savedPropG Λ Σ F.
Proof. apply: inGF_inG. Qed.

Definition saved_prop_own `{savedPropG Λ Σ F}
    (γ : gname) (x : F (iPropG Λ Σ) _) : iPropG Λ Σ :=
  own γ (to_agree $ Next (oFunctor_map F (iProp_fold, iProp_unfold) x)).
Typeclasses Opaque saved_prop_own.
Instance: Params (@saved_prop_own) 4.

Section saved_prop.
  Context `{savedPropG Λ Σ F}.
  Implicit Types x y : F (iPropG Λ Σ) _.
  Implicit Types γ : gname.

  Global Instance saved_prop_persistent γ x : Persistent (saved_prop_own γ x).
  Proof. rewrite /saved_prop_own. apply _. Qed.

  Lemma saved_prop_alloc_strong E x (G : gset gname) :
    emp ={E}=> (∃ γ, ■ (γ ∉ G) ∧ saved_prop_own γ x).
  Proof. by apply own_alloc_strong. Qed.

  Lemma saved_prop_alloc E x : emp ={E}=> (∃ γ, saved_prop_own γ x).
  Proof. by apply own_alloc. Qed.

  Lemma saved_prop_agree γ x y :
    saved_prop_own γ x ★ saved_prop_own γ y ⊢ ⧆▷(x ≡ y).
  Proof.
    rewrite -own_op own_valid agree_validI agree_equivI later_equivI.
    set (G1 := oFunctor_map F (iProp_fold, iProp_unfold)).
    set (G2 := oFunctor_map F (@iProp_unfold Λ (globalF Σ),
                               @iProp_fold Λ (globalF Σ))).
    assert (∀ z, G2 (G1 z) ≡ z) as help.
    { intros z. rewrite /G1 /G2 -oFunctor_compose -{2}[z]oFunctor_id.
      apply (ne_proper (oFunctor_map F)); split=>?; apply iProp_fold_unfold. }
    rewrite -{2}[x]help -{2}[y]help. apply bi.affinely_mono. apply later_mono.
    iIntros "Heq". by iRewrite "Heq".
  Qed.
End saved_prop.
