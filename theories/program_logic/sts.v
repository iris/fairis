From fri.algebra Require Export sts base_logic.
From fri.program_logic Require Export invariants ghost_ownership weakestpre.
From iris.proofmode Require Import tactics coq_tactics reduction.
Import uPred.

(** The CMRA we need. *)
Class stsG Λ Σ (sts : stsT) := StsG {
  sts_inG :> inG Λ Σ (gmapUR gname (stsR sts));
  sts_inhabited :> Inhabited (sts.state sts);
}.
Coercion sts_inG : stsG >-> inG.
(** The Functor we need. *)
Definition stsGF (sts : stsT) : gFunctor := GFunctor (gmapURF gname (constRF (stsR sts))).
(* Show and register that they match. *)
Instance inGF_stsG sts `{inGF Λ Σ (stsGF sts)}
  `{Inhabited (sts.state sts)} : stsG Λ Σ sts.
Proof. split; try apply _. apply: inGF_inG. Qed.

Section definitions.
  Context `{i : stsG Λ Σ sts} (γ : gname).
  Definition sts_ownS (S : sts.states sts) (T : sts.tokens sts) : iPropG Λ Σ:=
    own γ (sts_frag S T).
  Definition sts_own (s : sts.state sts) (T : sts.tokens sts) : iPropG Λ Σ :=
    own γ (sts_frag_up s T).
  Definition sts_inv (φ : sts.state sts → iPropG Λ Σ) : iPropG Λ Σ :=
    (∃ s, own γ (sts_auth s ∅) ★ ⧆φ s)%I.
  Definition sts_ctx (N : namespace) (φ: sts.state sts → iPropG Λ Σ) : iPropG Λ Σ :=
    inv N (sts_inv φ).

  Global Instance sts_inv_ne n :
    Proper (pointwise_relation _ (dist n) ==> dist n) sts_inv.
  Proof. solve_proper. Qed.
  Global Instance sts_inv_proper :
    Proper (pointwise_relation _ (≡) ==> (≡)) sts_inv.
  Proof. solve_proper. Qed.
  Global Instance sts_ownS_proper : Proper ((≡) ==> (≡) ==> (⊣⊢)) sts_ownS.
  Proof. solve_proper. Qed.
  Global Instance sts_ownS_affine S T : Affine (sts_ownS S T).
  Proof. apply _. Qed.
  Global Instance sts_own_proper s : Proper ((≡) ==> (⊣⊢)) (sts_own s).
  Proof. solve_proper. Qed.
  Global Instance sts_own_affine s T : Affine (sts_own s T).
  Proof. apply _. Qed.
  Global Instance sts_ctx_ne n N :
    Proper (pointwise_relation _ (dist n) ==> dist n) (sts_ctx N).
  Proof. solve_proper. Qed.
  Global Instance sts_ctx_proper N :
    Proper (pointwise_relation _ (≡) ==> (⊣⊢)) (sts_ctx N).
  Proof. solve_proper. Qed.
  Global Instance sts_ctx_relevant N φ : Persistent (sts_ctx N φ).
  Proof. apply _. Qed.
  Global Instance sts_ctx_affine N φ : Affine (sts_ctx N φ).
  Proof. apply _. Qed.
End definitions.

Typeclasses Opaque sts_own sts_ownS sts_ctx.
Instance: Params (@sts_inv) 5.
Instance: Params (@sts_ownS) 5.
Instance: Params (@sts_own) 6.
Instance: Params (@sts_ctx) 6.

Section sts.
  Context `{stsG Λ Σ sts} (φ : sts.state sts → iPropG Λ Σ).
  Implicit Types N : namespace.
  Implicit Types P Q R : iPropG Λ Σ.
  Implicit Types γ : gname.
  Implicit Types S : sts.states sts.
  Implicit Types T : sts.tokens sts.

  (* The same rule as implication does *not* hold, as could be shown using
     sts_frag_included. *)
  Lemma sts_ownS_weaken E γ S1 S2 T1 T2 :
    T2 ⊆ T1 → S1 ⊆ S2 → sts.closed S2 T2 →
    sts_ownS γ S1 T1 ={E}=> sts_ownS γ S2 T2.
  Proof. intros ???. by apply own_update, sts_update_frag. Qed.

  Lemma sts_own_weaken E γ s S T1 T2 :
    T2 ⊆ T1 → s ∈ S → sts.closed S T2 →
    sts_own γ s T1 ={E}=> sts_ownS γ S T2.
  Proof. intros ???. by apply own_update, sts_update_frag_up. Qed.

  Lemma sts_ownS_op γ S1 S2 T1 T2 :
    T1 ## T2 → sts.closed S1 T1 → sts.closed S2 T2 →
    sts_ownS γ (S1 ∩ S2) (T1 ∪ T2) ⊣⊢ (sts_ownS γ S1 T1 ★ sts_ownS γ S2 T2).
  Proof. intros. by rewrite /sts_ownS -own_op sts_op_frag. Qed.

  Lemma sts_alloc E N s :
    nclose N ⊆ E →
    ⧆▷ φ s ⊢ (|={E}=> ∃ γ, sts_ctx γ N φ ∧ sts_own γ s (⊤ ∖ sts.tok s)).
  Proof.
    iIntros (?) "Hφ". rewrite /sts_ctx /sts_own.
    iMod (own_alloc (sts_auth s (⊤ ∖ sts.tok s)) with "[#]") as (γ) "Hγ".
    { apply sts_auth_valid; set_solver. }
    { auto. }
    { set_solver. }
    iExists γ; iRevert "Hγ"; rewrite -sts_op_auth_frag_up. iIntros "[Hγ Hγ']".
    iMod (inv_alloc N _ (sts_inv γ φ) with "[Hφ Hγ]") as "#?"; auto; last by set_solver.
    iAlways. iNext. iExists s. by iFrame.
  Qed.

  Context {V} (fsa : FSA Λ (globalF Σ) V) `{!AffineFrameShiftAssertion fsaV fsa}.

  Lemma sts_afsaS E N (Ψ : V → iPropG Λ Σ) γ S T :
    fsaV → nclose N ⊆ E →
    (sts_ctx γ N φ ★ sts_ownS γ S T ★ ∀ s,
      ⧆■ (s ∈ S) ★ ⧆▷ φ s -★
      fsa (E ∖ nclose N) (λ x, ∃ s' T',
        ⧆■ sts.steps (s, T) (s', T') ★ ⧆▷ φ s' ★ (sts_own γ s' T' -★ Ψ x)))
    ⊢ fsa E Ψ.
  Proof.
    iIntros (??) "(#Hinv & Hγf & HΨ)". rewrite /sts_ctx /sts_ownS /sts_inv /sts_own.
    iInv "Hinv" as (s) "[Hγ Hφ]"; iMod "Hγ".
    rewrite (affinely_elim (own γ _)).
    iCombine "Hγ" "Hγf" as "Hγ"; iDestruct (own_valid with "Hγ") as %Hvalid.
    assert (s ∈ S) by eauto using sts_auth_frag_valid_inv.
    assert (✓ sts_frag S T) as [??] by eauto using cmra_valid_op_r.
    iRevert "Hγ"; rewrite sts_op_auth_frag //; iIntros "Hγ".
    iApply pvs_afsa_afsa; iApply afsa_wand_r; iSplitL "HΨ Hφ".
    { iApply "HΨ". rewrite -?uPred_affine_bi_affinely -affine_affine_later.
      iFrame "Hφ". rewrite ?uPred_affine_bi_affinely. by iPureIntro. }
    iAlways. iIntros (a); iDestruct 1 as (s' T') "(% & Hφ & HΨ)".
    iMod (own_update with "Hγ") as "Hγ"; first eauto using sts_update_auth.
    { set_solver.  }
    iRevert "Hγ"; rewrite -sts_op_auth_frag_up; iIntros "[Hγ Hγf]".
    iModIntro; iSplitL "Hφ Hγ"; last by iApply "HΨ".
    iAlways. iNext; iExists s'; by iFrame.
  Qed.

  Lemma sts_afsa E N (Ψ : V → iPropG Λ Σ) γ s0 T :
    fsaV → nclose N ⊆ E →
    (sts_ctx γ N φ ★ sts_own γ s0 T ★ ∀ s,
      ⧆■ (s ∈ sts.up s0 T) ★ ⧆▷ φ s -★
      fsa (E ∖ nclose N) (λ x, ∃ s' T',
        ⧆■ (sts.steps (s, T) (s', T')) ★ ⧆▷ φ s' ★
        (sts_own γ s' T' -★ Ψ x)))
    ⊢ fsa E Ψ.
  Proof. by apply sts_afsaS. Qed.
End sts.

Section afsa.
Context {Λ : language} {Σ : iFunctor}.
Implicit Types P Q : iProp Λ Σ.
Class IsAFSA {A} (P : iProp Λ Σ) (E : coPset)
    (fsa : FSA Λ Σ A) (fsaV : Prop) (Φ : A → iProp Λ Σ) := {
  is_afsa : P ⊣⊢ fsa E Φ;
  is_afsa_is_afsa :> AffineFrameShiftAssertion fsaV fsa;
}.
Global Arguments is_afsa {_} _ _ _ _ _ {_}.
Global Instance is_afsa_pvs E P :
  IsAFSA (|={E}=> P)%I E pvs_fsa True (λ _, P).
Proof. split. done. apply fsa_afsa, _. Qed.
Global Instance is_afsa_psvs E P :
  IsAFSA (|={E}=>> P)%I E psvs_fsa True (λ _, P).
Proof. split. done. apply _. Qed.
Global Instance is_afsa_afsa {A} (fsa : FSA Λ Σ A) E Φ :
  AffineFrameShiftAssertion fsaV fsa → IsAFSA (fsa E Φ) E fsa fsaV Φ.
Proof. done. Qed.
Global Instance is_afsa_wp E e Φ :
  IsAFSA (WP e @ E {{ Φ }})%I E (wp_fsa e) (language.atomic e) Φ.
Proof. split. done. apply _. Qed.
End afsa.

Section sts_tactic.
Context `{stsG Λ Σ sts} (φ : sts.state sts → iPropG Λ Σ).
Implicit Types P Q : iPropG Λ Σ.


Lemma tac_sts_fsa {A} (fsa : FSA Λ _ A) fsaV Δ E N j i γ S T Q Φ :
  IsAFSA Q E fsa fsaV Φ →
  fsaV →
  envs_lookup j Δ = Some (true, sts_ctx γ N φ) →
  envs_lookup i Δ = Some (false, sts_ownS γ S T) →
  nclose N ⊆ E →
  (∀ s, s ∈ S → ∃ Δ',
    envs_simple_replace i false (Esnoc Enil i (⧆▷ φ s)) Δ = Some Δ' ∧
    (envs_entails Δ' (fsa (E ∖ nclose N) (λ a, ∃ s' T',
      ⧆■ sts.steps (s, T) (s', T') ★ ⧆▷ φ s' ★ (sts_own γ s' T' -★ Φ a))))%I) →
  envs_entails Δ Q.
Proof.
  rewrite ?envs_entails_eq.
  intros ?? Hl1 Hl2 ? HΔ'. rewrite (is_afsa Q) -(sts_afsaS φ fsa) //.
  rewrite envs_lookup_intuitionistic_sound //; simpl.
  rewrite bi.intuitionistically_elim. apply sep_mono_r.
  clear Hl1.
  rewrite envs_lookup_sound //; simpl. apply sep_mono_r.
  apply forall_intro=>s; apply wand_intro_l.
  rewrite -assoc. apply upred_bi.pure_elim_sep_l=> Hs.
  destruct (HΔ' s) as (Δ'&?&?); clear HΔ'; auto.
  rewrite envs_simple_replace_sound' //; simpl.
  by rewrite right_id wand_elim_r.
Qed.
End sts_tactic.

Tactic Notation "iSts" constr(H) "as"
    simple_intropattern(s) simple_intropattern(Hs) :=
  match type of H with
  | string => eapply tac_sts_fsa with _ _ _ _ _ _ _ H _ _ _ _
  | gname => eapply tac_sts_fsa with _ _ _ _ _ _ _ _ H _ _ _
  | _ => fail "iSts:" H "not a string or gname"
  end;
    [let P := match goal with |- IsAFSA ?P _ _ _ _ => P end in
     apply _ || fail "iSts: cannot viewshift in goal" P
    |auto with fsaV
    |iAssumptionCore || fail "iSts: invariant not found"
    |iAssumptionCore || fail "iSts:" H "not found"
    |solve_ndisj
    |intros s Hs; eexists; split; [pm_reflexivity|simpl]].
Tactic Notation "iSts" constr(H) "as" simple_intropattern(s) := iSts H as s ?.
