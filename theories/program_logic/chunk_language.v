From fri.program_logic Require Import language.
From stdpp Require Import sets irelations.

Section chunk_lang.

  Context (L: language).
  
  Definition chunk_expr := list (option (expr L)).
  Definition chunk_state := state L.

End chunk_lang.
