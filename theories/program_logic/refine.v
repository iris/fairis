From stdpp Require Export propset.
From fri.algebra Require Import base_logic dra cmra_tactics.
From fri.program_logic Require Import pstepshifts
     language wsat refine_raw refine_raw_adequacy hoare ownership.
From iris.proofmode Require Import tactics.
Import uPred.


(* A cleaner version of results about the refinement monoid. The
   actual elements of the refinement monoid track lots of information
   to be able to make the adequacy and DRA property proofs go through,
   but from the user's perspective, this extra information is not
   needed (and the proofs in refine_raw and refine_raw_adequacy are
   quite hard to read).

   In this file we develop a "wrapper" layer that is more user friendly.
 *)

Section refine.
Context (sΛ: language).

Context `{refineG Λ Σ sΛ Kd}.

(* Asserts ownership of physical state sσ in source language *)
Definition ownSP (sσ: state sΛ) :=
  (⧆(∃ ts cs ixs, owne (refine sΛ Kd master ∅ (cs ++ [(ts, sσ)]) ixs)))%I.
Instance ownSP_affine sσ: Affine (ownSP sσ).
Proof. rewrite /ownSP; apply _. Qed.
Instance ownSP_atimeless sσ: ATimeless (ownSP sσ).
Proof. rewrite /ownSP; apply _. Qed.

(* Ownership of thread i executing expression e *)
Definition ownT (i: nat) (e: expr sΛ) :=
  (∃ c cs ixs, ⧆■(nth_error (fst c) i = Some e) ★ 
                 ownle (refine sΛ Kd snapshot {[i]} (cs ++ [c]) ixs))%I.


Section refine_lemmas.
Import uPred.

Lemma ownSP_ownSP sσ sσ': (ownSP sσ ★ ownSP sσ') ⊢ False.
Proof.
  rewrite /ownSP /master_own_exact.
  rewrite !bi.affinely_elim.
  rewrite ?sep_exist_r. apply exist_elim=> ts.
  rewrite ?sep_exist_r. apply exist_elim=> cs.
  rewrite ?sep_exist_r. apply exist_elim=> ixs.
  rewrite ?sep_exist_l. apply exist_elim=> ts'.
  rewrite ?sep_exist_l. apply exist_elim=> cs'.
  rewrite ?sep_exist_l. apply exist_elim=> ixs'.
  rewrite -owne_op owne_valid bi.affinely_elim. apply valid_elim.
  inversion 1 as (_&_&Hdisj).
  inversion Hdisj.
Qed.

Lemma ownT_ownT i i' e e': 
  (ownT i e ★ ownT i' e') ⊣⊢ (⧆■(i ≠ i') ★ ownT i e ★ ownT i' e').
Proof.
  apply (anti_symm (⊢)); last (by rewrite sep_elim_r).
  case (decide (i = i')).
  - intros ->. 
    etransitivity; last apply False_elim.
    rewrite /ownT /snapshot_ownl_exact.
    rewrite ?sep_exist_r. apply exist_elim=> c.
    rewrite ?sep_exist_r. apply exist_elim=> cs.
    rewrite ?sep_exist_r. apply exist_elim=> ixs.
    rewrite ?sep_exist_l. apply exist_elim=> c'.
    rewrite ?sep_exist_l. apply exist_elim=> cs'.
    rewrite ?sep_exist_l. apply exist_elim=> ixs'.
    iIntros "((_&H1)&(_&H2))".
    iCombine "H1" "H2" as "H".
    rewrite -ownle_op ownle_valid_l. 
    rewrite valid_elim.
    * iDestruct "H" as "(%&?)". done. 
    * inversion 1 as (_&_&Hdisj).
      inversion Hdisj as [?????? Hdisj' | |]. clear -Hdisj'. set_solver.
  - intros Hneq. iIntros "(H1&H2)". iFrame. iPureIntro. done. 
Qed.

Lemma ownT_ownSP_step_nofork N' E i e sσ e' sσ':
  nsteps (prim_step_nofork sΛ) N' (e, sσ) (e', sσ') →
  (1 ≤ N' ∧ N' ≤ Kd) →
  (ownT i e ★ ownSP sσ) ⊢ (|={E}=>> ownT i e' ★ ownSP sσ').
Proof.
  iIntros (Hnsteps Hbound). 
  iIntros "(Hown1&Hown2)".
  rewrite /ownT /ownSP.
  iDestruct "Hown1" as (c cs ixs) "(%&Hown1)".
  rewrite bi.affinely_exist. iDestruct "Hown2" as (ts') "Hown2".
  rewrite bi.affinely_exist. iDestruct "Hown2" as (cs') "Hown2".
  rewrite bi.affinely_exist. iDestruct "Hown2" as (ixs') "Hown2".
  destruct c as (ts&sσ0).
  efeed pose proof (owne_stepP refine_alt_triv) as Hshift0. 
  { eapply snap_master_stepshift_nofork; eauto. }
  iPoseProof Hshift0 as "Hshift".
  - rewrite (bi.affinely_elim (owne _)). iCombine "Hown2" "Hown1" as "Hown".
    iPsvs ("Hshift" with "Hown").
    iDestruct "Hshift" as (r' rl') "(%&Hownr&Hownrl)".
    destruct H1 as (cs''&ts''&ixs''&Hnth_error''&Hr'_equiv&Hrl'_equiv).
    rewrite Hr'_equiv. rewrite Hrl'_equiv.
    iSplitL "Hownrl". 
    * iExists (ts'', sσ'), cs'', ixs''. 
      iSplitR "Hownrl"; auto.
    * iExists ts''. iModIntro; iAlways. 
      iExists cs'', ixs''; auto.
Qed.

Lemma prim_step_nofork_ctx `{LanguageCtx sΛ K} N' e sσ e' sσ':
  nsteps (prim_step_nofork sΛ) N' (e, sσ) (e', sσ') →
  nsteps (prim_step_nofork sΛ) N' (K e, sσ) (K e', sσ').
Proof.
  remember (e, sσ) as c eqn:Heq_c.
  remember (e', sσ') as c' eqn:Heq_c'.
  intros Hnsteps. revert e sσ e' sσ' Heq_c Heq_c'.
  induction Hnsteps; intros.
  - subst. inversion Heq_c'. subst. econstructor.
  - subst. destruct y as (e''&sσ''). econstructor; eauto.
    * rewrite /prim_step_nofork //=. eapply fill_step; eauto.
Qed.

Lemma ownT_ownSP_step_nofork_ctx `{LanguageCtx sΛ K} N' E i e sσ e' sσ':
  nsteps (prim_step_nofork sΛ) N' (e, sσ) (e', sσ') →
  (1 ≤ N' ∧ N' ≤ Kd) →
  (ownT i (K e) ★ ownSP sσ) ⊢ |={E}=>> ownT i (K e') ★ ownSP sσ'.
Proof.
  intros. eapply ownT_ownSP_step_nofork; eauto using prim_step_nofork_ctx.
Qed.

Lemma ownT_ownSP_step_nofork_ctx1 `{LanguageCtx sΛ K} E i e sσ e' sσ':
  prim_step e sσ e' sσ' None →
  (ownT i (K e) ★ ownSP sσ) ⊢ |={E}=>> ownT i (K e') ★ ownSP sσ'.
Proof.
  intros; eapply (ownT_ownSP_step_nofork_ctx 1).
  rewrite /prim_step_nofork.
  eapply nsteps_once; eauto.
  split; auto. 
  destruct Kd; last lia. exfalso. eapply refine_Knz; auto. 
Qed.

Lemma ownT_ownSP_step_fork E i e sσ e' sσ' ef:
  prim_step e sσ e' sσ' (Some ef)  →
  (ownT i e ★ ownSP sσ) ⊢ |={E}=>> ∃ i', ownT i e' ★ ownT i' ef ★ ownSP sσ'.
Proof.
  iIntros (Hstep).
  iIntros "(Hown1&Hown2)".
  rewrite /ownT /ownSP.
  iDestruct "Hown1" as (c cs ixs) "(%&Hown1)".
  rewrite ?affine_exist. iDestruct "Hown2" as (ts') "Hown2".
  rewrite ?affine_exist. iDestruct "Hown2" as (cs') "Hown2".
  rewrite ?affine_exist. iDestruct "Hown2" as (ixs') "Hown2".
  destruct c as (ts&sσ0).
  efeed pose proof (owne_stepP refine_alt_triv) as Hshift0. 
  { eapply snap_master_simple_fork; eauto using refine_Knz. }
  iPoseProof Hshift0 as "Hshift"; eauto.
  - rewrite (affinely_elim (owne _)). iCombine "Hown2" "Hown1" as "Hown".
    iPsvs ("Hshift" with "Hown").
    iDestruct "Hshift" as (r' rl') "(%&Hownr&Hownrl)".
    destruct H1 as (i'&cs''&ts''&ixs''&Hnth_error''&Hnth_error2''&Hr'_equiv&Hrl'_equiv).
    rewrite Hr'_equiv. rewrite Hrl'_equiv.
    rewrite ownle_op. iDestruct "Hownrl" as "(Hownrl1&Hownrl2)".
    iExists i'.
    iSplitL "Hownrl1". 
    * iExists (ts'', sσ'), cs'', ixs''. 
      iSplitR "Hownrl1"; auto.
    * iSplitL "Hownrl2".
      ** iExists (ts'', sσ'), cs'', ixs''. 
         iSplitR "Hownrl2"; auto.
      ** iModIntro; iAlways. iExists ts'', cs'', ixs''; auto. 
Qed.

Lemma ownT_ownSP_step_fork_ctx `{LanguageCtx sΛ K} E i e sσ e' sσ' ef:
  prim_step e sσ e' sσ' (Some ef)  →
  (ownT i (K e) ★ ownSP sσ) ⊢ |={E}=>> ∃ i', ownT i (K e') ★ ownT i' ef ★ ownSP sσ'.
Proof.
  intros Hprim. eapply ownT_ownSP_step_fork; eauto using fill_step.
Qed.

Lemma own_value_stopped P V ES sσ' k:
  nth_error ES k = Some (of_val V) →
  (snapshot_ownl _ _ {[k]} (ES, sσ') ★ ⧆ P) ⊢ uPred_stopped.
Proof.
  intros. eapply own_value_stopped.
  - apply (refine_Knz).
  - eauto.
Qed.

Lemma ownT_val_stopped i v P:
  (ownT i (of_val v) ★ ⧆P) ⊢ uPred_stopped.
Proof.
    rewrite /ownT.
    rewrite sep_exist_r. apply exist_elim=> V.
    rewrite sep_exist_r. apply exist_elim=> cs.
    rewrite sep_exist_r. apply exist_elim=> ixs.
    rewrite -assoc.
    destruct V.
    iIntros "(%&?&?)".
    iApply own_value_stopped; eauto. iFrame.
    by iExists cs, ixs.
Qed.
End refine_lemmas.

Section refine_adequacy.
Import uPred.

(* We assume the primitive step relation of the source language is
   deterministic, so that the only source of non-determinism is the
   choice of the concurrent scheduler. *)
Context (PrimDet: ∀ (e: expr sΛ) σ e1' σ1' ef1' e2' σ2' ef2',
            prim_step e σ e1' σ1' ef1' →
            prim_step e σ e2' σ2' ef2' →
            e1' = e2' ∧ σ1' = σ2' ∧ ef1' = ef2').
            

Context (PrimDec: ∀ (e: expr sΛ) σ, { t | prim_step e σ (fst (fst t)) (snd (fst t)) (snd t)} +
                                    {¬ ∃ e' σ' ef', prim_step e σ e' σ' ef'}).

Lemma init_ownT E σ:
  ownGl (to_globalFe (refine sΛ Kd snapshot {[0]} ([([E], σ)]) []))
        ⊢ ownT 0 E.
Proof.
  rewrite /ownT. iIntros "H1". iExists ([E], σ). iExists ([]). iExists [].
  rewrite ownle_eq. iFrame "H1".
  by iPureIntro.
Qed.

Lemma init_ownSP E σ:
  ownG (to_globalFe (refine sΛ Kd master ∅ ([([E], σ)]) []))
        ⊢ ownSP σ.
Proof.
  rewrite /ownSP. iIntros "H1". iAlways. 
  iExists [E], ([]), []. rewrite owne_eq // app_nil_l.
Qed.

  
(* Adequacy theorem for terminating executions of target exoression *)
Lemma ht_adequacy_refine' (E: expr sΛ) (e: expr Λ) v t2 (sσ: state sΛ) (σ σ2: state Λ) Φ l:
  isteps idx_step l ([e], σ) (of_val v :: t2, σ2) →
  Forall (λ e, ¬ reducible e σ2) (of_val v :: t2) →
  {{ ownT 0 E ★ ownP σ ★ ownSP sσ }} 
    e  
  {{ v, (∃ V, ownT 0 (of_val V) ★ ⧆■ Φ v V) }} →
  ∃ l' V T2 sσ2, 
    isteps idx_step l' ([E], sσ) (of_val V :: T2, sσ2) ∧
    (∀ i, ¬ (enabled idx_step (of_val V :: T2, sσ2)) i) ∧
    Φ v V.
Proof.
  intros Histep Hnr Hht.
  eapply ht_adequacy_refine; eauto using refine_Knz.
  rewrite Hht; apply ht_mono.
  - repeat apply sep_mono; auto.
    * rewrite /ownT /snapshot_ownl_exact. 
      iIntros "H". iExists ([E], sσ), [], []. 
      iFrame; by iPureIntro.
    * rewrite /ownSP /master_own_exact. iIntros.
      iAlways. by iExists [E], [], []. 
  - intros v'.
    rewrite /ownT /snapshot_ownl.
    apply exist_elim=> V.
    rewrite sep_exist_r. apply exist_elim=> c.
    rewrite sep_exist_r. apply exist_elim=> cs.
    rewrite sep_exist_r. apply exist_elim=> ixs.
    rewrite -assoc.
    iIntros "(Hp&H1&H2)".
    iDestruct "Hp" as %Hnth.
    destruct c as (ts&sσ').
    destruct ts as [| t ts']; simpl in Hnth; first congruence.
    inversion Hnth; subst. 
    iExists V, ts', sσ'. iFrame.
    by iExists cs, ixs. 
Qed.

(* Adequacy theorem for diverging executions of target expression *)
Lemma ht_adequacy_inf_refine' (E: expr sΛ) (e: expr Λ) (sσ: state sΛ) (σ σ2: state Λ) Φ
  (tr: trace idx_step ([e], σ)):
  {{ ownT 0 E ★ ownP σ ★ ownSP sσ }} 
    e  
  {{ v, (∃ V, ownT 0 (of_val V) ★ ⧆■ Φ v V) }} →
  exists (tr': trace idx_step ([E], sσ)), fair_pres _ _ tr tr'.
Proof.
  intros Hht.
  eapply ht_adequacy_inf_refine with _ _ _ (λ v V, ■Φ v V)%I; eauto using refine_Knz.
  rewrite Hht; apply ht_mono.
  - repeat apply sep_mono; auto.
    * rewrite /ownT /snapshot_ownl_exact. 
      iIntros "H". iExists ([E], sσ), [], []. 
      iFrame; by iPureIntro.
    * rewrite /ownSP /master_own_exact. iIntros.
      iAlways. by iExists [E], [], []. 
  - intros v'.
    rewrite /ownT /snapshot_ownl.
    apply exist_elim=> V.
    rewrite sep_exist_r. apply exist_elim=> c.
    rewrite sep_exist_r. apply exist_elim=> cs.
    rewrite sep_exist_r. apply exist_elim=> ixs.
    rewrite -assoc.
    iIntros "(Hp&H1&H2)".
    iDestruct "Hp" as %Hnth.
    destruct c as (ts&sσ').
    destruct ts as [| t ts']; simpl in Hnth; first congruence.
    inversion Hnth; subst. 
    iExists V, ts', sσ'. iFrame.
    by iExists cs, ixs. 
Qed.

(* Bundle previous 2 results + safety result from adequacy.v to get safe refinement: *)
Lemma ht_safe_refine' e σ E sσ Φ: 
  {{ ownT 0 E ★ ownP σ ★ ownSP sσ }} e {{ v, (∃ V, ownT 0 (of_val V) ★ ⧆■ Φ v V) }} →
  safe_refine' Φ e σ E sσ.
Proof.
  intros Ht. split_and!.
  - intros; apply Forall_forall=>e' Hin.
    case_eq (to_val e').
      * intros; left; eauto. 
      * intros; right.
        eapply (adequacy.ht_adequacy_reducible _
                (λ v, (∃ V, ownT 0 (of_val V) ★ ⧆■ Φ v V)%I)); eauto; last first.
        ** erewrite Ht. apply ht_mono; last auto.
           repeat apply sep_mono; eauto using init_ownT, init_ownSP.
        ** eapply init_own_snap_master_valid; apply refine_Knz.
  - intros ???? Histeps Hforall. 
    edestruct (ht_adequacy_refine') as (l'&V&T2&sσ2&Hstep&Hnenabled&Hphi); 
      eauto.
    { econstructor.
      - intros Hr%reducible_not_val.
        rewrite to_of_val in Hr; congruence.
      - apply Forall_forall. rewrite Forall_forall in Hforall *. 
        intros Hforall e' Hin Hr%reducible_not_val. edestruct (Hforall); eauto.
        congruence.
    }
    exists l', V, T2, sσ2.
    split_and!; auto.
    * apply forall_not_enabled_forall_not_reducible, Forall_cons in Hnenabled. 
      destruct Hnenabled; auto.
  - intros t Hfair.
    edestruct (ht_adequacy_inf_refine') as (T&Hfp);
      eauto.
Qed.

Lemma ht_safe_refine e σ E sσ Φ: 
  {{ ownT 0 E ★ ownP σ ★ ownSP sσ }} e {{ v, (∃ V, ownT 0 (of_val V) ★ ⧆■ Φ v V) }} →
  safe_refine Φ e σ E sσ.
Proof.
  eauto using ht_safe_refine', safe_refine'_to_safe_refine.
Qed.

End refine_adequacy.

End refine.