From fri.algebra Require Export updates base_logic.
From fri.program_logic Require Export model.
From iris.proofmode Require Import tactics.
Import uPred.

Definition ownI {Λ Σ} (i : positive) (P : iProp Λ Σ) : iProp Λ Σ :=
  (⧆ (uPred_ownM (Res {[ i := to_agree (Next (iProp_unfold P)) ]} ∅ ∅)))%I.
Arguments ownI {_ _} _ _%I.
Definition ownP {Λ Σ} (σ: state Λ) : iProp Λ Σ := (⧆ (uPred_ownM (Res ∅ (Excl' σ) ∅)))%I.
Definition ownG {Λ Σ} (m: iGst Λ Σ) : iProp Λ Σ := (⧆ (uPred_ownM (Res ∅ ∅ m)))%I.
Definition ownGl {Λ Σ} (m: iGst Λ Σ) : iProp Λ Σ := uPred_ownMl (Res ∅ ∅ m).
Instance: Params (@ownI) 3 := {}.
Instance: Params (@ownP) 2 := {}.
Instance: Params (@ownG) 2 := {}.
Instance: Params (@ownGl) 2 := {}.

Typeclasses Opaque ownI ownG ownP.

Section ownership.
Context {Λ : language} {Σ : iFunctor}.
Implicit Types r : iRes Λ Σ.
Implicit Types σ : state Λ.
Implicit Types P : iProp Λ Σ.
Implicit Types m : iGst Λ Σ.

(* Invariants *)
Global Instance ownI_contractive i : Contractive (@ownI Λ Σ i).
Proof.
  intros n P Q HPQ. rewrite /ownI.
  unfold_bi.
  apply and_ne => //.
  apply uPred.ownM_ne, Res_ne; auto; apply singleton_ne, to_agree_ne.
  apply Next_contractive. destruct n => //=; rewrite //= in HPQ; rewrite HPQ //=.
Qed.
Global Instance ownI_persistent i P : Persistent (ownI i P).
Proof. rewrite /ownI. apply _. Qed.
Global Instance ownI_affine i P : Affine (ownI i P).
Proof. rewrite /ownI. apply _. Qed.

(* physical state *)
Lemma ownP_twice σ1 σ2 : ownP σ1 ∗ ownP σ2 ⊢ (False : iProp Λ Σ).
Proof.
  rewrite /ownP.
  iIntros "(H1&H2)". iCombine "H1" "H2" as "H".
  rewrite -upred_bi.ownM_op Res_op.
  rewrite uPred.ownM_invalid; eauto with I.
  by intros (_&?&_).
Qed.
Global Instance ownP_affine σ : Affine (@ownP Λ Σ σ).
Proof. rewrite /ownP. apply _. Qed.
Global Instance ownP_atimeless σ : ATimeless (@ownP Λ Σ σ).
Proof. rewrite /ownP. apply _. Qed.
(* ghost state *)
Global Instance ownG_ne n : Proper (dist n ==> dist n) (@ownG Λ Σ).
Proof. intros ??. rewrite /ownG. intros ->.  done. Qed.
Global Instance ownG_affine m : Affine (ownG m).
Proof. rewrite /ownG. apply _. Qed.
Global Instance ownG_proper : Proper ((≡) ==> (⊣⊢)) (@ownG Λ Σ) := ne_proper _.
Lemma ownG_op m1 m2 : ownG (m1 ⋅ m2) ⊣⊢ ownG m1 ∗ ownG m2.
Proof. by rewrite /ownG -upred_bi.ownM_op'' Res_op !left_id. Qed.
Global Instance ownG_mono : Proper (flip (≼) ==> (⊢)) (@ownG Λ Σ).
Proof. move=>a b [c H]. rewrite H ownG_op. apply: sep_elim_l. Qed.
Lemma ownG_valid m : ownG m ⊢ ⧆ (✓ m).
Proof. rewrite /ownG uPred.ownM_valid res_validI /=. rewrite !affinely_and; auto with *. Qed.
Lemma ownG_empty : emp ⊢ (ownG ∅ : iProp Λ Σ).
Proof. rewrite /ownG. by apply @upred_bi.ownM_empty. Qed.
Global Instance ownG_timeless m : Discrete m → ATimeless (ownG m).
Proof. rewrite /ownG; apply _. Qed.
Global Instance ownG_persistent m : cmra.Persistent m → Persistent (ownG m).
Proof. rewrite /ownG; apply _. Qed.

(* linear ghost state *)
Global Instance ownGl_ne n : Proper (dist n ==> dist n) (@ownGl Λ Σ).
Proof. intros P Q HPQ. rewrite /ownGl. rewrite HPQ. done. Qed.
Global Instance ownGl_proper : Proper ((≡) ==> (⊣⊢)) (@ownGl Λ Σ) := ne_proper _.
Lemma ownGl_op m1 m2 : ownGl (m1 ⋅ m2) ⊣⊢ (ownGl m1 ★ ownGl m2).
Proof. by rewrite /ownGl; unfold_bi; rewrite -uPred.ownMl_op Res_op !left_id. Qed.
Lemma ownGl_valid_r m : ownGl m ⊢ (ownGl m ★ ⧆✓ m).
Proof. rewrite /ownGl {1}upred_bi.ownMl_valid  res_validI /= !affinely_and; auto with I. Qed.
Lemma ownGl_empty : emp ⊢ (ownGl ∅ : iProp Λ Σ).
Proof. by rewrite /ownGl upred_bi.ownMl_empty. Qed.
Global Instance ownGl_relevant m : cmra.Persistent m → Persistent (⧆ownGl m).
Proof. rewrite /ownGl. apply _. Qed.

(* inversion lemmas *)
Lemma ownI_spec n r r' i P :
  ✓{n} r →
  ✓{n} r' →
  (ownI i P) n r r' ↔ wld r !! i ≡{n}≡ Some (to_agree (Next (iProp_unfold P))) ∧ r' ≡{n}≡ ∅.
Proof.
  intros (?&?&?) (?&?&?). rewrite /ownI; do 2 unseal.
  rewrite /uPred_holds/=/uPred_holds/=res_includedN/= singleton_includedN; split.
  - intros [? ((P'&Hi&HP)&_)]; rewrite Hi.
    split; auto. constructor; symmetry; apply agree_valid_includedN. 
    + by apply lookup_validN_Some with (wld r) i.
    + by destruct HP as [?| ->].
  - intros (?&?); split_and?; try apply ucmra_unit_leastN; eauto.
Qed.
Lemma ownP_spec n r r' σ : ✓{n} r → ✓{n} r' → (ownP σ) n r r' ↔ pst r ≡ Excl' σ ∧ r' ≡{n}≡ ∅.
Proof.
  intros (?&?&?) (?&?&?). rewrite /ownP; do 2 unseal.
  rewrite /uPred_holds /= /uPred_holds /= res_includedN /= Excl_includedN //.
  rewrite (discrete_iff n). naive_solver (apply ucmra_unit_leastN).
Qed.
Lemma ownP_spec' n r r' σ : ✓{n} r → ✓{n} r' → (ownP σ) n r r' → pst r ≡ Excl' σ.
Proof.
  intros (?&?&?) (?&?&?). rewrite /ownP; do 2 unseal.
  rewrite /uPred_holds /= /uPred_holds /= res_includedN /= Excl_includedN //.
  rewrite (discrete_iff n). naive_solver (apply cmra_unit_leastN).
Qed.
Lemma ownG_spec n r r' m : (ownG m) n r r' ↔ m ≼{n} gst r ∧ r' ≡{n}≡ ∅.
Proof.
  rewrite /ownG; do 2 unseal.
  rewrite /uPred_holds /= /uPred_holds /= res_includedN; naive_solver (apply ucmra_unit_leastN).
Qed.
Lemma ownGl_spec n r r' m : (ownGl m) n r r' ↔ Res ∅ ∅ m ≡{n}≡ r'.
Proof.
  rewrite /ownGl; uPred.unseal.
  rewrite /uPred_holds /= /uPred_holds //=.
Qed.
End ownership.
