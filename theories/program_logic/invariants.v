From stdpp Require Export coPset.
From fri.algebra Require Export updates base_logic.
From fri.program_logic Require Export model.
From fri.program_logic Require Import ownership wsat.
From fri.program_logic Require Export pviewshifts.
From iris.proofmode Require Import tactics coq_tactics intro_patterns.
From stdpp Require Export namespaces.
Import uPred.

(** Derived forms and lemmas about them. *)
Definition inv_def {Λ Σ} (N : namespace) (P : iProp Λ Σ) : iProp Λ Σ :=
  (∃ i, ⧆■ (i ∈ nclose N) ∧ ownI i P)%I.
Definition inv_aux : { x | x = @inv_def }. by eexists. Qed.
Definition inv {Λ Σ} := proj1_sig inv_aux Λ Σ.
Definition inv_eq : @inv = @inv_def := proj2_sig inv_aux.
Instance: Params (@inv) 3.
Typeclasses Opaque inv.

Section inv.
Context {Λ : language} {Σ : iFunctor}.
Implicit Types i : positive.
Implicit Types N : namespace.
Implicit Types P Q R : iProp Λ Σ.
Implicit Types Φ : val Λ → iProp Λ Σ.

Global Instance inv_contractive N : Contractive (@inv Λ Σ N).
Proof.
  rewrite inv_eq=> n ???. apply exist_ne=>i. by apply and_ne, ownI_contractive.
Qed.
Global Instance inv_relevant N P : Persistent (inv N P).
Proof. rewrite inv_eq /inv /inv_def. apply _. Qed.
Global Instance inv_affine N P : Affine (inv N P).
Proof. rewrite inv_eq /inv/inv_def. apply _. Qed.
Lemma inv_alloc N E P : nclose N ⊆ E → ⧆▷ P ⊢ (|={E,E}=> inv N P).
Proof.
  intros. rewrite -(pvs_mask_weaken (nclose N)) //.
  rewrite inv_eq /inv (pvs_allocI (nclose N)); last apply nclose_infinite.
  eapply pvs_mono, exist_mono. intros i. unfold ownI.
  iStartProof. iIntros "[#? ?]". iFrame. by iAlways.
Qed.

Lemma inv_open E N P :
  nclose N ⊆ E →
  inv N P ⊢ ∃ E', ⧆■ (E ∖ nclose N ⊆ E' ⊆ E) ★
                    |={E,E'}=> ⧆▷ P ★ ⧆(⧆▷ P ={E',E}=∗ emp).
Proof.
  rewrite inv_eq /inv. iDestruct 1 as (i) "[% #Hi]".
  iExists (E ∖ {[ i ]}). iSplit.
  { by iAlways; iPureIntro; set_solver. }
  iMod (pvs_openI' with "Hi") as "HP"; [set_solver..|].
  iModIntro. iSplitL "HP"; first done.
  iAlways. iIntros "HP".
  iMod (pvs_closeI_sep' _ _ P with "[HP]"); [set_solver|iSplit;done|set_solver|].
  by iModIntro.
Qed.


(*
Global Instance elim_inv_inv E N P P' Q Q' (φ: Prop):
  φ →
  ElimModal φ (∃ E', ⧆■ (E ∖ nclose N ⊆ E' ⊆ E)
                           ★ |={E,E'}=> ⧆▷ P ∗ ⧆(⧆▷ P ={E',E}=∗ emp))%I P' Q Q' →
  ElimInv (↑N ⊆ E) N (inv N P) [] P' Q Q'.
Proof.
  rewrite /ElimInv/ElimModal.
  iIntros (Hpi Helim ?) "(#H1&_&H2)".
  iApply Helim; auto; iFrame.
  iApply (inv_open E N with "[#]"); auto.
Qed.
*)

(*

Global Instance elim_inv_inv E N P P' Q Q':
  ElimModal (E2 ⊆ E1 ∪ E3)True (∃ E', ⧆■ (E ∖ nclose N ⊆ E' ⊆ E)
                           ★ |={E,E'}=> ⧆▷ P ∗ ⧆(⧆▷ P ={E',E}=∗ emp))%I P' Q Q' →
  ElimInv (↑N ⊆ E) N (inv N P) [] P' Q Q'.
Proof.
  rewrite /ElimInv/ElimModal.
  iIntros (Helim ?) "(#H1&_&H2)".
  iApply Helim; auto; iFrame.
  iApply (inv_open E N with "[#]"); auto.
Qed.
*)

(** Invariants can be opened around any frame-shifting assertion. This is less
    verbose to apply than [inv_open]. *)
Lemma inv_fsa {A} (fsa : FSA Λ Σ A) `{!FrameShiftAssertion fsaV fsa} E N P Ψ :
  fsaV → nclose N ⊆ E →
  (inv N P ★ (⧆▷ P -★ fsa (E ∖ nclose N) (λ a, ⧆▷ P ★ Ψ a))) ⊢ fsa E Ψ.
Proof.
  iIntros (??) "[Hinv HΨ]".
  iDestruct (inv_open E N P with "Hinv") as (E') "[% Hvs]"; first done.
  iApply (fsa_open_close E E'); auto; first set_solver.
  iMod "Hvs" as "[HP Hvs]"; first set_solver.
  (* TODO: How do I do sth. like [iSpecialize "HΨ HP"]? *)
  iModIntro. iApply (fsa_mask_weaken _ (E ∖ (nclose N))); first set_solver.
  iApply fsa_wand_r. iSplitR "Hvs"; first by (iApply "HΨ").
  iIntros (v) "[HP HΨ]". rewrite affinely_elim. iMod ("Hvs" with "HP"); first set_solver.
  iModIntro. done.
Qed.

Lemma inv_afsa {A} (fsa : FSA Λ Σ A) `{!AffineFrameShiftAssertion fsaV fsa} E N P Ψ :
  fsaV → nclose N ⊆ E →
  (inv N P ★ (⧆▷ P -★ fsa (E ∖ nclose N) (λ a, ⧆▷ P ★ Ψ a))) ⊢ fsa E Ψ.
Proof.
  iIntros (??) "[Hinv HΨ]".
  iDestruct (inv_open E N P with "Hinv") as (E') "[% Hvs]"; first done.
  iApply (afsa_open_close E E'); auto; first set_solver.
  iMod "Hvs" as "[HP Hvs]"; first set_solver.
  iModIntro. iApply (afsa_mask_weaken _ (E ∖ (nclose N))); first set_solver.
  iApply afsa_wand_r. iSplitR "Hvs"; first by (iApply "HΨ").
  iAlways. iIntros (v) "[HP HΨ]".
  rewrite affinely_elim. iMod ("Hvs" with "HP"); first set_solver.
  iModIntro; done.
Qed.

Global Instance into_inv_inv N P: IntoInv (inv N P) N.

Global Instance elim_inv_inv E N P Q:
  ElimInv (↑N ⊆ E) (inv N P) emp (λ _:(), ⧆▷ P)%I None (|={E}=> Q) (λ _, |={E ∖ nclose N}=> ⧆▷ P ∗ Q)%I.
Proof.
  rewrite /ElimInv/ElimModal.
  intros Hclose. rewrite //= forall_unit !right_id !left_id.
  iIntros "H".
  iPoseProof (inv_afsa _ E _ _ (λ x, Q) with "[H]") as "H"; eauto.
Qed.

Global Instance elim_inv_fsa {V: Type} (fsa : FSA Λ Σ V) fsaV E N P Ψ:
  FrameShiftAssertion fsaV fsa →
  ElimInv (↑N ⊆ E ∧ fsaV) (inv N P) emp
          (λ _:(), ⧆▷ P)%I None (fsa E Ψ) (λ _, fsa (E ∖ nclose N) (λ a, ⧆▷ P ∗ Ψ a))%I.
Proof.
  rewrite /ElimInv => FSA [Hclose ?]. rewrite //= forall_unit !right_id !left_id.
  eapply inv_fsa; eauto.
Qed.

Global Instance elim_inv_afsa {A} (fsa : FSA Λ Σ A) fsaV E N P Ψ:
  AffineFrameShiftAssertion fsaV fsa →
  ElimInv (↑N ⊆ E ∧ fsaV) (inv N P) emp
          (λ _:(), ⧆▷ P)%I None (fsa E Ψ) (λ _, fsa (E ∖ nclose N) (λ a, ⧆▷ P ∗ Ψ a))%I.
Proof.
  rewrite /ElimInv => FSA [Hclose ?]. rewrite //= forall_unit !right_id !left_id.
  eapply inv_afsa; eauto.
Qed.

End inv.
