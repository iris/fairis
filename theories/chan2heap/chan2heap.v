(* Here we define a translation from chan_lang to heap_lang. We will prove that
   this is a refinement for well-typed chan_lang expressions *)

From fri.chan_lang Require Export lang notation.
From fri.heap_lang Require Import lang notation.
From stdpp Require Import gmap stringmap mapset.

(* Implementations of channel receive/send as linked list. The subtlety
   here is that whereas in the source language, threads communicate over a pair of channels,
   here we flatten it as just 1 linked list.  *)

Definition alloc : heap_lang.expr :=
  let: "l" := ref (InjR #()) in
    ("l", "l").

Definition recv : heap_lang.expr :=
  rec: "recv" "l" :=
    match: !"l" with
      InjR "_" => ("recv": heap_lang.expr) "l"
    | InjL "x" => "x"
    end. 

Definition send l v : heap_lang.expr :=
  letp: "l" "v" := (l, v) in
        let: "lnew" := ref (InjR #()) in
        "l" <- InjL (("lnew", "v"));; "lnew".

Section translate.
Import chan_lang.
Import fri.chan_lang.derived.
Import fri.chan_lang.notation.

Fixpoint c2h (ec: chan_lang.expr) : heap_lang.expr :=
  (match ec with
   | Var x => x
   | Rec f x e => rec: f x := c2h e
   | App e1 e2 => (c2h e1) (c2h e2)
   | Lit l => 
     match l with
     | LitInt n => #n
     | LitBool b => #b
     | LitUnit => heap_lang.Lit heap_lang.LitUnit
     (* Well typed programs in the empty-context do not have locations in them, so
        this is fine. *)
     | LitLoc l _ => heap_lang.Lit (heap_lang.LitLoc l)
     end
   | Pair e1 e2 => (c2h e1, c2h e2)
   | Fork e => heap_lang.Fork (c2h e)
   | Letp x y e eb => heap_lang.Letp x y (c2h e) (c2h eb)
   | Recv e => recv (c2h e)
   | Send e1 e2 => send (c2h e1) (c2h e2)
   | Alloc => alloc
   | _ => (heap_lang.Lit heap_lang.LitUnit)
   end)%E.

Lemma c2h_closed ec l: is_closed l ec → heap_lang.is_closed l (c2h ec).
Proof.
  revert l; induction ec; simpl; try (naive_solver; eauto).
  intros. case_match; auto.
Qed.

End translate.