From fri.algebra Require Import upred.
From iris.bi Require Export bi.
From iris.proofmode Require Import coq_tactics classes class_instances_sbi tactics.


Import uPred.
Lemma uPred_bi_mixin (M : ucmraT) : BiMixin
  uPred_entails uPred_emp uPred_pure uPred_and uPred_or uPred_impl
                (@uPred_forall M) (@uPred_exist M) 
                uPred_sep uPred_wand uPred_persistent.
Proof.
  split; try apply _; eauto with I.
  - (* (P ⊣⊢ Q) ↔ (P ⊢ Q) ∧ (Q ⊢ P) *)
    intros P Q. split.
    + intros HPQ; split; split=> x i; apply HPQ.
    + intros [HPQ HQP]; split=> x n; by split; [apply HPQ|apply HQP].
  - intros n P Q HPQ. apply equiv_dist. unseal; split => ?????; auto.
  - intros ?? Himpl; eapply pure_elim; eauto; intros. etransitivity; last eapply Himpl; eauto.
  - by unseal.
  - intros P Q. unseal. by split=> n x y ?? [? ?].
  - intros P Q. unseal. by split=> n x y ?? [? ?].
  - intros P Q. unseal. by split=> n x y ??; left.
  - intros P Q. unseal. by split=> n x y ??; right.
  - apply impl_intro_r.
  - intros P Q R HP. eapply impl_elim with Q; auto.
    * etransitivity; last eapply HP. apply and_elim_l.
    * apply and_elim_r.
  - intros; by apply forall_intro.
  - intros; by apply forall_elim.
  - intros; by apply exist_intro.
  - intros; by apply exist_elim.
  - apply sep_mono.
  - intros; by rewrite left_id.
  - intros; by rewrite left_id.
  - intros; by rewrite comm.
  - intros; by rewrite assoc.
  - intros; by apply wand_intro_r.
  - intros; by apply wand_elim_l'.
  - unseal. intros P. split => n x y ?? => //=. rewrite /uPred_holds //=.
    intros HP. eapply H; eauto.
    * apply cmra_core_validN; eauto.
    * apply ucmra_unit_validN.
  - unseal. split => n x y ?? => //=. rewrite /uPred_holds //= => H.
    rewrite /uPred_holds //=. rewrite cmra_core_idemp //.
  - unseal. split => n x y ?? => //=.
    do 2 rewrite /uPred_holds //=.
  - unseal. split => n x y ?? => //=.
  - unseal. split => n x y ?? => //=.
  - intros.
    unseal; split=>n x y ??. 
    intros (x1&x2&y1&y2&Heq1&Heq2&HP&HQ).
    eapply (uPred_mono _ _ _ x1 y); eauto. 
    eexists; done.
  - intros.
    unseal; split=>n x y ??.
    intros (HP&HQ). 
    exists (core x), x, (∅: M), y; split_and!.
    * by rewrite cmra_core_l.
    * by rewrite left_id.
    * move: HP. rewrite /uPred_holds//=.
    * done.
Qed.

Lemma uPred_sbi_mixin (M : ucmraT) : SbiMixin
  uPred_entails uPred_pure uPred_or uPred_impl
                (@uPred_forall M) (@uPred_exist M) 
                uPred_sep uPred_persistent (@uPred_eq M)
                uPred_later.
Proof.
  split; try apply _; eauto with I.
  - unseal; split=> n x y ???; simpl. rewrite /uPred_holds => //=.
  - (* a ≡ b ⊢ Ψ a → Ψ b *)
    intros A a b Ψ Hnonexp.
    unseal; split=> n x ? Hab n' x' y' ????? HΨ. eapply Hnonexp with n a; auto.
  - by unseal.
  - by unseal.
  - (* Discrete a → a ≡ b ⊣⊢ ⌜a ≡ b⌝ *)
    intros A a b ?. unseal; split=> n x y ??; by apply (discrete_iff n).
  - by unseal. 
  - by unseal. 
  - apply later_mono.
  - apply later_intro.
  - intros. unseal; by split=> -[|n] x y.
  - intros A Φ. unseal; split=> -[|[|n]] x y ?? Hsat /=; eauto.
    * by left. 
    * destruct Hsat as (a&?). right. by exists a.
    * destruct Hsat as (a&?). right. by exists a.
  - intros; by rewrite later_sep.
  - intros; by rewrite later_sep.
  - by unseal. 
  - by unseal. 
  - (* ▷ P ⊢ ▷ False ∨ (▷ False → P) *)
    intros P. unseal; split=> -[|n] x y ?? /= HP; [by left|right].
    intros [|n'] x' y' ????; [|done].
    eapply (uPred_mono _ _ _ x); eauto.
    eapply (uPred_closed _ _ n); eauto.
    * lia. 
    * eapply cmra_validN_le; eauto.
    * by apply cmra_included_includedN.
Qed.

Canonical Structure uPredI (M : ucmraT) : bi :=
  {| bi_ofe_mixin := ofe_mixin_of (uPred M); bi_bi_mixin := uPred_bi_mixin M |}.
Canonical Structure uPredSI (M : ucmraT) : sbi :=
  {| sbi_ofe_mixin := ofe_mixin_of (uPred M);
     sbi_bi_mixin := uPred_bi_mixin M; sbi_sbi_mixin := uPred_sbi_mixin M |}.

Global Notation "■ φ" := (uPred_pure φ%stdpp%type)
  (at level 20, right associativity) : bi_scope.
Global Notation "⧆ P" := (bi_affinely P%I)
  (at level 20, right associativity) : bi_scope.
Global Notation "⧈ P" := (bi_intuitionistically P%I)
  (at level 20, right associativity) : bi_scope.
Global Notation "✓ x" := (uPred_valid x) (at level 20) : bi_scope.
Infix "★" := bi_sep : bi_scope.
Notation "P -★ Q" := (bi_wand P Q) : bi_scope.
Notation "■ φ" := (bi_pure φ%stdpp%type)
  (at level 20, right associativity) : bi_scope.

Lemma ownM_op {M: ucmraT} (a1 a2 : M) :
  uPred_ownM (a1 ⋅ a2) ⊣⊢ uPred_ownM a1 ∗ uPred_ownM a2.
Proof.
  unseal; split=> n x y ??; split.
  - intros [z ?]; exists a1, (a2 ⋅ z), y, (core y); 
    split_and!; [by rewrite (assoc op)| by rewrite cmra_core_r | |].
    * by exists (core a1); rewrite cmra_core_r. 
    * by exists z.
  - intros (y1&y2&q1&q2&Hx&Hy&[z1 Hy1]&[z2 Hy2]); exists (z1 ⋅ z2).
    by rewrite (assoc op _ z1) -(comm op z1) (assoc op z1)
      -(assoc op _ a2) (comm op z1) -Hy1 -Hy2.
Qed.

Lemma ownM_op'' {M: ucmraT} (a1 a2 : M) :
  ⧆ uPred_ownM (a1 ⋅ a2) ⊣⊢ (⧆ uPred_ownM a1 ∗ ⧆ uPred_ownM a2).
Proof.
  unseal; split=> n x y ??; split.
  - unseal. intros [Heqy [z ?]]. exists a1, (a2 ⋅ z), y, (∅ : M) ; 
    split_and!; [by rewrite (assoc op)| by rewrite right_id | |].
    * rewrite Heqy; split; auto; unfold uPred_holds => //=.
    * split; auto; unfold uPred_holds => //=.
      by exists z.
  - unseal. intros (y1&y2&q1&q2&Hx&Hy&(Hq1&[z1 Hy1])&(Hq2&[z2 Hy2])). 
    split.
    * rewrite Hy Hq1 Hq2 right_id //=. unfold uPred_holds => //=.
    * exists (z1 ⋅ z2).
        by rewrite (assoc op _ z1) -(comm op z1) (assoc op z1)
           -(assoc op _ a2) (comm op z1) -Hy1 -Hy2.
Qed.

Global Instance ownM_timeless {M: ucmraT} (a : M) : Discrete a → Timeless (uPred_ownM a).
Proof.
  intros. rewrite /Timeless. 
  unseal. split=> n x y ?? HP.
  destruct n => //=; rewrite /sbi_except_0; unseal => //=.
  * left. auto.
  * right. apply cmra_included_includedN, cmra_timeless_included_l; eauto.
    ** eapply cmra_validN_le; eauto. lia.
    ** destruct HP as (x'&?). apply cmra_includedN_le with n.
       *** exists x'. eauto.
       *** lia.
Qed.

Lemma ownM_empty {M: ucmraT} : emp ⊢ ⧆uPred_ownM (∅: M).
Proof. unseal; split=> n x ????. unseal. split; auto; exists x. by rewrite left_id. Qed.

Lemma ownMl_valid {M: ucmraT} (a : M) : uPred_ownMl a ⊢ (uPred_ownMl a ★ ⧆✓ a).
Proof.
  unseal; split=> n x y Hv //= ? Heq.
  exists x, (core x), y, (∅ : M). rewrite ?cmra_core_r ?right_id. split_and!; auto.
  unseal. split; rewrite /uPred_holds //= in Heq *. by ofe_subst.
Qed.

Lemma ownMl_empty {M: ucmraT} : emp ⊣⊢ uPred_ownMl (∅: M).
Proof. unseal; split=> n x y ??. rewrite /uPred_holds //=. Qed.

Lemma uPred_affine_bi_affinely {M: ucmraT} (P: uPred M):
  uPred_affine P ⊣⊢  bi_affinely P.
Proof.
  rewrite affine_equiv. rewrite comm. by repeat unseal.
Qed.

Global Instance valid_persistent {M: ucmraT} {A : cmraT} (a : A) :
  Persistent ((⧆✓ a) : uPred M)%I.
Proof.
  intros; rewrite /Persistent. rewrite -uPred_affine_bi_affinely.
  rewrite -{1}relevant_valid. unfold_bi. rewrite -unlimited_persistent.
  by rewrite relevant_affine affine_affine_idemp.
Qed.

Global Instance ownM_persistent {M: ucmraT} (a: M) :
  cmra.Persistent a → Persistent (⧆(@uPred_ownM M a)).
Proof.
  intros; rewrite /Persistent. rewrite -uPred_affine_bi_affinely.
  unfold_bi. rewrite -unlimited_persistent.
  rewrite relevant_affine unlimited_ownM affine_affine_idemp //=.
Qed.
Global Instance ownMl_relevant {M: ucmraT} (a: M) :
  cmra.Persistent a → Persistent (⧆(@uPred_ownMl M a)).
Proof.
  intros; rewrite /Persistent. rewrite -uPred_affine_bi_affinely.
  unfold_bi. rewrite -unlimited_persistent.
  rewrite relevant_affine relevant_ownMl affine_affine_idemp //=.
Qed.

Class ATimeless {PROP: sbi} (P: PROP)
  := atimeless : ⧆▷ P ⊢ ◇ ⧆ P.
Arguments ATimeless {_} _%I.
Arguments atimeless {_} _%I {_}.

Section ATimeless_sbi.
  Context {PROP: sbi}.
  Implicit Types P Q : PROP. 

  Lemma timeless_atimeless P: Timeless P → ATimeless P.
  Proof.
    rewrite /Timeless /ATimeless. intros ->. apply bi.except_0_affinely_2.
  Qed.
  Import bi.

  Global Instance and_atimeless P Q: ATimeless P → ATimeless Q → ATimeless (P ∧ Q).
  Proof.
    intros; rewrite /ATimeless affinely_and except_0_and later_and !affinely_and.
    auto using and_mono.
  Qed.
  Global Instance or_atimeless P Q : ATimeless P → ATimeless Q → ATimeless (P ∨ Q).
  Proof.
    intros; rewrite /ATimeless affinely_or except_0_or later_or affinely_or.
    auto using or_mono.
  Qed.

  (*
Global Instance impl_atimeless P Q : ATimeless Q → ATimeless (P → Q).
Proof.
  rewrite /ATimeless=> HQ. rewrite later_false_em.
  rewrite affinely_or.
  apply or_mono.
  { rewrite affinely_elim.  done. }
  apply affinely_mono, impl_intro_l.
  rewrite -{2}(löb Q); apply impl_intro_l.
  rewrite HQ /sbi_except_0. !and_or_r. apply or_elim; last auto.
  by rewrite assoc (comm _ _ P) -assoc !impl_elim_r.
  rewrite !atimelessP_spec; unseal=> HP [|n] x ? HPQ  [|n'] x' ?????; auto.
  apply HP, HPQ, uPred_closed with (S n'); eauto using cmra_validN_le.
Qed.
*)
  Global Instance sep_atimeless P Q: Timeless P → Timeless Q → ATimeless (P ∗ Q).
  Proof.
    intros; rewrite /ATimeless. apply timeless_atimeless; apply _. 
  Qed.
  Global Instance affinely_atimeless P :
    ATimeless P → ATimeless (bi_affinely P).
  Proof.
    intros HP; rewrite /ATimeless.
    rewrite {1}(affinely_elim P).
    rewrite atimeless. apply except_0_mono.
      by rewrite affine_affinely.
  Qed.

End ATimeless_sbi.

Section ATimeless_uPred.
  Import bi.
  Context (M: ucmraT).
  Implicit Types P Q : uPred M.

  Lemma atimeless_spec P : ATimeless P ↔ ∀ n x, ✓{n} x → P 0 x ∅ → P n x ∅.
  Proof.
    split.
    - intros HP n x ??; induction n as [|n]; auto.
      move: HP; rewrite /ATimeless/sbi_except_0; unseal=> /uPred_in_entails /(_ (S n) x ∅).
      unseal => //=.
      destruct 1 as [ HlP | HlF]; auto using cmra_validN_S, ucmra_unit_validN.
      * split; rewrite /uPred_holds => //=. 
          by apply IHn, cmra_validN_S.
      * destruct HlP. 
      * destruct HlF as [? ?]; auto. 
    - move=> HP; rewrite /ATimeless/sbi_except_0; unseal.
      split=> -[|n] x y /=; auto; unseal; intros ?? (Haff&HP').
      * left. split; auto.
      * right. split; auto. rewrite Haff.
        apply HP, uPred_closed with n; eauto using cmra_validN_le, ucmra_unit_validN.
        ** rewrite -(dist_le _ _ _ _ Haff); auto with *.
        ** lia.
        ** eapply cmra_validN_le; eauto; lia.
  Qed.

  Global Instance forall_atimeless {A} (Ψ : A → uPred M) :
    (∀ x, ATimeless (Ψ x)) → ATimeless (∀ x, Ψ x).
  Proof. by setoid_rewrite atimeless_spec; unseal=> HΨ n x ?? a; apply HΨ. Qed.
  Global Instance exist_atimeless {A} (Ψ : A → uPred M) :
    (∀ x, ATimeless (Ψ x)) → ATimeless (∃ x, Ψ x).
  Proof.
      by setoid_rewrite atimeless_spec;
        unseal=> HΨ [|n] x ?; [|intros [a ?]; exists a; apply HΨ].
  Qed.
  Lemma except_0_relevant P:  uPred_relevant (sbi_except_0 P) ⊢ sbi_except_0 (uPred_relevant P).
  Proof.
    rewrite /sbi_except_0. unfold_bi.
    rewrite relevant_or.
    rewrite relevant_later_1. apply or_mono; auto. apply later_mono => //=.
      by rewrite relevant_elim.
  Qed.
  Global Instance persistently_atimeless P : ATimeless P → ATimeless (□ P).
  Proof.
    rewrite /ATimeless /bi_intuitionistically.
    rewrite -?uPred_affine_bi_affinely.
    rewrite -unlimited_affine_persistent.
    rewrite -relevant_affine affine_relevant_later.
    rewrite -relevant_affine.
    rewrite ?uPred_affine_bi_affinely => H.
    rewrite relevant_mono; last first.
    { apply affinely_mono. eauto. }
    rewrite -?uPred_affine_bi_affinely.
    rewrite -relevant_affine affine_affine_idemp.
    rewrite -except_0_relevant. apply relevant_mono.
    rewrite ?(uPred_affine_bi_affinely).
    rewrite {1}(affinely_elim P) H. done.
  Qed.

  Global Instance eq_atimeless {A : ofeT} (a b : A) :
    Discrete a → ATimeless (a ≡ b : uPred M)%IP.
  Proof. intros. apply timeless_atimeless; apply _. Qed.
  Global Instance ownM_atimeless (a : M) : Discrete a → ATimeless (uPred_ownM a).
  Proof. intros. apply timeless_atimeless; apply _. Qed.

  Global Instance pure_atimeless φ : ATimeless (bi_pure φ : uPred M).
  Proof. apply timeless_atimeless, _. Qed.

  Global Instance into_sep_affinely_later P Q1 Q2 :
    IntoSep P Q1 Q2 → Affine Q1 → Affine Q2 →
    IntoSep (bi_affinely (▷ P)) (bi_affinely (▷ Q1)) (bi_affinely (▷ Q2)).
  Proof.
    intros Hsep HA1 HA2.
    rewrite /IntoSep.
    rewrite Hsep.
    rewrite -(affine_affinely Q1) -{1}(affine_affinely Q1).
    rewrite -(affine_affinely Q2) -{1}(affine_affinely Q2).
    rewrite -!uPred_affine_bi_affinely.
    unfold_bi. by rewrite affine_later_distrib.
  Qed.
End ATimeless_uPred.

Global Instance into_except_0_affine_later {PROP : sbi} (P : PROP):
  ATimeless P → IntoExcept0 (⧆▷ P) (⧆P).
Proof.
    by rewrite /ATimeless/IntoExcept0 => ->.
Qed.

Global Instance into_pure_valid {M: ucmraT} `{CMRADiscrete A} (a : A) :
  IntoPure (✓ a : uPred M)%I (✓ a).
Proof. by rewrite /IntoPure discrete_valid. Qed.

Lemma later_sep_affine_l {M: ucmraT} (P Q : uPred M):
  (▷ (⧆ P ★ Q)) ⊣⊢ (⧆ ▷ P ★ ▷ Q).
Proof.
  rewrite -?uPred_affine_bi_affinely. unfold_bi. apply later_sep_affine_l.
Qed.

Lemma pure_elim_sep_l {M: ucmraT} (φ : Prop) (Q R: uPred M) : (φ → Q ⊢ R) → ⧆■ φ ★ Q ⊢ R.
Proof. rewrite -?uPred_affine_bi_affinely. unfold_bi.
  intros H. eapply (pure_elim_spare φ _ Q); auto.
  rewrite ?uPred_affine_bi_affinely.
  iIntros (?) "(?&?)". iApply H; eauto.
Qed.

Lemma sep_affine_3 {M: ucmraT} (P Q: uPred M):
   (⧆ (⧆ P ★ Q)) ⊢ (⧆ P ★ ⧆ Q).
Proof.
  rewrite -?uPred_affine_bi_affinely; unfold_bi. apply sep_affine_3.
Qed.

Tactic Notation "iApply" open_constr(lem) "in" open_constr(H) :=
  let Hfresh := iFresh in
  iRename H into Hfresh; iPoseProof lem as H; iSpecialize (H with Hfresh).

Lemma envs_entails_entail {PROP: bi} Δ (P Q: PROP):
  envs_entails Δ P → (P ⊢ Q) → envs_entails Δ Q.
Proof. by rewrite envs_entails_eq => -> ->. Qed.

Lemma of_envs_envs_entails {PROP: bi} Δ (P: PROP):
  envs_entails Δ P → (of_envs Δ ⊢ P).
Proof. by rewrite envs_entails_eq => ->. Qed.

Lemma envs_entails_of_envs_refl {PROP : bi} Δ:
  envs_entails Δ (of_envs Δ : PROP).
Proof. rewrite envs_entails_eq //=. Qed.


Section gmap.
  Context `{Countable K} {A : Type} {M: ucmraT}.
  Implicit Types m : gmap K A.
  Implicit Types Φ Ψ : K → A → uPred M.
  Existing Instance gmap_finmap.

  Notation "'[★' 'map]' k ↦ x ∈ m , P" := (big_opM bi_sep (λ k x, P) m)
  (at level 200, m at level 10, k, x at level 1, right associativity,
   format "[★  map]  k ↦ x  ∈  m ,  P") : bi_scope.
  Notation "'[★' 'map]' x ∈ m , P" := (big_opM bi_sep (λ _ x, P) m)
  (at level 200, m at level 10, x at level 1, right associativity,
   format "[★  map]  x  ∈  m ,  P") : bi_scope.

  Lemma map_to_list_union m1 m2:
    dom (gset K) m1 ∩ dom (gset K) m2 ≡ ∅ →
    Permutation (map_to_list (m1 ∪ m2)) (map_to_list m1 ++ map_to_list m2).
  Proof.
    intros Hdom.
    apply (anti_symm (submseteq)).
    - apply NoDup_submseteq; first by apply NoDup_map_to_list. 
      intros (x, a). rewrite elem_of_map_to_list.
      rewrite lookup_union_Some_raw.
      set_unfold.
      intros [?|?].
      * left. apply elem_of_map_to_list; naive_solver.
      * right. apply elem_of_map_to_list; naive_solver.
    - apply NoDup_submseteq. apply NoDup_app.
      * split_and!; try by apply  NoDup_map_to_list.
        intros (x, a). rewrite ?elem_of_map_to_list.
        intros Hin1 Hin2.
        assert (x ∈ dom  (gset K) m1).
        { apply elem_of_dom; eauto. }
        assert (x ∈ dom  (gset K) m2).
        { apply elem_of_dom; eauto. }
        set_solver.
      * intros (x, a). set_unfold. 
        rewrite ?elem_of_map_to_list. 
        intros [Hleft|Hright]; apply lookup_union_Some_raw. 
        ** naive_solver.
        ** specialize (Hdom x). rewrite ?elem_of_dom Hright in Hdom *=>Hdom'.
           assert (m1 !! x = None).
           { destruct (m1 !! x); auto. exfalso. naive_solver. }
           naive_solver.
  Qed.
    
  Lemma big_sepM_union Φ m1 m2 :
    dom (gset K) m1 ∩ dom (gset K) m2 ≡ ∅ →
    ([∗ map] k↦x ∈ m1, Φ k x) ★ ([∗ map] k↦x ∈ m2, Φ k x)
      ⊣⊢ [∗ map] k↦x ∈ (m1 ∪ m2), Φ k x.
  Proof.
    intros Hdom. rewrite /big_opM //.
    rewrite map_to_list_union; eauto.
    induction (map_to_list m1).
    - by rewrite //= left_id.
    - simpl. rewrite -assoc. apply sep_proper; auto.
  Qed.

  Lemma map_union_least m1 m2 m3:
    m1 ⊆ m3 →
    m2 ⊆ m3 →
    m1 ∪ m2 ⊆ m3.
  Proof.
    intros Hsub1 Hsub2.
    apply map_subseteq_spec.
    intros i x Hlook%lookup_union_Some_raw.
    destruct Hlook as [Hlook1|(?&Hlook2)].
    - eapply (map_subseteq_spec m1); eauto.
    - eapply (map_subseteq_spec m2); eauto.
  Qed.

  (* This could probably be cleaned up... I later saw there is a difference operation
     on maps? *)
  Lemma big_sepM_split Φ m m1 m2 :
    m1 ⊆ m →
    m2 ⊆ m →
    dom (gset K) m1 ∩ dom (gset K) m2 ≡ ∅ →
    ∃ m3,
      ([★ map] k↦x ∈ m, Φ k x)
        ⊣⊢ ([★ map] k↦x ∈ m1, Φ k x) ★
          ([★ map] k↦x ∈ m2, Φ k x) ★
          ([★ map] k↦x ∈ m3, Φ k x).
  Proof.
    intros Hsub1 Hsub2 Hdom.
    pose (m3 := (list_to_map (filter (λ p, p.1 ∉ dom (gset K) m1 ∧ p.1 ∉ dom (gset K) m2)
                                (map_to_list m)) : gmap K A)).
    exists m3.
    rewrite assoc.
    rewrite big_sepM_union; auto.
    rewrite big_sepM_union; auto.
    - cut (m = m1 ∪ m2 ∪ m3).
      { by intros <-. }
      assert (m3 ⊆ m) as Hsub3.
      { apply map_subseteq_spec=> x a.
        rewrite /m3. intros Hlook3.
        apply elem_of_list_to_map_2, elem_of_list_filter in Hlook3.
        rewrite elem_of_map_to_list in Hlook3 *. naive_solver.
      }
      apply (anti_symm (⊆)).
      * apply map_subseteq_spec=>x a Hlook.
        apply lookup_union_Some.
        ** apply (map_disjoint_dom (D:=gset K)).
           set_unfold=>x'. rewrite dom_union_L=>Hin_12. 
           rewrite ?elem_of_dom //=.
           inversion 1 as (a'&Hlook3).
           rewrite /m3 in Hlook3. 
           apply elem_of_list_to_map_2, elem_of_list_filter in Hlook3.
           set_unfold. naive_solver.
        ** rewrite lookup_union_Some; last first.
           { apply (map_disjoint_dom (D:= gset K)). set_unfold. set_solver. }
           case_eq (m1 !! x). 
           { intros ? Hlook'. eapply map_subseteq_spec in Hlook'; eauto. 
             rewrite Hlook in Hlook'. inversion Hlook'. naive_solver. }
           intros Hnone1. case_eq (m2 !! x).
           { intros ? Hlook'. eapply map_subseteq_spec in Hlook'; eauto. 
             rewrite Hlook in Hlook'. inversion Hlook'. naive_solver. }
           intros Hnone2.
           assert (is_Some (m3 !! x)) as (a'&Hlook').
           { rewrite /is_Some. case_eq (m3 !! x); first eauto.
             rewrite /m3. intros Hnot%not_elem_of_list_to_map_2.
             exfalso; apply Hnot. apply elem_of_list_fmap.
             exists (x, a); split; auto.
             apply elem_of_list_filter. rewrite elem_of_map_to_list; split; auto.
             rewrite ?not_elem_of_dom. naive_solver. }
           rewrite Hlook'. eapply map_subseteq_spec in Hlook'; eauto. 
           rewrite Hlook in Hlook'. inversion Hlook'. naive_solver.
      * apply map_union_least; auto.
        apply map_union_least; auto. 
    - set_unfold=>x'. rewrite dom_union_L. intros (Hin_12&Hin_3). 
      rewrite elem_of_dom //= in Hin_3 *.
      inversion 1 as (a'&Hlook3).
      rewrite /m3 in Hlook3. 
      apply elem_of_list_to_map_2, elem_of_list_filter in Hlook3.
      set_unfold. naive_solver.
  Qed.
End gmap.
