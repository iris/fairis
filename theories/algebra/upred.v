From fri.algebra Require Export cmra.
From iris.bi Require derived_connectives updates.
Local Hint Extern 1 (_ ≼ _) => etrans; [eassumption|].
Local Hint Extern 1 (_ ≼ _) => etrans; [|eassumption].
(* Local Hint Extern 1 (_ ≡{_}≡ _) => reflexivity. *)
Local Hint Extern 10 (_ ≤ _) => lia.
Set Default Proof Using "Type".

Record uPred (M : ucmraT) : Type := IProp {
  uPred_holds :> nat → M → M → Prop;
  uPred_mono n x1 y1 x2 y2:
    uPred_holds n x1 y1 → x1 ≼{n} x2 → y1 ≡{n}≡ y2 → uPred_holds n x2 y2;    
  uPred_closed n1 n2 x y :
    uPred_holds n1 x y → n2 ≤ n1 → ✓{n2} x → ✓{n2} y → uPred_holds n2 x y
}.
Arguments uPred_holds {_} _ _ _ _ : simpl never.
Add Printing Constructor uPred.
Instance: Params (@uPred_holds) 3 := {}.

Delimit Scope uPred_scope with IP.
Bind Scope uPred_scope with uPred.
Arguments uPred_holds {_} _%IP _ _ _.

Section cofe.
  Context {M : ucmraT}.

  Inductive uPred_equiv' (P Q : uPred M) : Prop :=
    { uPred_in_equiv : ∀ n x y, ✓{n} x → ✓{n} y → P n x y ↔ Q n x y }.
  Instance uPred_equiv : Equiv (uPred M) := uPred_equiv'.
  Inductive uPred_dist' (n : nat) (P Q : uPred M) : Prop :=
    { uPred_in_dist : ∀ n' x y, n' ≤ n → ✓{n'} x → ✓{n'} y → P n' x y ↔ Q n' x y}.
  Instance uPred_dist : Dist (uPred M) := uPred_dist'.
  Definition uPred_cofe_mixin : OfeMixin (uPred M).
  Proof.
    split.
    - intros P Q; split.
      + by intros HPQ n; split=> i x ??; apply HPQ.
      + intros HPQ; split=> n x ?; apply HPQ with n; auto.
    - intros n; split.
      + by intros P; split=> x i.
      + by intros P Q HPQ; split=> x i ??; symmetry; apply HPQ.
      + intros P Q Q' HP HQ; split=> i x y ???.
        by trans (Q i x y);[apply HP|apply HQ].
    - intros n P Q HPQ; split=> i x ??; apply HPQ; auto.
  Qed.
  Canonical Structure uPredO : ofeT := OfeT (uPred M) uPred_cofe_mixin.

  Program Definition uPred_compl : Compl uPredO := λ c,
    {| uPred_holds n x := c n n x |}.
  Next Obligation. naive_solver eauto using uPred_mono. Qed.
  Next Obligation.
    intros c n1 n2 x y ????; simpl in *.
    apply (chain_cauchy c n2 n1); eauto using uPred_closed.
  Qed.
  Global Program Instance uPred_cofe : Cofe uPredO := {| compl := uPred_compl |}.
  Next Obligation.
    intros n c; split=>i x ??; symmetry; apply (chain_cauchy c i n); auto.
  Qed.

End cofe.
Arguments uPredO : clear implicits.

Instance uPred_ne {M} (P : uPred M) n : Proper (dist n ==> dist n ==> iff) (P n).
Proof. intros x1 x2 Hx y1 y2 Hy; split=> ?; eapply uPred_mono; eauto; by rewrite Hx. Qed.
Instance uPred_proper {M} (P : uPred M) n : Proper ((≡) ==> (≡) ==> iff) (P n).
Proof. by intros x1 x2 Hx y1 y2 Hy; apply uPred_ne; apply equiv_dist. Qed.

Lemma uPred_holds_ne {M} (P Q : uPred M) n1 n2 x y :
  P ≡{n2}≡ Q → n2 ≤ n1 → ✓{n2} x → ✓{n2} y → Q n1 x y → P n2 x y.
Proof.
  intros [Hne] ????. eapply Hne; try done.
  eapply uPred_closed; eauto using cmra_validN_le.
Qed.

(** functor *)
Program Definition uPred_map {M1 M2 : ucmraT} (f : M2 -n> M1)
  `{!CMRAMonotone f} (P : uPred M1) :
  uPred M2 := {| uPred_holds n x y := P n (f x) (f y)|}.
Next Obligation. 
  intros M1 M2 f ? P n y1 y2 x1 x2 ? Hx Hy. rewrite /=.
  rewrite -Hy. naive_solver eauto using uPred_mono, cmra_monotoneN.
Qed.
Next Obligation. naive_solver eauto using uPred_closed, validN_preserving. Qed.

Instance uPred_map_ne {M1 M2 : ucmraT} (f : M2 -n> M1)
  `{!CMRAMonotone f} n : Proper (dist n ==> dist n) (uPred_map f).
Proof.
  intros x1 x2 Hx; split=> n' y ??.
  split; apply Hx; auto using validN_preserving.
Qed.
Lemma uPred_map_id {M : ucmraT} (P : uPred M): uPred_map cid P ≡ P.
Proof. by split=> n x ?. Qed.
Lemma uPred_map_compose {M1 M2 M3 : ucmraT} (f : M1 -n> M2) (g : M2 -n> M3)
    `{!CMRAMonotone f, !CMRAMonotone g} (P : uPred M3):
  uPred_map (g ◎ f) P ≡ uPred_map f (uPred_map g P).
Proof. by split=> n x Hx. Qed.
Lemma uPred_map_ext {M1 M2 : ucmraT} (f g : M1 -n> M2)
      `{!CMRAMonotone f} `{!CMRAMonotone g}:
  (∀ x, f x ≡ g x) → ∀ x, uPred_map f x ≡ uPred_map g x.
Proof. intros Hf P; split=> n x y Hx Hy /=; by rewrite /uPred_holds /= ?Hf. Qed.
Definition uPredO_map {M1 M2 : ucmraT} (f : M2 -n> M1) `{!CMRAMonotone f} :
  uPredO M1 -n> uPredO M2 := OfeMor (uPred_map f : uPredO M1 → uPredO M2).
Lemma uPredO_map_ne {M1 M2 : ucmraT} (f g : M2 -n> M1)
    `{!CMRAMonotone f, !CMRAMonotone g} n :
  f ≡{n}≡ g → uPredO_map f ≡{n}≡ uPredO_map g.
Proof.
  by intros Hfg P; split=> n' x y ???;
    rewrite /uPred_holds /= ?(dist_le _ _ _ _(Hfg x)) //= ?(dist_le _ _ _ _(Hfg y)); last lia.
Qed.

Program Definition uPredOF (F : urFunctor) : oFunctor := {|
  oFunctor_car A _ B _ := uPredO (urFunctor_car F B A);
  oFunctor_map A1 _ A2 _ B1 _ B2 _ fg := uPredO_map (urFunctor_map F (fg.2, fg.1))
|}.
Next Obligation.
  intros F A1 ? A2 ? B1 ? B2 ? n P Q HPQ.
  apply uPredO_map_ne, urFunctor_ne; split; by apply HPQ.
Qed.
Next Obligation.
  intros F A ? B ? P; simpl. rewrite -{2}(uPred_map_id P).
  apply uPred_map_ext=>y. by rewrite urFunctor_id.
Qed.
Next Obligation.
  intros F A1 ? A2 ? A3 ? B1 ? B2 ? B3 ? f g f' g' P; simpl. rewrite -uPred_map_compose.
  apply uPred_map_ext=>y; apply urFunctor_compose.
Qed.

Instance uPredOF_contractive F :
  urFunctorContractive F → oFunctorContractive (uPredOF F).
Proof.
  intros ? A1 ? A2 ? B1 ? B2 ? n P Q HPQ. apply uPredO_map_ne, urFunctor_contractive.
  destruct n; split; by apply HPQ.
Qed.

(** logical entailement *)
Inductive uPred_entails {M} (P Q : uPred M) : Prop :=
  { uPred_in_entails : ∀ n x y, ✓{n} x → ✓{n} y → P n x y → Q n x y}.
Hint Extern 0 (uPred_entails _ _) => reflexivity : core.
Instance uPred_entails_rewrite_relation M : RewriteRelation (@uPred_entails M) := {}.

Hint Resolve uPred_mono uPred_closed : uPred_def.

(** logical connectives *)
Program Definition uPred_pure_def {M} (φ : Prop) : uPred M :=
  {| uPred_holds n x y := φ|}.
Solve Obligations with (intros; ofe_subst; done).

Definition uPred_pure_aux : { x | x = @uPred_pure_def}. by eexists. Qed.
Definition uPred_pure {M} := proj1_sig uPred_pure_aux M.
Definition uPred_pure_eq :
  @uPred_pure = @uPred_pure_def := proj2_sig uPred_pure_aux.

Instance uPred_inhabited M : Inhabited (uPred M) := populate (uPred_pure True).

Program Definition uPred_and_def {M} (P Q : uPred M) : uPred M :=
  {| uPred_holds n x y := P n x y ∧ Q n x y|}.
Solve Obligations with naive_solver eauto 2 with uPred_def.
Definition uPred_and_aux : { x | x = @uPred_and_def }. by eexists. Qed.
Definition uPred_and {M} := proj1_sig uPred_and_aux M.
Definition uPred_and_eq: @uPred_and = @uPred_and_def := proj2_sig uPred_and_aux.

Program Definition uPred_or_def {M} (P Q : uPred M) : uPred M :=
  {| uPred_holds n x y := P n x y ∨ Q n x y |}.
Solve Obligations with naive_solver eauto 2 with uPred_def.
Definition uPred_or_aux : { x | x = @uPred_or_def }. by eexists. Qed.
Definition uPred_or {M} := proj1_sig uPred_or_aux M.
Definition uPred_or_eq: @uPred_or = @uPred_or_def := proj2_sig uPred_or_aux.

Program Definition uPred_impl_def {M} (P Q : uPred M) : uPred M :=
  {| uPred_holds n x y := ∀ n' x',
       x ≼ x' → n' ≤ n → ✓{n'} x' → ✓{n'} y → P n' x' y → Q n' x' y |}.
Next Obligation.
  intros M P Q n1 x1 x1' y1 y1' HPQ [x2 Hx1'] Hx2' n2 x3 [x4 Hx3] ?; simpl in *.
  rewrite Hx3 (dist_le _ _ _ _ Hx1') // -(dist_le _ _ _ _ Hx2'); auto. intros ???.
  eapply HPQ; auto. exists (x2 ⋅ x4); by rewrite assoc.
Qed.
Next Obligation. intros M P Q [|n1] [|n2] x; auto with lia. Qed.
Definition uPred_impl_aux : { x | x = @uPred_impl_def }. by eexists. Qed.
Definition uPred_impl {M} := proj1_sig uPred_impl_aux M.
Definition uPred_impl_eq :
  @uPred_impl = @uPred_impl_def := proj2_sig uPred_impl_aux.

Program Definition uPred_forall_def {M A} (Ψ : A → uPred M) : uPred M :=
  {| uPred_holds n x y := ∀ a, Ψ a n x y|}.
Solve Obligations with naive_solver eauto 2 with uPred_def.
Definition uPred_forall_aux : { x | x = @uPred_forall_def }. by eexists. Qed.
Definition uPred_forall {M A} := proj1_sig uPred_forall_aux M A.
Definition uPred_forall_eq :
  @uPred_forall = @uPred_forall_def := proj2_sig uPred_forall_aux.

Program Definition uPred_exist_def {M A} (Ψ : A → uPred M) : uPred M :=
  {| uPred_holds n x y := ∃ a, Ψ a n x y|}.
Solve Obligations with naive_solver eauto 2 with uPred_def.
Definition uPred_exist_aux : { x | x = @uPred_exist_def }. by eexists. Qed.
Definition uPred_exist {M A} := proj1_sig uPred_exist_aux M A.
Definition uPred_exist_eq: @uPred_exist = @uPred_exist_def := proj2_sig uPred_exist_aux.

Program Definition uPred_eq_def {M} {A : ofeT} (a1 a2 : A) : uPred M :=
  {| uPred_holds n x y := a1 ≡{n}≡ a2|}.
Solve Obligations with naive_solver eauto 2 using (dist_le (A:=A)).
Definition uPred_eq_aux : { x | x = @uPred_eq_def }. by eexists. Qed.
Definition uPred_eq {M A} := proj1_sig uPred_eq_aux M A.
Definition uPred_eq_eq: @uPred_eq = @uPred_eq_def := proj2_sig uPred_eq_aux.

Program Definition uPred_sep_def {M} (P Q : uPred M) : uPred M :=
  {| uPred_holds n x y := ∃ x1 x2 y1 y2, x ≡{n}≡ x1 ⋅ x2 ∧ y ≡{n}≡ y1 ⋅ y2 
                                       ∧ P n x1 y1 ∧ Q n x2 y2 |}.
Next Obligation.
  intros M P Q n x1 y1 x2 y2 (x1'&x2'&y1'&y2'&Hx&Hy&?&?) [z Hz] ?.
  exists x1', (x2' ⋅ z), y1', y2'. 
  split_and?; eauto using uPred_mono, cmra_includedN_l.
  - by rewrite Hz Hx assoc.
  - by rewrite -Hy.
Qed.
Next Obligation.
  intros M P Q n1 n2 x y (x1&x2&y1&y2&Hx&Hy&?&?) Hlt.
  rewrite {1}(dist_le _ _ _ _ Hx) // =>?.
  rewrite {1}(dist_le _ _ _ _ Hy) // =>?.
  exists x1, x2, y1, y2; ofe_subst; split_and!;
  eauto using dist_le, uPred_closed, cmra_validN_op_l, cmra_validN_op_r.
Qed.
Definition uPred_sep_aux : { x | x = @uPred_sep_def }. by eexists. Qed.
Definition uPred_sep {M} := proj1_sig uPred_sep_aux M.
Definition uPred_sep_eq: @uPred_sep = @uPred_sep_def := proj2_sig uPred_sep_aux.

Program Definition uPred_wand_def {M} (P Q : uPred M) : uPred M :=
  {| uPred_holds n x y := ∀ n' x' y',
       n' ≤ n → ✓{n'} (x ⋅ x') → ✓{n'} (y ⋅ y') → P n' x' y' → Q n' (x ⋅ x') (y ⋅ y') |}.
Next Obligation.
  intros M P Q n1 x1 y1 x2 y2 HPQ Hx Hy n2 x3 y3 ????; simpl in *.
  rewrite -(dist_le _ _ _ _ Hy) //.
  apply uPred_mono with (x1 ⋅ x3) (y1 ⋅ y3); first apply HPQ;
    eauto using cmra_validN_includedN, cmra_monoN_r, cmra_includedN_le.
  by rewrite (dist_le _ _ _ _ Hy).
Qed.
Next Obligation. naive_solver. Qed.
Definition uPred_wand_aux : { x | x = @uPred_wand_def }. by eexists. Qed.
Definition uPred_wand {M} := proj1_sig uPred_wand_aux M.
Definition uPred_wand_eq :
  @uPred_wand = @uPred_wand_def := proj2_sig uPred_wand_aux.

Program Definition uPred_relevant_def {M} (P : uPred M) : uPred M :=
  {| uPred_holds n x y:= P n (core x) (core y) ∧ y ≡{n}≡ core y |}.
Next Obligation.
  intros M P n x1 x2 y1 y2 (?&?) Hx Hy. rewrite -Hy.
  naive_solver eauto using uPred_mono, @cmra_core_monoN.
Qed.
Next Obligation. 
  intros M P n1 n2 x y (?&Hc) ???. 
  naive_solver eauto using uPred_closed, @cmra_core_mono, @cmra_core_validN, dist_le.
Qed.
Definition uPred_relevant_aux : { x | x = @uPred_relevant_def }. by eexists. Qed.
Definition uPred_relevant {M} := proj1_sig uPred_relevant_aux M.
Definition uPred_relevant_eq :
  @uPred_relevant = @uPred_relevant_def := proj2_sig uPred_relevant_aux.

Program Definition uPred_persistent_def {M} (P : uPred M) : uPred M :=
  {| uPred_holds n x y:= P n (core x) ∅ |}.
Next Obligation.
  intros M P n x1 x2 y1 y2 ? Hx Hy. 
  naive_solver eauto using uPred_mono, @cmra_core_monoN.
Qed.
Next Obligation. 
  intros M P n1 n2 x y ? ???. 
  rewrite //= in H. rewrite //=. 
  eapply uPred_closed; eauto.
  - apply @cmra_core_validN; eauto. 
  - apply ucmra_unit_validN.
Qed.
Definition uPred_persistent_aux : { x | x = @uPred_persistent_def }. by eexists. Qed.
Definition uPred_persistent {M} := proj1_sig uPred_persistent_aux M.
Definition uPred_persistent_eq :
  @uPred_persistent = @uPred_persistent_def := proj2_sig uPred_persistent_aux.

Program Definition uPred_affine_def {M} (P : uPred M) : uPred M :=
  {| uPred_holds n x y:= P n x y ∧ y ≡{n}≡ ∅ |}.
Next Obligation. 
  intros M P n x1 y1 x2 y2 (?&Hempty) Hx Hy.
  rewrite -Hy. naive_solver eauto 2 with uPred_def.
Qed.
Next Obligation.
  intros M P n1 n2 x y (?&?) ???. naive_solver eauto 2 using uPred_closed, dist_le.
Qed.
Definition uPred_affine_aux : { x | x = @uPred_affine_def }. by eexists. Qed.
Definition uPred_affine {M} := proj1_sig uPred_affine_aux M.
Definition uPred_affine_eq :
  @uPred_affine = @uPred_affine_def := proj2_sig uPred_affine_aux.

Program Definition uPred_later_def {M} (P : uPred M) : uPred M :=
  {| uPred_holds n x y := match n return _ with 0 => True | S n' => P n' x y end |}.
Next Obligation. 
  intros M P [|n] ??; eauto using uPred_mono, cmra_includedN_S,(dist_le (A:=M)). 
Qed.
Next Obligation.
  intros M P [|n1] [|n2] x1 x2 y1 y2; eauto using uPred_closed,cmra_validN_S; try lia.
Qed.
Definition uPred_later_aux : { x | x = @uPred_later_def }. by eexists. Qed.
Definition uPred_later {M} := proj1_sig uPred_later_aux M.
Definition uPred_later_eq :
  @uPred_later = @uPred_later_def := proj2_sig uPred_later_aux.

Program Definition uPred_ownM_def {M: ucmraT} (a : M) : uPred M :=
  {| uPred_holds n x y := a ≼{n} x|}.
Next Obligation.
  intros M a n x1 y1 x2 y2 [a' Hx1] [x2' ->] Hy;
    exists (a' ⋅ x2'). by rewrite (assoc op) Hx1.
Qed.
Next Obligation. naive_solver eauto using cmra_includedN_le. Qed.
Definition uPred_ownM_aux : { x | x = @uPred_ownM_def }. by eexists. Qed.
Definition uPred_ownM {M} := proj1_sig uPred_ownM_aux M.
Definition uPred_ownM_eq :
  @uPred_ownM = @uPred_ownM_def := proj2_sig uPred_ownM_aux.

Program Definition uPred_ownMl_def {M: ucmraT} (a : M) : uPred M :=
  {| uPred_holds n x y := a ≡{n}≡ y|}.
Next Obligation.
  intros M a n x1 y1 x2 y2 Hequiv Hx Hy.
  rewrite Hequiv //=.
Qed.
Next Obligation.
  intros M a n1 n2 x1 x2 Hequiv Hle ?.
  rewrite -(dist_le _ _ _ _ Hequiv) //=. 
Qed.
Definition uPred_ownMl_aux : { x | x = @uPred_ownMl_def }. by eexists. Qed.
Definition uPred_ownMl {M} := proj1_sig uPred_ownMl_aux M.
Definition uPred_ownMl_eq :
  @uPred_ownMl = @uPred_ownMl_def := proj2_sig uPred_ownMl_aux.

Program Definition uPred_valid_def {M : ucmraT} {A : cmraT} (a : A) : uPred M :=
  {| uPred_holds n x y := ✓{n} a|}.
Solve Obligations with (intros; ofe_subst; naive_solver eauto 2 using cmra_validN_le, dist_le).
Definition uPred_valid_aux : { x | x = @uPred_valid_def }. by eexists. Qed.
Definition uPred_valid {M A} := proj1_sig uPred_valid_aux M A.
Definition uPred_valid_eq :
  @uPred_valid = @uPred_valid_def := proj2_sig uPred_valid_aux.

Program Definition uPred_stopped_def {M:  ucmraT} : uPred M :=
  {| uPred_holds n x y :=  ∀ n', n' ≤ n → nf (stepN n') y|}.
Next Obligation.
  rewrite /nf /red; intros M n x1 x2 y1 y2 Hns Heq1 Heq2; simpl.
  intros n' Hle (y&Hs').
  eapply Hns; eauto. exists y. by rewrite (dist_le _ _ _ _ Heq2).
Qed.
Next Obligation. naive_solver. Qed.
Definition uPred_stopped_aux : { x | x = @uPred_stopped_def }. by eexists. Qed.
Definition uPred_stopped {M} := proj1_sig uPred_stopped_aux M.
Definition uPred_stopped_eq :
  @uPred_stopped = @uPred_stopped_def := proj2_sig uPred_stopped_aux.

Program Definition uPred_emp_def {M: ucmraT} : uPred M :=
  {| uPred_holds n x y := y ≡{n}≡ ∅ |}.
Solve Obligations with (intros; ofe_subst; naive_solver eauto 2 using cmra_validN_le, dist_le).
Definition uPred_emp_aux : { x | x = @uPred_emp_def }. by eexists. Qed.
Definition uPred_emp {M} := proj1_sig uPred_emp_aux M.
Definition uPred_emp_eq :
  @uPred_emp = @uPred_emp_def := proj2_sig uPred_emp_aux.


Local Notation "P ⊢ Q" := (uPred_entails P%IP Q%IP)
  (at level 99, Q at level 200, right associativity) : stdpp_scope.
Local Notation "(⊢)" := uPred_entails (only parsing) : stdpp_scope.
Local Notation "P ⊣⊢ Q" := (equiv (A:=uPred _) P%IP Q%IP)
  (at level 95, no associativity) : stdpp_scope.
Local Notation "(⊣⊢)" := (equiv (A:=uPred _)) (only parsing) : stdpp_scope.
Local Notation "■ φ" := (uPred_pure φ%stdpp%type)
  (at level 20, right associativity) : uPred_scope.
Notation "x = y" := (uPred_pure (x%stdpp%type = y%stdpp%type)) : uPred_scope.
Notation "x ## y" := (uPred_pure (x%stdpp%type ## y%stdpp%type)) : uPred_scope.
Notation "'False'" := (uPred_pure False) : uPred_scope.
Notation "'True'" := (uPred_pure True) : uPred_scope.
Notation "'Emp'" := (uPred_emp) : uPred_scope.
Infix "∧" := uPred_and : uPred_scope.
Notation "(∧)" := uPred_and (only parsing) : uPred_scope.
Infix "∨" := uPred_or : uPred_scope.
Notation "(∨)" := uPred_or (only parsing) : uPred_scope.
Infix "→" := uPred_impl : uPred_scope.
Infix "★" := uPred_sep (at level 80, right associativity) : uPred_scope.
Notation "(★)" := uPred_sep (only parsing) : uPred_scope.
Notation "P -★ Q" := (uPred_wand P Q)
  (at level 99, Q at level 200, right associativity) : uPred_scope.
Notation "∀ x .. y , P" :=
  (uPred_forall (λ x, .. (uPred_forall (λ y, P)) ..)%IP) : uPred_scope.
Notation "∃ x .. y , P" :=
  (uPred_exist (λ x, .. (uPred_exist (λ y, P)) ..)%IP) : uPred_scope.
Notation "□ P" := (uPred_relevant P)
  (at level 20, right associativity) : uPred_scope.
Notation "⧆ P" := (uPred_affine P)
  (at level 20, right associativity) : uPred_scope.
Notation "⧈ P" := (uPred_affine (uPred_relevant P))
  (at level 20, right associativity) : uPred_scope.
Notation "▷ P" := (uPred_later P)
  (at level 20, right associativity) : uPred_scope.
Infix "≡" := (uPred_eq) : uPred_scope.
Notation "✓ x" := (uPred_valid x) (at level 20) : uPred_scope.

Definition uPred_iff {M} (P Q : uPred M) : uPred M := ((P → Q) ∧ (Q → P))%IP.
Instance: Params (@uPred_iff) 1 := {}.
Infix "↔" := uPred_iff : uPred_scope.

Program Definition uPred_plainly_def {M} (P : uPred M) : uPred M :=
  {| uPred_holds n x y := P n ∅ ∅ |}.
Solve Obligations with naive_solver eauto using uPred_closed, ucmra_unit_validN.
Definition uPred_plainly_aux : { x | x = @uPred_plainly_def}. by eexists. Qed.
Definition uPred_plainly {M} := proj1_sig uPred_plainly_aux M.
Definition uPred_plainly_eq :
  @uPred_plainly = @uPred_plainly_def := proj2_sig uPred_plainly_aux.


Module uPred.
Module Unseal.
Definition unseal_eqs :=
  (uPred_pure_eq, uPred_and_eq, uPred_or_eq, uPred_impl_eq, uPred_forall_eq,
  uPred_exist_eq, uPred_eq_eq, uPred_sep_eq, uPred_wand_eq,
  uPred_plainly_eq, uPred_persistent_eq, uPred_later_eq, uPred_ownM_eq,
  uPred_valid_eq, uPred_relevant_eq, uPred_affine_eq, uPred_ownMl_eq, uPred_emp_eq, uPred_stopped_eq).
Import iris.bi.derived_connectives. 
Ltac unfold_bi := (* Coq unfold is used to circumvent bug #5699 in rewrite /foo *)
  unfold bi_emp; simpl;
  unfold (* uPred_emp, *) bi_pure, bi_and, bi_or, bi_impl, bi_forall, bi_exist,
  bi_sep, bi_wand, bi_persistently, bi_affinely, sbi_later; simpl;
  unfold sbi_emp, sbi_pure, sbi_and, sbi_or, sbi_impl, sbi_forall, sbi_exist,
  sbi_internal_eq, sbi_sep, sbi_wand, sbi_persistently; simpl.
                                                                          
Ltac unseal := unfold_bi; rewrite !unseal_eqs /=.
Ltac try_unseal := unfold_bi; rewrite ?unseal_eqs /=.
End Unseal.
Include Unseal.

Section uPred_logic.
Context {M : ucmraT}.
Implicit Types φ : Prop.
Implicit Types P Q : uPred M.
Implicit Types A : Type.
Local Notation "P ⊢ Q" := (@uPred_entails M P%IP Q%IP). (* Force implicit argument M *)
Local Notation "P ⊣⊢ Q" := (equiv (A:=uPred M) P%IP Q%IP). (* Force implicit argument M *)
Arguments uPred_holds {_} !_ _ _ _ /.
Hint Immediate uPred_in_entails : core.

Global Instance: PreOrder (@uPred_entails M).
Proof.
  split.
  * by intros P; split=> x i.
  * by intros P Q Q' HP HQ; split=> x y i ???; apply HQ, HP.
Qed.
Global Instance: AntiSymm (⊣⊢) (@uPred_entails M).
Proof. intros P Q HPQ HQP; split=> x n; by split; [apply HPQ|apply HQP]. Qed.
Lemma equiv_spec P Q : (P ⊣⊢ Q) ↔ (P ⊢ Q) ∧ (Q ⊢ P).
Proof.
  split; [|by intros [??]; apply (anti_symm (⊢))].
  intros HPQ; split; split=> x i; apply HPQ.
Qed.
Lemma equiv_entails P Q : (P ⊣⊢ Q) → (P ⊢ Q).
Proof. apply equiv_spec. Qed.
Lemma equiv_entails_sym P Q : (Q ⊣⊢ P) → (P ⊢ Q).
Proof. apply equiv_spec. Qed.
Global Instance entails_proper :
  Proper ((⊣⊢) ==> (⊣⊢) ==> iff) ((⊢) : relation (uPred M)).
Proof.
  move => P1 P2 /equiv_spec [HP1 HP2] Q1 Q2 /equiv_spec [HQ1 HQ2]; split; intros.
  - by trans P1; [|trans Q1].
  - by trans P2; [|trans Q2].
Qed.
Lemma entails_equiv_l (P Q R : uPred M) : (P ⊣⊢ Q) → (Q ⊢ R) → (P ⊢ R).
Proof. by intros ->. Qed.
Lemma entails_equiv_r (P Q R : uPred M) : (P ⊢ Q) → (Q ⊣⊢ R) → (P ⊢ R).
Proof. by intros ? <-. Qed.

(** Non-expansiveness and setoid morphisms *)
Global Instance pure_proper : Proper (iff ==> (⊣⊢)) (@uPred_pure M).
Proof. intros φ1 φ2 Hφ. by unseal; split=> -[|n] ?; try apply Hφ. Qed.
Global Instance and_ne n : Proper (dist n ==> dist n ==> dist n) (@uPred_and M).
Proof.
  intros P P' HP Q Q' HQ; unseal; split=> x n' ????.
  split; (intros [??]; split; [by apply HP|by apply HQ]).
Qed.
Global Instance and_proper :
  Proper ((⊣⊢) ==> (⊣⊢) ==> (⊣⊢)) (@uPred_and M) := ne_proper_2 _.
Global Instance or_ne n : Proper (dist n ==> dist n ==> dist n) (@uPred_or M).
Proof.
  intros P P' HP Q Q' HQ; split=> x n' ??.
  unseal; split; (intros [?|?]; [left; by apply HP|right; by apply HQ]).
Qed.
Global Instance or_proper :
  Proper ((⊣⊢) ==> (⊣⊢) ==> (⊣⊢)) (@uPred_or M) := ne_proper_2 _.
Global Instance impl_ne n :
  Proper (dist n ==> dist n ==> dist n) (@uPred_impl M).
Proof.
  intros P P' HP Q Q' HQ; split=> n' x y ???;
  by unseal; split; intros HPQ; intros n'' x' ?????;
  apply HQ, HPQ, HP; auto.
Qed.
Global Instance impl_proper :
  Proper ((⊣⊢) ==> (⊣⊢) ==> (⊣⊢)) (@uPred_impl M) := ne_proper_2 _.
Global Instance sep_ne n : Proper (dist n ==> dist n ==> dist n) (@uPred_sep M).
Proof.
  intros P P' HP Q Q' HQ; split=> n' x y ???.
  unseal; split; intros (x1&x2&y1&y2&?&?&?&?); ofe_subst x; ofe_subst y;
    exists x1, x2, y1, y2; split_and!; try (apply HP || apply HQ);
    eauto using cmra_validN_op_l, cmra_validN_op_r.
Qed.
Global Instance sep_proper :
  Proper ((⊣⊢) ==> (⊣⊢) ==> (⊣⊢)) (@uPred_sep M) := ne_proper_2 _.
Global Instance wand_ne n :
  Proper (dist n ==> dist n ==> dist n) (@uPred_wand M).
Proof.
  intros P P' HP Q Q' HQ; split=> n' x y ???; unseal; split; intros HPQ n'' x' y' ????;
    apply HQ, HPQ, HP; eauto using cmra_validN_op_r.
Qed.
Global Instance wand_proper :
  Proper ((⊣⊢) ==> (⊣⊢) ==> (⊣⊢)) (@uPred_wand M) := ne_proper_2 _.
Global Instance eq_ne (A : ofeT) n :
  Proper (dist n ==> dist n ==> dist n) (@uPred_eq M A).
Proof.
  intros x x' Hx y y' Hy; split=> n' z; unseal; split; intros; simpl in *.
  * by rewrite -(dist_le _ _ _ _ Hx) -?(dist_le _ _ _ _ Hy); auto.
  * by rewrite (dist_le _ _ _ _ Hx) ?(dist_le _ _ _ _ Hy); auto.
Qed.
Global Instance eq_proper (A : ofeT) :
  Proper ((≡) ==> (≡) ==> (⊣⊢)) (@uPred_eq M A) := ne_proper_2 _.
Global Instance forall_ne A n :
  Proper (pointwise_relation _ (dist n) ==> dist n) (@uPred_forall M A).
Proof.
  by intros Ψ1 Ψ2 HΨ; unseal; split=> n' x; split; intros HP a; apply HΨ.
Qed.
Global Instance forall_proper A :
  Proper (pointwise_relation _ (⊣⊢) ==> (⊣⊢)) (@uPred_forall M A).
Proof.
  by intros Ψ1 Ψ2 HΨ; unseal; split=> n' x; split; intros HP a; apply HΨ.
Qed.
Global Instance exist_ne A n :
  Proper (pointwise_relation _ (dist n) ==> dist n) (@uPred_exist M A).
Proof.
  intros Ψ1 Ψ2 HΨ.
  unseal; split=> n' x ??; split; intros [a ?]; exists a; by apply HΨ.
Qed.
Global Instance exist_proper A :
  Proper (pointwise_relation _ (⊣⊢) ==> (⊣⊢)) (@uPred_exist M A).
Proof.
  intros Ψ1 Ψ2 HΨ.
  unseal; split=> n' x ?; split; intros [a ?]; exists a; by apply HΨ.
Qed.
Global Instance later_contractive : Contractive (@uPred_later M).
Proof.
  unseal; intros [|n] P Q HPQ; split=> -[|n'] x y ??? //=; try lia.
  apply HPQ; eauto using cmra_validN_S.
Qed.
Global Instance later_proper' :
  Proper ((⊣⊢) ==> (⊣⊢)) (@uPred_later M) := ne_proper _.
Global Instance relevant_ne n : Proper (dist n ==> dist n) (@uPred_relevant M).
Proof.
  intros P1 P2 HP.
  unseal; split=> n' x y; split; 
  (intros (?&?); split; first apply HP; eauto using @cmra_core_validN).
Qed.
Global Instance relevant_proper :
  Proper ((⊣⊢) ==> (⊣⊢)) (@uPred_relevant M) := ne_proper _.
Global Instance persistent_ne n : Proper (dist n ==> dist n) (@uPred_persistent M).
Proof.
  intros P1 P2 HP.
  unseal; split=> n' x y.
  intros ???; apply HP; eauto using @cmra_core_validN, @ucmra_unit_validN.
Qed.
Global Instance persistent_proper :
  Proper ((⊣⊢) ==> (⊣⊢)) (@uPred_relevant M) := ne_proper _.
Global Instance affine_ne n : Proper (dist n ==> dist n) (@uPred_affine M).
Proof.
  intros P1 P2 HP.
  unseal; split=> n' x y ???.
  split; intros (?&?); split; auto; apply HP; eauto.
Qed.
Global Instance affine_proper :
  Proper ((⊣⊢) ==> (⊣⊢)) (@uPred_affine M) := ne_proper _.
Global Instance ownM_ne n : Proper (dist n ==> dist n) (@uPred_ownM M).
Proof.
  intros a b Ha.
  unseal; split=> n' x y ?? /=. by rewrite (dist_le _ _ _ _ Ha); last lia.
Qed.
Global Instance ownM_proper: Proper ((≡) ==> (⊣⊢)) (@uPred_ownM M) := ne_proper _.
Global Instance ownMl_ne n : Proper (dist n ==> dist n) (@uPred_ownMl M).
Proof.
  intros a b Ha.
  unseal; split=> n' x y ?? /=. by rewrite (dist_le _ _ _ _ Ha); last lia.
Qed.
Global Instance ownMl_proper: Proper ((≡) ==> (⊣⊢)) (@uPred_ownMl M) := ne_proper _.
Global Instance valid_ne {A : cmraT} n :
Proper (dist n ==> dist n) (@uPred_valid M A).
Proof.
  intros a b Ha; unseal; split=> n' x y ?? /=.
  by rewrite (dist_le _ _ _ _ Ha); last lia.
Qed.
Global Instance valid_proper {A : cmraT} :
  Proper ((≡) ==> (⊣⊢)) (@uPred_valid M A) := ne_proper _.

Global Instance plainly_ne n : Proper (dist n ==> dist n) (@uPred_plainly M).
Proof.
  intros P1 P2 HP.
  unseal; split=> n' x y; split; apply HP; eauto using ucmra_unit_validN.
Qed.
Global Instance plainly_proper :
  Proper ((⊣⊢) ==> (⊣⊢)) (@uPred_plainly M) := ne_proper _.

(** Introduction and elimination rules *)
Lemma pure_intro φ P : φ → P ⊢ ■ φ.
Proof. by intros ?; unseal; split. Qed.
Lemma pure_elim φ Q R : (Q ⊢ ■ φ) → (φ → Q ⊢ R) → Q ⊢ R.
Proof.
  unseal; intros HQP HQR; split=> n x y ?? HQ; apply HQR; eauto.
  eapply HQP; last apply HQ; eauto.
Qed.
Lemma and_elim_l P Q : P ∧ Q ⊢ P.
Proof. by unseal; split=> n x y ?? [??]. Qed.
Lemma and_elim_r P Q : P ∧ Q ⊢ Q.
Proof. by unseal; split=> n x y ?? [??]. Qed.
Lemma and_intro P Q R : (P ⊢ Q) → (P ⊢ R) → P ⊢ Q ∧ R.
Proof. intros HQ HR; unseal; split=> n x ??; by split; [apply HQ|apply HR]. Qed.
Lemma or_intro_l P Q : P ⊢ P ∨ Q.
Proof. unseal; split=> n x y ???; left; auto. Qed.
Lemma or_intro_r P Q : Q ⊢ P ∨ Q.
Proof. unseal; split=> n x y ???; right; auto. Qed.
Lemma or_elim P Q R : (P ⊢ R) → (Q ⊢ R) → P ∨ Q ⊢ R.
Proof. intros HP HQ; unseal; split=> n x y ?? [?|?]. by apply HP. by apply HQ. Qed.
Lemma impl_intro_r P Q R : (P ∧ Q ⊢ R) → P ⊢ Q → R.
Proof.
  unseal; intros HQ; split=> n x y ??? n' x' ?????.
  apply HQ; try naive_solver eauto using uPred_mono, uPred_closed, cmra_included_includedN.
Qed.
Lemma impl_elim P Q R : (P ⊢ Q → R) → (P ⊢ Q) → P ⊢ R.
Proof. by unseal; intros HP HP'; split=> n x y ???; apply HP with n x, HP'. Qed.
Lemma forall_intro {A} P (Ψ : A → uPred M): (∀ a, P ⊢ Ψ a) → P ⊢ ∀ a, Ψ a.
Proof. unseal; intros HPΨ; split=> n x y ??? a; by apply HPΨ. Qed.
Lemma forall_elim {A} {Ψ : A → uPred M} a : (∀ a, Ψ a) ⊢ Ψ a.
Proof. unseal; split=> n x y ?? HP; apply HP. Qed.
Lemma exist_intro {A} {Ψ : A → uPred M} a : Ψ a ⊢ ∃ a, Ψ a.
Proof. unseal; split=> n x y ???; by exists a. Qed.
Lemma exist_elim {A} (Φ : A → uPred M) Q : (∀ a, Φ a ⊢ Q) → (∃ a, Φ a) ⊢ Q.
Proof. unseal; intros HΦΨ; split=> n x y ?? [a ?]; by apply HΦΨ with a. Qed.

Lemma affine_equiv P: ⧆P ⊣⊢ (P ∧ Emp).
Proof.
  uPred.unseal; apply (anti_symm (⊢)); split=> n x y ?? => //=; naive_solver.
Qed.
Lemma affine_affine_idemp P : ⧆⧆P ⊣⊢ ⧆P. 
Proof. 
  rewrite ?affine_equiv. apply (anti_symm (⊢)); auto.
  - apply and_elim_l. 
  - apply and_intro; auto. apply and_elim_r.
Qed.
  

(* BI connectives *)
Lemma sep_mono P P' Q Q' : (P ⊢ Q) → (P' ⊢ Q') → P ★ P' ⊢ Q ★ Q'.
Proof.
  intros HQ HQ'; unseal.
  split; intros n' x y ?? (x1&x2&y1&y2&?&?&?&?); exists x1,x2,y1,y2; ofe_subst x; ofe_subst y.
    split_and!; eauto 7 using cmra_validN_op_l, cmra_validN_op_r, uPred_in_entails.
Qed.
Global Instance Emp_sep : LeftId (⊣⊢) Emp%IP (@uPred_sep M).
Proof.
  intros P; unseal; split=> n x y Hvalid Hvalidy; split.
  - intros (x1&x2&y1&y2&?&?&?&?); ofe_subst. 
    rewrite left_id in Hvalidy *; eauto using uPred_mono, cmra_includedN_r.
  - intros ?. exists (core x), x, (∅ : M), y; rewrite cmra_core_l left_id.
    split_and!; eauto. simpl; reflexivity.
Qed. 
Global Instance sep_comm : Comm (⊣⊢) (@uPred_sep M).
Proof.
  intros P Q; unseal; split=> n x y ??; split;
    intros (x1&x2&y1&y2&?&?&?&?); exists x2, x1, y2, y1;
  by rewrite (comm op x2) (comm op y2).
Qed.
Global Instance sep_assoc : Assoc (⊣⊢) (@uPred_sep M).
Proof.
  intros P Q R; unseal; split=> n x y ??; split.
  - intros (x1&x2&y1&y2&Hx&Hy&?&z1&z2&w1&w2&Hz&Hw&?&?); exists (x1 ⋅ z1), z2, (y1 ⋅ w1), w2;
    split_and?; auto.
    + by rewrite -(assoc op) -Hz -Hx.
    + by rewrite -(assoc op) -Hw -Hy.
    + by exists x1, z1, y1, w1.
  - intros (x1&x2&y1&y2&Hx&Hy&(z1&z2&w1&w2&Hz&Hw&?&?)&?). 
    exists z1, (z2 ⋅ x2), w1, (w2 ⋅ y2); split_and?; auto.
    + by rewrite (assoc op) -Hz -Hx.
    + by rewrite (assoc op) -Hw -Hy.
    + by exists z2, x2, w2, y2.
Qed.
Lemma wand_intro_r P Q R : (P ★ Q ⊢ R) → P ⊢ Q -★ R.
Proof.
  unseal=> HPQR; split=> n x y ??? n' x' y' ????. apply HPQR; auto.
  exists x, x', y, y'; split_and?; auto.
  eapply uPred_closed with n; eauto using cmra_validN_op_l.
Qed.
Lemma wand_elim_l' P Q R : (P ⊢ Q -★ R) → P ★ Q ⊢ R.
  unseal =>HPQR. split; intros n x y ?? (?&?&?&?&?&?&?&?). ofe_subst.
  eapply HPQR; eauto using cmra_validN_op_l.
Qed.
Lemma affine_elim P : ⧆P ⊢ P. 
Proof. by rewrite affine_equiv and_elim_l. Qed.

Lemma sep_affine_3 P Q: ⧆(⧆P ★ Q) ⊢ (⧆P ★ ⧆Q).
Proof.
  unseal; constructor.
  intros n x y ?? ((x1&x2&y1&y2&Hx&Hy&(?&Hy1)&HQ)&Hempty).
  assert (y2 ≡{n}≡ ∅) as Hy2. 
  { rewrite -Hempty Hy Hy1 left_id //=. }
  exists x1, x2, y1, y2. split_and!; auto.
  * split; auto.
  * split; auto.
Qed.

Lemma persistent_intro' P Q : (uPred_persistent P ⊢ Q) → uPred_persistent P ⊢ uPred_persistent Q.
Proof.
  unseal=> HPQ.
  split=> n x y ?? ?.
  apply HPQ; simpl; auto using @cmra_core_validN, @ucmra_unit_validN. by rewrite ?cmra_core_idemp.
Qed.

Lemma relevant_elim P : □ P ⊢ P.
Proof.
  unseal; split=> n x y ?? /=. 
  intros (HP&Hy). rewrite Hy. 
  eapply uPred_mono; eauto using @cmra_included_core, cmra_included_includedN.
Qed.
Lemma relevant_and P Q : (□ (P ∧ Q)) ⊣⊢ (□ P ∧ □ Q).
Proof. unseal; split=>n x y ??; naive_solver. Qed.
Lemma relevant_or P Q : (□ (P ∨ Q)) ⊣⊢ (□ P ∨ □ Q).
Proof. unseal; split=>n x y ??; naive_solver. Qed.
Lemma relevant_forall `{Inhabited A} (Ψ : A → uPred M) : (□ ∀ a, Ψ a) ⊣⊢ (∀ a, □ Ψ a).
Proof. 
  unseal; split=>n x y ??. 
  split; simpl; try naive_solver.
  intros HP. split; first naive_solver. 
  edestruct (HP inhabitant); eauto.
Qed.
Lemma relevant_exist {A} (Ψ : A → uPred M) : (□ ∃ a, Ψ a) ⊣⊢ (∃ a, □ Ψ a).
Proof. 
  unseal; split=>n x y ??; try naive_solver.
Qed.
Lemma relevant_and_sep_1 P Q : □ (P ∧ Q) ⊢ □ (P ★ Q).
Proof.
  unseal; split=> n x y ?? [??].
  split; auto.
  exists (core x), (core x), (core y), (core y); rewrite -?cmra_core_dup; auto.
Qed.
Lemma relevant_and_sep_l_1' P Q : (□ P ∧ Q) ⊢ (□ P ★ Q).
Proof.
  unseal; split=> n x y ?? [[??]?]; exists (core x), x, (core y), y; simpl in *.
  by rewrite ?cmra_core_l ?cmra_core_idemp.
Qed.
Lemma relevant_relevant_later P : (□ ▷ P) ⊣⊢ (□ ▷ □ P).
Proof. unseal. split=>-[|n] x y ??; auto.
       simpl. rewrite ?cmra_core_idemp. naive_solver.
Qed.
Lemma relevant_later_1 P : (□ ▷ P) ⊢ (▷ □ P).
Proof. rewrite relevant_relevant_later. rewrite relevant_elim //=. Qed.
Lemma relevant_mono P Q : (P ⊢ Q) → □ P ⊢ □ Q.
Proof. unseal. intro HPQ. econstructor; intros n x y ?? [? ?]. 
       split; auto. eapply HPQ; eauto using @cmra_core_validN.
Qed.

Lemma pure_elim_spare φ Q Q' R : (Q ⊢ ⧆■ φ ★ Q') → (φ → (Q ⊢ R)) → Q ⊢ R.
Proof.
  unseal; intros HQP HQR. split=> n x ??? HQ; apply HQR; eauto.
  edestruct HQP as (HQP').
  edestruct HQP' as (?&?&?&?&?); try eapply HQ; eauto.
  naive_solver.
Qed.

Hint Resolve relevant_mono.

(* Affine *)


Lemma affine_relevant P : ⧆□ P ⊢ □⧆P.
Proof.
  unseal; auto; econstructor; intros n x y ?? ((?&Hcore)&Heqy); auto.
  split; auto. split; auto.
  by rewrite Heqy persistent_core.
Qed.
Lemma affine_and P Q : (⧆(P ∧ Q)) ⊣⊢ (⧆P ∧ ⧆Q).
Proof. unseal; split=>n x y ??; naive_solver. Qed.
Lemma affine_and_l P Q : (⧆P ∧ Q) ⊣⊢ (⧆P ∧ ⧆Q).
Proof. unseal; split=>n x y ??; naive_solver. Qed.
Lemma affine_and_r P Q : (P ∧ ⧆Q) ⊣⊢ (⧆P ∧ ⧆Q).
Proof. unseal; split=>n x y ??; naive_solver. Qed.
Lemma affine_or P Q : (⧆(P ∨ Q)) ⊣⊢ (⧆P ∨ ⧆Q).
Proof. unseal; split=>n x y ??; naive_solver. Qed.
Lemma affine_forall `{Inhabited A} (Ψ : A → uPred M) : (⧆ ∀ a, Ψ a) ⊣⊢ (∀ a, ⧆ Ψ a).
Proof. 
  unseal; split=>n x y ??. 
  split; simpl; try naive_solver.
  intros HP. split; first naive_solver. 
  edestruct (HP inhabitant); eauto.
Qed.
Lemma affine_exist {A} (Ψ : A → uPred M) : (⧆ ∃ a, Ψ a) ⊣⊢ (∃ a, ⧆ Ψ a).
Proof. 
  unseal; split=>n x y ??; try naive_solver.
Qed.
Lemma affine_affine_later P : (⧆ ▷ P) ⊣⊢ (⧆ ▷ ⧆P).
Proof. unseal. split=>-[|n] x y ??; auto.
       simpl. split; intros; naive_solver eauto 2 using dist_le.
Qed.
Lemma affine_later_distrib P Q: (⧆▷ (⧆P ★ ⧆Q) ⊢ (⧆▷ P ★ ⧆▷ Q)).
Proof. 
  unseal. split=>-[|n] x y ?? [HL ?]; auto; simpl.
  - exists x, (∅ : M), (∅ : M), (∅ : M).
    split_and!; rewrite ?right_id; auto.
  - destruct HL as (x1&x2&y1&y2&?&?&(?&Hay1)&(?&Hay2)).
    destruct (cmra_extend n x x1 x2)
      as ([x1' x2']&Hx'&Hx1'&Hx2'); eauto using cmra_validN_S; simpl in *.
    destruct (cmra_extend (S n) y ∅ ∅)
      as ([y1' y2']&Hy'&Hy1'&Hy2'); eauto using cmra_validN_S; simpl in *.
      { rewrite ?right_id; eauto using dist_le. }
    exists x1', x2', y1', y2'; split_and!; auto.
    * by rewrite Hx1' (dist_le _ _ _ _ Hy1') //= -?Hay1; last lia.
    * by rewrite Hx2' (dist_le _ _ _ _ Hy2') //= -?Hay2; last lia.
Qed.
Lemma relevant_affine P : □⧆P ⊣⊢ ⧆□ P.
Proof.
  apply (anti_symm (⊢)).
  - unseal; auto; econstructor; intros n x y ?? ((?&Hcore)&Heqy); auto.
    split.
    * split; auto.
    * by rewrite Heqy Hcore.
  - unseal; auto; econstructor; intros n x y ?? ((?&Hcore)&Heqy); auto.
    split; auto. split; auto. rewrite -Heqy; auto.
Qed.
Lemma affine_mono P Q: (P ⊢ Q) → ⧆P ⊢ ⧆Q.
Proof. intros HP. split. unseal => n x y ?? [? ?]; split; auto. eapply HP; eauto. Qed.

Lemma affine_relevant_later P: ⧆▷□P ⊣⊢ ⧆□▷ P.
Proof.
  apply (anti_symm (⊢)).
  - unseal; auto; econstructor; intros n x y ??. 
    intros (HP&Haff). split; auto.
    rewrite Haff in HP *. destruct n as [|n']; simpl; rewrite ?persistent_core; auto.
    intros (HP&_). split; auto.
  - apply affine_mono, relevant_later_1.
Qed.

Lemma unlimited_persistent P: ⧈P ⊢ (uPred_persistent P).
Proof.
  unseal. split => n x y ??.
  intros ((?&Heq1)&Heq2). rewrite /uPred_holds//=. rewrite -Heq2 Heq1. done. 
Qed.

Lemma unlimited_affine_persistent P: ⧈P ⊣⊢ uPred_affine (uPred_persistent P).
Proof.
  unseal; split; intros n x y ??; split.
  - intros ((?&Heq1)&Heq2). rewrite /uPred_holds//=. rewrite -Heq2 Heq1. done. 
  - intros (HP&Heq1). rewrite /uPred_holds//= Heq1.
    rewrite /core/core'//= ucmra_pcore_unit //=.
Qed.


(* Later *)
Lemma later_mono P Q : (P ⊢ Q) → ▷ P ⊢ ▷ Q.
Proof.
  unseal=> HP; split=>-[|n] x y ???; [done|apply HP; eauto using cmra_validN_S].
Qed.
Lemma later_intro P : P ⊢ ▷ P.
Proof.
  unseal; split=> -[|n] x y ???; simpl in *; [done|].
  apply uPred_closed with (S n); eauto using cmra_validN_S.
Qed.
Lemma löb P : (▷ P → P) ⊢ P.
Proof.
  unseal; split=> n x y ?? HP; induction n as [|n IH]; [by apply HP|].
  apply HP, IH, uPred_closed with (S n); eauto using cmra_validN_S.
Qed.
Lemma later_sep P Q : ▷ (P ★ Q) ⊣⊢ ▷ P ★ ▷ Q.
Proof.
  unseal; split=> n x z ??; split.
  - destruct n as [|n]; simpl.
    { by exists x, (core x), z, (core z); rewrite ?cmra_core_r. }
    intros (x1&x2&z1&z2&Hx&Hz&?&?); 
      destruct (cmra_extend n x x1 x2) as ([y1 y2]&Hx'&Hy1&Hy2); 
      eauto using cmra_validN_S; simpl in *.
      destruct (cmra_extend n z z1 z2) as ([q1 q2]&Hz'&Hq1&Hq2); 
      eauto using cmra_validN_S; simpl in *.
    exists y1, y2, q1, q2; split_and!; [by rewrite Hx'| by rewrite Hz' 
                                        | by rewrite Hy1 Hq1 | by rewrite Hy2 Hq2].
  - destruct n as [|n]; simpl; [done|intros (x1&x2&z1&z2&Hx&Hz&?&?)].
    exists x1, x2, z1, z2; split_and!; eauto using dist_S.
Qed.
Lemma later_sep_affine_l P Q : (▷ (⧆P ★ Q)) ⊣⊢ (⧆▷ P ★ ▷ Q).
Proof.
  unseal; split=> n x z ??; split.
  - destruct n as [|n]; simpl.
    { by intros; exists x, (core x), (∅ : M), z; rewrite ?cmra_core_r ?left_id. }
    intros (x1&x2&z1&z2&Hx&Hz&(?&Haff)&?); 
      destruct (cmra_extend n x x1 x2) as ([y1 y2]&Hx'&Hy1&Hy2); 
      eauto using cmra_validN_S; simpl in *.
      destruct (cmra_extend n z z1 z2) as ([q1 q2]&Hz'&Hq1&Hq2); 
      eauto using cmra_validN_S; simpl in *.
    exists y1, y2, (∅ : M), (q1 ⋅ q2); split_and!. 
    * by rewrite Hx'.
    * by rewrite left_id Hz'.
    * by rewrite -Haff Hy1.
    * done.
    * by rewrite Hq1 Hq2 Hy2 Haff ?left_id.
  - destruct n as [|n]; simpl; [done|intros (x1&x2&z1&z2&Hx&Hz&(?&?)&?)].
    exists x1, x2, z1, z2; split_and!; eauto using dist_S.
Qed.

(* Own *)
Lemma ownM_op (a1 a2 : M) :
  uPred_ownM (a1 ⋅ a2) ⊣⊢ uPred_ownM a1 ★ uPred_ownM a2.
Proof.
  unseal; split=> n x y ??; split.
  - intros [z ?]; exists a1, (a2 ⋅ z), y, (core y); 
    split_and!; [by rewrite (assoc op)| by rewrite cmra_core_r | |].
    * by exists (core a1); rewrite cmra_core_r. 
    * by exists z.
  - intros (y1&y2&q1&q2&Hx&Hy&[z1 Hy1]&[z2 Hy2]); exists (z1 ⋅ z2).
    by rewrite (assoc op _ z1) -(comm op z1) (assoc op z1)
      -(assoc op _ a2) (comm op z1) -Hy1 -Hy2.
Qed.
Lemma ownM_op' (a1 a2 : M) :
  uPred_ownM (a1 ⋅ a2) ⊣⊢ (uPred_ownM a1 ★ ⧆ uPred_ownM a2).
Proof.
  unseal; split=> n x y ??; split.
  - intros [z ?]; exists a1, (a2 ⋅ z), y, (∅ : M) ; 
    split_and!; [by rewrite (assoc op)| by rewrite right_id | |].
    * by exists (core a1); rewrite cmra_core_r. 
    * split; auto. by exists z.
  - intros (y1&y2&q1&q2&Hx&Hy&[z1 Hy1]&([z2 Hy2]&Hq2)); exists (z1 ⋅ z2).
    by rewrite (assoc op _ z1) -(comm op z1) (assoc op z1)
      -(assoc op _ a2) (comm op z1) -Hy1 -Hy2.
Qed.
Lemma ownM_op'' (a1 a2 : M) :
  ⧆ uPred_ownM (a1 ⋅ a2) ⊣⊢ (⧆ uPred_ownM a1 ★ ⧆ uPred_ownM a2).
Proof.
  unseal; split=> n x y ??; split.
  - intros [[z ?] Heqy]. exists a1, (a2 ⋅ z), y, (∅ : M) ; 
    split_and!; [by rewrite (assoc op)| by rewrite right_id | |].
    * rewrite Heqy; split; auto. exists (core a1); rewrite cmra_core_r //=.
    * split; auto. by exists z.
  - intros (y1&y2&q1&q2&Hx&Hy&([z1 Hy1]&Hq1)&([z2 Hy2]&Hq2)). 
    split.
    * exists (z1 ⋅ z2).
        by rewrite (assoc op _ z1) -(comm op z1) (assoc op z1)
           -(assoc op _ a2) (comm op z1) -Hy1 -Hy2.
    * rewrite Hy Hq1 Hq2 right_id //=.
Qed.
Lemma unlimited_ownM_core (a : M) : (⧈ uPred_ownM (core a)) ⊣⊢ ⧆ uPred_ownM (core a).
Proof.
  split=> n x y; split. 
  - unseal. intros (([a' Hx]&?)&?); simpl. split; auto.
    exists (a' ⋅ x). by rewrite assoc -Hx cmra_core_l.
  - unseal; intros ([a' Hx]&->); simpl. rewrite -(cmra_core_idemp a) Hx.
    split_and!; try (rewrite ?persistent_core; auto; done).
    apply cmra_core_monoN, cmra_includedN_l.
Qed.
Lemma unlimited_ownM (a : M) : Persistent a → (⧈ uPred_ownM a) ⊣⊢ ⧆ uPred_ownM a.
Proof. intros. by rewrite -(persistent_core a) unlimited_ownM_core. Qed.
Lemma ownM_something : True ⊢ ∃ a, uPred_ownM a.
Proof. unseal; split=> n x ??. by exists x; simpl. Qed.
Lemma ownM_something' : Emp ⊢ ⧆∃ a, uPred_ownM a.
Proof. unseal; split=> n x ??. by split; auto; exists x; simpl. Qed.
Lemma ownM_empty : True ⊢ uPred_ownM ∅.
Proof. unseal; split=> n x ??; by  exists x; rewrite left_id. Qed.
Lemma ownM_empty' : Emp ⊢ ⧆uPred_ownM ∅.
Proof. unseal; split=> n x ??; by split; auto; exists x; rewrite left_id. Qed.

(* Own Linear *)
Lemma ownMl_op (a1 a2 : M) :
  uPred_ownMl (a1 ⋅ a2) ⊣⊢ (uPred_ownMl a1 ★ uPred_ownMl a2).
Proof.
  unseal; split=> n x y ??; split.
  - intros ?. exists x, (core x), a1, a2; 
    split_and!; simpl; auto.
    by rewrite cmra_core_r.
  - intros (y1&y2&q1&q2&Hx&Hy&Hy1&Hy2).
    simpl in *. by rewrite Hy Hy1 Hy2.
Qed.
Lemma relevant_ownMl_core (a : M) : (□ uPred_ownMl (core a)) ⊣⊢ uPred_ownMl (core a).
Proof.
  split=> n x y; split. 
  - unseal. intros (Hx&?); simpl. simpl in Hx. rewrite Hx. auto.
  - unseal=>//=. intros Heq. rewrite -?Heq -(cmra_core_idemp a) Heq.
    by rewrite persistent_core.
Qed.
Lemma relevant_ownMl (a : M) : Persistent a → (□ uPred_ownMl a) ⊣⊢  uPred_ownMl a.
Proof. intros. by rewrite -(persistent_core a) relevant_ownMl_core. Qed.
Lemma ownMl_something : True ⊢ ∃ a, uPred_ownMl a.
Proof. unseal; split=> n x y ???. by exists y; simpl. Qed.
Lemma ownMl_empty : Emp ⊣⊢ uPred_ownMl ∅.
Proof. unseal; split=> n x y ??. simpl. split; auto. Qed.

(* Valid *)
Lemma ownM_valid (a : M) : uPred_ownM a ⊢ ✓ a.
Proof.
  unseal; split=> n x y Hv ? [a' ?]; ofe_subst; eauto using cmra_validN_op_l.
Qed.
Lemma ownMl_valid (a : M) : uPred_ownMl a ⊢ (uPred_ownMl a ★ ⧆✓ a).
Proof.
  unseal; split=> n x y Hv //= ? Heq.
  exists x, (core x), y, (∅ : M). rewrite ?cmra_core_r ?right_id. split_and!; auto; by ofe_subst.
Qed.
Lemma valid_intro {A : cmraT} (a : A) : ✓ a → True ⊢ ✓ a.
Proof. unseal=> ?; split=> n x y ? ? _ /=; by apply cmra_valid_validN. Qed.
Lemma valid_elim {A : cmraT} (a : A) : ¬ ✓{0} a → ✓ a ⊢ False.
Proof. unseal=> Ha; split=> n x y ???; apply Ha, cmra_validN_le with n; auto. Qed.
Lemma relevant_valid {A : cmraT} (a : A) : □ ⧆ (✓ a) ⊣⊢ ⧆ (✓ a).
Proof. 
  unseal. split=> n x y ??. simpl; split. 
  - intros ((?&?)&?). split; auto. etransitivity; eauto. 
  - intros (?&Hempty). split_and!; auto; rewrite Hempty persistent_core //=.
Qed.
Lemma valid_weaken {A : cmraT} (a b : A) : ✓ (a ⋅ b) ⊢ ✓ a.
Proof. unseal; split=> n x y _ ?; apply cmra_validN_op_l. Qed.

(* Own and valid derived *)
Lemma ownM_invalid (a : M) : ¬ ✓{0} a → uPred_ownM a ⊢ False.
Proof. by intros; rewrite ownM_valid valid_elim. Qed.

(* Products *)
Lemma prod_equivI {A B : ofeT} (x y : A * B) : x ≡ y ⊣⊢ x.1 ≡ y.1 ∧ x.2 ≡ y.2.
Proof. by unseal. Qed.
Lemma prod_validI {A B : cmraT} (x : A * B) : ✓ x ⊣⊢ ✓ x.1 ∧ ✓ x.2.
Proof. by unseal. Qed.

(* Later *)
Lemma later_equivI {A : ofeT} (x y : later A) :
  x ≡ y ⊣⊢ ▷ (later_car x ≡ later_car y).
Proof. by unseal. Qed.

Lemma discrete_valid {A : cmraT} `{!CMRADiscrete A} (a : A) : ✓ a ⊣⊢ (■ ✓ a).
Proof. unseal; split=> n x y _ _. by rewrite /= -cmra_discrete_valid_iff. Qed.

(* Option *)
Lemma option_equivI {A : ofeT} (mx my : option A) :
  mx ≡ my ⊣⊢ match mx, my with
             | Some x, Some y => x ≡ y | None, None => True | _, _ => False
             end.
Proof.
  uPred.unseal. do 2 split. by destruct 1. by destruct mx, my; try constructor.
Qed.
Lemma option_validI {A : cmraT} (mx : option A) :
  ✓ mx ⊣⊢ match mx with Some x => ✓ x | None => True end.
Proof. uPred.unseal. by destruct mx. Qed.

End uPred_logic.

(* Hint DB for the logic *)
Hint Resolve pure_intro.
Hint Resolve or_elim : I.
Hint Resolve and_intro : I.

End uPred.
