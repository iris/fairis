From iris.bi Require Export bi.
From iris.proofmode Require Import tactics.
From fri.algebra Require Export upred upred_bi.
Module Import uPred.
  Export upred.uPred.
  Export bi.
End uPred.

(* Hint DB for the logic *)
Hint Resolve pure_intro : I.
Hint Resolve or_elim or_intro_l' or_intro_r' : I.
Hint Resolve and_intro and_elim_l' and_elim_r' : I.
Hint Resolve persistently_mono : I.
Hint Resolve sep_mono : I. (* sep_elim_l' sep_elim_r'  *)
Hint Immediate True_intro False_elim : I.
Hint Immediate iff_refl internal_eq_refl : I.
(* Setup of the proof mode *)
Hint Extern 1 (environments.envs_entails _ (|==> _)) => iModIntro.
