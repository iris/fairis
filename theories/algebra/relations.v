From Coq Require Import Wf_nat.
From stdpp Require Export tactics base relations.

(** * Definitions *)
Section definitions.
  Context `(R : relation A).

  Inductive nstepsS `{Equiv A}: nat → relation A :=
    | nstepsS_O x y : x ≡ y → nstepsS 0 x y
    | nstepsS_l n x y z : R x y → nstepsS n y z → nstepsS (S n) x z.

End definitions.

Hint Unfold nf red.

(* Some general theorems about relations on setoids *)
Section setoid_rel.
  Context `{A_equiv: Equiv A} `{Equivalence _ A_equiv}.
  
  
  Lemma rel_proper_iff_impl: 
    forall (P: A -> A -> Prop), Proper (equiv ==> equiv ==> iff) P -> 
                                Proper (equiv ==> equiv ==> impl) P.
  Proof.
    intros P Hpr ? ? Heq ? ? Heq' Hp.
    eapply Hpr; [ | | apply Hp]; eauto.
  Qed.

  Global Instance rel_proper_impl_iff (R: relation A) (resp: Proper (equiv ==> equiv ==> impl) R):
    Proper (equiv ==> equiv ==> iff) R.
  Proof.
    intros x y Heq x' y' Heq'.
    split.
    * intros HR. rewrite <-Heq, <-Heq'; auto.
    * intros HR. rewrite <-Heq, <-Heq' in HR; auto.
  Qed.

  Context `(R : relation A) (resp: Proper (equiv ==> equiv ==> iff) R). 

  Existing Instance resp.
  
  Section union.
    Context `(R' : relation A) (resp': Proper (equiv ==> equiv ==> iff) R'). 
    Existing Instance resp'.
    Global Instance rel_union_proper: Proper ((≡) ==> (≡) ==> iff) (Relation_Operators.union _ R R').
    Proof.
      intros x y Heq x' y' Heq'.
      split.
      * intros [HR | HR']; [ left | right ]; rewrite <- Heq, <- Heq'; auto.
      * intros [HR | HR']; [ left | right ]; rewrite Heq, Heq'; auto.
    Qed.

  End union.

  Global Instance tc_proper: Proper ((≡) ==> (≡) ==> iff) (tc R).
  Proof. 
    intros x y Heq x' y' Heq'. 
    split. 
    * intros HR. revert HR y Heq y' Heq'.
      induction 1; intros.
      ** eapply tc_once; rewrite <- Heq, <- Heq'; eauto. 
      ** eapply tc_l.
         rewrite <- Heq; eauto.
         eauto.
    * intros HR. revert HR x Heq x' Heq'.
      induction 1; intros.
      ** eapply tc_once; rewrite Heq, Heq'; eauto. 
      ** eapply tc_l.
         rewrite Heq; eauto.
         eauto.
  Qed.

  Global Instance nstepsS_proper n: Proper ((≡) ==> (≡) ==> iff) (nstepsS R n).
  Proof.
    intros x y Heq x' y' Heq'. 
    split. 
    * intros HR. revert HR y Heq y' Heq'.
      induction 1; intros.
      ** econstructor; rewrite <- Heq, <- Heq'; eauto. 
      ** eapply nstepsS_l.
         rewrite <- Heq; eauto.
         eauto.
    * intros HR. revert HR x Heq x' Heq'.
      induction 1; intros.
      ** econstructor; rewrite Heq, Heq'; eauto. 
      ** eapply nstepsS_l.
         rewrite Heq; eauto.
         eauto.
  Qed.
    
  Lemma nsteps_nstepsS: ∀ n x y, nsteps R n x y → nstepsS R n x y.
  Proof.
    induction 1. 
    * econstructor; eauto.
    * eapply nstepsS_l; eauto.
  Qed.

  Lemma nstepsS_nsteps: ∀ n x y, nstepsS R (S n) x y → nsteps R (S n) x y.
  Proof.
    intros ? ? ? Hns. assert (S n ≠ 0) as Hnz by auto.
    revert Hns Hnz.
    induction 1 as [| n'].
    * intros; exfalso; auto.
    * intros. destruct n'.
      ** inversion Hns as [ ? ? Heq|]. subst. econstructor; [|econstructor].
         rewrite <-Heq. auto.
      ** econstructor; eauto.
  Qed.

  Global Instance rtcS_proper: Proper ((≡) ==> (≡) ==> iff) (rtcS R).
  Proof. 
    intros x y Heq x' y' Heq'. 
    split. 
    * intros HR. revert HR y Heq y' Heq'.
      induction 1; intros.
      ** eapply rtcS_refl; rewrite <- Heq, <- Heq'; eauto. 
      ** eapply rtcS_l.
         rewrite <- Heq; eauto.
         eauto.
    * intros HR. revert HR x Heq x' Heq'.
      induction 1; intros.
      ** eapply rtcS_refl; rewrite Heq, Heq'; eauto. 
      ** eapply rtcS_l.
         rewrite Heq; eauto.
         eauto.
  Qed.

  Global Instance rtcS_reflexive: Reflexive (rtcS R).
  Proof. intros r. eapply rtcS_refl. reflexivity. Qed.
  Lemma rtcS_transitive x y z : rtcS R x y → rtcS R y z → rtcS R x z.
  Proof. induction 1; eauto; try rewrite <-H0; auto. 
         intros. eapply rtcS_l; eauto.
  Qed.
  Global Instance: Transitive (rtcS R).
  Proof. exact rtcS_transitive. Qed.

  Lemma rtc_rtcS x y: rtc R x y -> rtcS R x y.
  Proof.
    induction 1; subst.
    * eapply rtcS_refl; eauto. 
    * eapply rtcS_l; eauto.
  Qed.

  Hint Resolve rtc_rtcS : arsS. 

  Lemma rtcS_once x y : R x y → rtcS R x y.
  Proof. eauto using rtc_once with arsS. Qed.
  Lemma rtcS_r x y z : rtcS R x y → R y z → rtcS R x z.
  Proof.  intros. etransitivity; eauto. eapply rtcS_once; auto. Qed.
  Lemma rtcS_inv x z : rtcS R x z → x ≡ z ∨ ∃ y, R x y ∧ rtcS R y z.
  Proof. inversion_clear 1; eauto. Qed.
  Lemma rtcS_ind_l (P : A → Prop) (Hp: Proper ((≡)  ==> iff) P) (z : A)
    (Prefl : P z) (Pstep : ∀ x y, R x y → rtcS R y z → P y → P x) :
    ∀ x, rtcS R x z → P x.
  Proof. induction 1; eauto with arsS. eapply Hp; eauto. Qed.
  Lemma rtcS_ind_r_weak (P : A → A → Prop) (Hp: Proper ((≡) ==> (≡) ==> iff) P)
    (Prefl : ∀ x, P x x) (Pstep : ∀ x y z, rtcS R x y → R y z → P x y → P x z) :
    ∀ x z, rtcS R x z → P x z.
  Proof.
    cut (∀ y z, rtcS R y z → ∀ x, rtcS R x y → P x y → P x z).
    { eauto using rtcS_refl. }
    induction 1; eauto using rtcS_r with arsS.
    intros x0 Hr HP; eapply Hp; try eapply HP; eauto.
  Qed.
  Lemma rtcS_ind_r (P : A → Prop) (x : A) (Hp: Proper ((≡) ==> iff) P)
    (Prefl : P x) (Pstep : ∀ y z, rtcS R x y → R y z → P y → P z) :
    ∀ z, rtcS R x z → P z.
  Proof.
    intros z p. revert x z p Prefl Pstep. refine (rtcS_ind_r_weak _ _ _ _); eauto.
    intros x y Heq x' y' Heq'.
    split.
    * intros Himpl1 Hy Himpl2. eapply Hp. symmetry; eauto. 
      eapply Himpl1; [ eapply Hp |]; eauto.
      intros. eapply Himpl2; [ rewrite <- Heq; eauto | | ]; eauto.
    * intros Himpl1 Hy Himpl2. eapply Hp. eauto. 
      eapply Himpl1. eapply Hp. symmetry. eauto. eauto.
      intros. eapply Himpl2; [ rewrite Heq; eauto | | ]; eauto.
  Qed.
  Lemma rtcS_inv_r x z : rtcS R x z → x ≡ z ∨ ∃ y, rtcS R x y ∧ R y z.
  Proof. 
    revert z. apply rtcS_ind_r; eauto with arsS.
    intros y z Heq.
    split. 
    * rewrite Heq.
      intros [ | [y' (Hrtc&HR) ] ]; eauto. right.
      eexists; split_and?; eauto.
      eapply resp; [reflexivity | symmetry; eapply Heq |]; eauto.
    * rewrite Heq.
      intros [ | [y' (Hrtc&HR) ] ]; eauto. right.
      eexists; split_and?; eauto.
      eapply resp; [reflexivity | eapply Heq |]; eauto.
 Qed.

End setoid_rel.
