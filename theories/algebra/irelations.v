(* Copyright (c) 2012-2015, Robbert Krebbers; Joseph Tassarotti *)
(* This file is distributed under the terms of the BSD license. *)

(** This file contains some definitions similar to relations.v, except for relations indexed
    by a natural number representing which "thread" took a step.

    For such relations, we can define a notion of "fairness" for infinite
    reduction sequences.

    We then prove a number of results about when, given a fair trace for one relation,
    there is a corresponding fair trace for another relation.
 **)

From Coq Require Import Wf_nat Omega.
From stdpp Require Export tactics base relations list sets.
From stdpp Require propset.
Require ClassicalEpsilon.

(** * Definitions *)
Section definitions.
  Context `(R : nat → relation A).

  (** An element is reducible if a step is possible. *)
  Definition red (x : A) := ∃ y i, R i x y.

  (** Index i is enabled in x if an i step is possible. *)
  Definition enabled (x : A) i := ∃ y, R i x y.

  (** An element is in normal form if no further steps are possible. *)
  Definition nf (x : A) := ¬red x.

  (** Sequence of reductions with corresponding list of indices. *)
  Inductive isteps : list nat → relation A :=
    | isteps_nil x : isteps nil x x
    | isteps_l i l x y z : R i x y → isteps l y z → isteps (i :: l) x z.

  (* Sequence of reductions where every state satisfies some predicate φ *)
  Inductive isteps_good φ : list nat → relation A :=
    | isteps_good_nil x : φ x → isteps_good φ nil x x
    | isteps_good_l i l x y z : R i x y → φ y → isteps_good φ l y z → isteps_good φ (i :: l) x z.
  
  Inductive istepsS `{Equiv A}: list nat → relation A :=
    | istepsS_O x y : x ≡ y → istepsS nil x y
    | istepsS_l i l x y z : R i x y → istepsS l y z → istepsS (i :: l) x z.

  Inductive isteps_aux: list (nat * A) → relation A :=
    | isteps_aux_nil x : isteps_aux nil x x
    | isteps_aux_l i l x y z : R i x y → isteps_aux l y z → isteps_aux ((i, y) :: l) x z.

  Inductive isteps_aux': list (A * nat) → relation A :=
    | isteps_aux'_nil x : isteps_aux' nil x x
    | isteps_aux'_l i l x y z : R i x y → isteps_aux' l y z → isteps_aux' ((x, i) :: l) x z.

  Lemma isteps_augment: ∀ l a a', isteps l a a' →
                                    ∃ l',  map snd l' = l ∧
                                           isteps_aux' l' a a'.
  Proof.
    induction 1.
    - exists []; split; auto; econstructor; auto.
    - edestruct IHisteps as (l'&?&?).
      exists ((x, i) :: l'); split; simpl; f_equal; auto.
      econstructor; eauto.
  Qed.

  Lemma isteps_aux'_erase: ∀ l a a', isteps_aux' l a a' →
                                           isteps (map snd l) a a'.
  Proof.
    induction 1; simpl; econstructor; eauto.
  Qed.

  (** Infinite executions **)

  (** An element [x] is universally looping if all paths starting at [x]
  are infinite. *)
  CoInductive all_loop : A → Prop :=
    | all_loop_do_step x : red x → (∀ y i, R i x y → all_loop y) → all_loop x.

  (** An element [x] is existentally looping if some path starting at [x]
  is infinite. *)
  CoInductive ex_loop : A → Prop :=
    | ex_loop_do_step i x y : R i x y → ex_loop y → ex_loop x.

  CoInductive trace : A → Type :=
    | trace_step i x y : R i x y → trace y → trace x.

  CoInductive eq_ext: ∀ {x}, trace x → trace x → Prop :=
    | eq_ext_hd: ∀ i x x' HR HR' (e e': trace x'), 
        eq_ext e e' →
        eq_ext (trace_step i x x' HR e) (trace_step i x x' HR' e').
 
  Inductive suffix: ∀ {x y}, trace x → trace y → Prop :=
    | suffix_eq: ∀ x (e e': trace x), eq_ext e e' → suffix e e'
    | suffix_tl i x x' HR (e: trace x') y (e': trace y):
        suffix e e' →
        suffix (trace_step i x x' HR e) e'.

  Definition trace_aux {x} (e: trace x) :=
    match e with
      | trace_step i x y H e => trace_step i x y H e
    end.

  Definition hd {x} (e: trace x) :=
    match e with
      | trace_step i x y _ _ => x
    end.
  
  Lemma trace_aux_id: 
    forall {x} (e: trace x), e = trace_aux e.
  Proof.
    destruct e. auto.
  Qed.

  CoInductive always (P: ∀ i x y, R i x y → trace y → Prop) : ∀ x,  trace x → Prop :=
    | al_hd i x y e (HR: R i x y):
        P _ _ _ HR e → always P _ e -> always P _ (trace_step i x y HR e).
    
  Inductive eventually (P: ∀ i x y, R i x y → trace y → Prop) : ∀ x,  trace x → Prop :=
    | ev_now i x y e (HR: R i x y):  P _ _ _ HR e → eventually P _ (trace_step i x y HR e)
    | ev_later i x y e (HR: R i x y): 
        eventually P y e -> eventually P x (trace_step i x y HR e).

  (** Index i is always enabled in a trace if it's enabled at the head
  and always enabled in the tail **)

  CoInductive al_enabled: forall (x : A), trace x → nat → Prop :=
    | al_enabled_hd i i' x y e (HR: R i' x y):
        enabled x i -> al_enabled _ e i -> al_enabled _ (trace_step i' x y HR e) i.
  
  Definition al_enabled' x e i := always (λ _ x y _ _, enabled x i) x e.

  Definition al_disabled x e i := always (λ _ x y _ _, ¬ enabled x i) x e.
  
  Lemma al_enabled_equiv: ∀ x e i, al_enabled x e i ↔ al_enabled' x e i.
  Proof.
    unfold al_enabled'.
    split.
    - revert x e i. cofix COFIX.
      intros x e i Hae. 
      destruct Hae. econstructor; eauto.
    - revert x e i. cofix COFIX.
      intros x e i Hae. 
      destruct Hae. econstructor; eauto.
  Qed.
  
  (** Index i is eventually always enabled in a trace if it's either always enabled 
  or it's eventually always enabled in the tail **)

  Inductive ea_enabled: forall (x : A), trace x → nat → Prop :=
    | ea_enabled_now i x e: al_enabled x e i -> ea_enabled x e i
    | ea_enabled_later i i' x y e (HR: R i' x y): 
        ea_enabled y e i -> ea_enabled x (trace_step i' x y HR e) i.
  
  Definition ea_enabled' x (e: trace x) i := 
    eventually (λ _ _ _ HR e', al_enabled' _ (trace_step _ _ _ HR e') i) _ e.
  
  Lemma ea_enabled_equiv: ∀ x e i, 
      ea_enabled x e i ↔ ea_enabled' x e i.
  Proof.
    unfold ea_enabled'.
    split.
    - revert x e i. induction 1.
      * destruct e.
        eapply ev_now.
        eapply al_enabled_equiv; eauto.
      * destruct e.
        eapply ev_later.
        eauto.
    - revert x e i. induction 1.
      * destruct e.
        eapply ea_enabled_now.
        eapply al_enabled_equiv; eauto.
      * destruct e.
        eapply ea_enabled_later.
        eauto.
  Qed.

  Definition ea_disabled x (e: trace x) i := 
    eventually (λ _ _ _ HR e', al_disabled _ (trace_step _ _ _ HR e') i) _ e.
  
  (** Index i takes a step eventually if it takes a step at the head or
   or it eventually takes a step in the tail **)

   Inductive ev_taken: forall (x : A), trace x → nat → Prop :=
    | ev_taken_now i x y e (HR: R i x y): ev_taken x (trace_step i x y HR e) i
    | ev_taken_later i i' x y e (HR: R i' x y): 
        ev_taken y e i -> ev_taken x (trace_step i' x y HR e) i.

   Definition ev_taken' x (e: trace x) i := 
     eventually (λ i' _ _ HR e', i = i') _ e.
   
   Lemma ev_taken_equiv: ∀ x e i, 
       ev_taken x e i ↔ ev_taken' x e i.
   Proof.
     unfold ev_taken'.
     split.
     - revert x e i. induction 1.
       * destruct e.
         eapply ev_now; auto.
       * destruct e.
         eapply ev_later; eauto.
     - revert x e i. induction 1.
       * subst. destruct e.
         eapply ev_taken_now.
       * destruct e.
         eapply ev_taken_later.
         eauto.
   Qed.

   (* ev_taken_k expresses the idea that index i eventually takes at least k steps *)
   Inductive ev_taken_k: forall (x : A), trace x → nat → nat → Prop :=
    | ev_taken_now_O i x y e (HR: R i x y): ev_taken_k x (trace_step i x y HR e) i 0
    | ev_taken_now_S i x y e (HR: R i x y) k:
        ev_taken_k y e i k  →
        ev_taken_k x (trace_step i x y HR e) i (S k)
    | ev_taken_later_k i i' x y e (HR: R i' x y) k: 
        ev_taken_k y e i k -> ev_taken_k x (trace_step i' x y HR e) i k.

  CoInductive ae_taken: forall (x : A), trace x → nat → Prop :=
    | ae_taken_hd i i' x y e (HR: R i' x y): 
        ev_taken x (trace_step i' x y HR e) i → 
        ae_taken y e i →
        ae_taken x (trace_step i' x y HR e) i.
  

  Definition ae_taken' x e i :=
    always (λ _ x y HR e, ev_taken' x (trace_step _ _ _ HR e) i) x e.
  
  Lemma ae_taken_equiv: ∀ x e i,
      ae_taken x e i ↔ ae_taken' x e i.
  Proof.
    unfold ae_taken'.
    split.
    - revert x e i. cofix COFIX.
      intros x e i Hae. 
      destruct Hae. econstructor; eauto.
      apply ev_taken_equiv; auto.
    - revert x e i. cofix COFIX.
      intros x e i Hae. 
      destruct Hae. econstructor; eauto.
      apply ev_taken_equiv; auto.
  Qed.
    
  CoInductive ae_taken_k: forall (x : A), trace x → nat → nat → Prop :=
    | ae_taken_k_hd i i' x y e (HR: R i' x y) k: 
        ev_taken_k x (trace_step i' x y HR e) i k → 
        ae_taken_k y e i k →
        ae_taken_k x (trace_step i' x y HR e) i k.

  Fixpoint tr2fun {x: A} (e: trace x) (i: nat) :  A * nat := 
    match i, e with
    | O, trace_step i _ _ _ _ => (x, i) 
    | S i', trace_step _ _ _ _ e' => tr2fun e' i'
    end.

  Definition tfun (f: nat → A * nat) :=
     ∀ k, R (snd (f k)) (fst (f k)) (fst (f (S k))).
  
  CoFixpoint fun2tr k (f: nat → A * nat) (HTr: tfun f) : trace (fst (f k)) :=
    trace_step (snd (f k)) (fst (f k)) (fst (f (S k))) (HTr k) (fun2tr (S k) f HTr).

  Lemma tr2fun_fun2tr:
    ∀ k f HTr idx, tr2fun (fun2tr k f HTr) idx = f (k + idx).
  Proof.
    intros.
    revert k f HTr.
    induction idx; simpl; intros.
    - rewrite plus_0_r. destruct (f k); auto.
    - rewrite IHidx. 
      replace (S k + idx) with (k + S idx) by lia; auto.
  Qed.
  
  Lemma tr2fun_hd x (e: trace x):
    fst (tr2fun e O) = x.
  Proof.
    destruct e; auto.
  Qed.

  Lemma tr2fun_succ x (e: trace x) k:
     R (snd (tr2fun e k)) (fst (tr2fun e k)) (fst (tr2fun e (S k))).
  Proof.
    revert x e.
    induction k; intros.
    - destruct e; simpl; destruct e; eauto.
    - destruct e; simpl; eauto.
  Qed.

  Lemma tr2fun_tfun x (e: trace x): tfun (tr2fun e).
  Proof.
    unfold tfun; intros k.
    revert x e.
    induction k; intros.
    - destruct e; simpl; destruct e; eauto.
    - destruct e; simpl; eauto.
  Qed.

  (*
  Program Lemma fun2tr_tr2fun x e {!(tr2fun e 0).1 = x}: eq_ext e (@fun2tr x O (tr2fun e) (tr2fun_tfun x e)).
                                                     
   *)
  
  Lemma tr2fun_ev x (e: trace x) P:
    eventually (λ i x y HR _, P (x, i)) x e  ↔ ∃ k, P (tr2fun e k). 
  Proof.
    split.
    - induction 1. 
      * exists 0; simpl; eauto.
      * edestruct IHeventually as (k&?). exists (S k); eauto.
    - intros Hf. edestruct Hf as (k & Hk).
      clear -Hk. revert x e Hk. induction k; intros.
      * destruct e. simpl in *; subst; econstructor; eauto.
      * eauto. destruct e. eapply ev_later. eapply IHk. eauto.
  Qed.

  Lemma tr2fun_al x (e: trace x) P:
    always (λ i x _ _ _, P (x, i)) x e 
           ↔ ∀ k, P (tr2fun e k). 
  Proof.
    split.
    - intros Hae k. revert x e Hae.
      induction k. 
      * intros. destruct Hae. 
        simpl; auto.
      * intros. destruct Hae.
        simpl. eapply IHk. eauto.
    - revert x e. cofix COFIX.
      intros x e Hf. destruct e. 
      econstructor.
      * eapply (Hf 0).
      * eapply COFIX; eauto.
        intros k. specialize (Hf (S k)); auto.
  Qed.

  Lemma tr2fun_al_ev x (e: trace x) P:
    always (λ i x y HR e, eventually (λ i x _ _ _, P (x, i)) x (trace_step _ _ _ HR e)) x e
           ↔ ∀ k, ∃ k', k' ≥ k ∧ P (tr2fun e k'). 
  Proof.
    split.
    - intros Hae k. revert x e Hae.
      induction k. 
      * intros. destruct Hae. 
        edestruct tr2fun_ev as ((k&Hk)&_); eauto.
        exists k; split; eauto.
        destruct k; auto.
        lia.
      * intros. destruct Hae.
        edestruct IHk as (k'&?&?); eauto.
        exists (S k'); split; auto.
        lia.
    - revert x e. cofix COFIX.
      intros x e Hf. destruct e. econstructor.
      * edestruct (Hf O) as (k'&?&?).
        eapply tr2fun_ev. exists k'. eauto.
      * eapply COFIX.
        intros k.
        edestruct (Hf (S k)) as (k'&?&?).
        destruct k' as [| k']; [lia|].
        exists k'. split; auto; lia.
  Qed.
  
  Lemma tr2fun_ev_al x (e: trace x) P:
    eventually (λ i x y HR e, always (λ i x _ _ _, P (x, i)) x (trace_step _ _ _ HR e)) x e
           ↔ ∃ k, ∀ k', k' ≥ k → P (tr2fun e k'). 
  Proof.
    split.
    - intros Hea.
      induction Hea as [? ? ? ? ? Hal|].
      * exists O. rewrite tr2fun_al in Hal.
        eauto.
      * edestruct IHHea as (k & Hk).
        exists (S k). intros k' ?. destruct k'; [lia|].
        simpl. eapply Hk. lia.
    - intros (k & Hk).
      revert x e Hk. induction k.
      * intros x e Hk. destruct e. 
        econstructor. rewrite tr2fun_al.
        intros k'; eapply Hk; eauto.
        lia.
      * intros x e Hk. destruct e.
        eapply (ev_later). eapply IHk.
        intros k' ?.
        specialize (Hk (S k')); simpl in Hk; eapply Hk.
        lia.
  Qed.

  Lemma tr2fun_ev_2 x (e: trace x) P:
    eventually (λ i x y HR _, P (x, i) y) x e  ↔ ∃ k, P (tr2fun e k) (fst (tr2fun e (S k))). 
  Proof.
    split.
    - induction 1. 
      * exists 0; simpl; eauto. destruct e. auto.
      * edestruct IHeventually as (k&?). exists (S k); eauto.
    - intros Hf. edestruct Hf as (k & Hk).
      clear -Hk. revert x e Hk. induction k; intros.
      * destruct e. simpl in *; subst; econstructor; eauto. destruct e; auto.
      * eauto. destruct e. eapply ev_later. eapply IHk. eauto.
  Qed.

  Lemma tr2fun_al_2 x (e: trace x) P:
    always (λ i x y _ _, P (x, i) y) x e 
           ↔ ∀ k, P (tr2fun e k) (fst (tr2fun e (S k))). 
  Proof.
    split.
    - intros Hae k. revert x e Hae.
      induction k. 
      * intros. destruct Hae. 
        simpl; destruct e;auto.
      * intros. destruct Hae.
        simpl. eapply IHk. eauto.
    - revert x e. cofix COFIX.
      intros x e Hf. destruct e. 
      econstructor.
      * simpl in Hf. specialize (Hf 0). simpl in Hf. destruct e; auto.
      * eapply COFIX; eauto.
        intros k. specialize (Hf (S k)); auto.
  Qed.

  Lemma tr2fun_al_ev_2 x (e: trace x) P:
    always (λ i x y HR e, eventually (λ i x y _ _, P (x, i) y) x (trace_step _ _ _ HR e)) x e
           ↔ ∀ k, ∃ k', k' ≥ k ∧ P (tr2fun e k') (fst (tr2fun e (S k'))). 
  Proof.
    split.
    - intros Hae k. revert x e Hae.
      induction k. 
      * intros. destruct Hae. 
        edestruct tr2fun_ev_2 as ((k&Hk)&_); eauto.
        exists k; split; eauto.
        destruct k; auto.
        lia.
      * intros. destruct Hae.
        edestruct IHk as (k'&?&?); eauto.
        exists (S k'); split; auto.
        lia.
    - revert x e. cofix COFIX.
      intros x e Hf. destruct e. econstructor.
      * edestruct (Hf O) as (k'&?&?).
        eapply tr2fun_ev_2. exists k'. eauto.
      * eapply COFIX.
        intros k.
        edestruct (Hf (S k)) as (k'&?&?).
        destruct k' as [| k']; [lia|].
        exists k'. split; auto; lia.
  Qed.
  
  Lemma tr2fun_ev_al_2 x (e: trace x) P:
    eventually (λ i x y HR e, always (λ i x y _ _, P (x, i) y) x (trace_step _ _ _ HR e)) x e
           ↔ ∃ k, ∀ k', k' ≥ k → P (tr2fun e k') (fst (tr2fun e (S k'))). 
  Proof.
    split.
    - intros Hea.
      induction Hea as [? ? ? ? ? Hal|].
      * exists O. rewrite tr2fun_al_2 in Hal.
        eauto.
      * edestruct IHHea as (k & Hk).
        exists (S k). intros k' ?. destruct k'; [lia|].
        simpl. eapply Hk. lia.
    - intros (k & Hk).
      revert x e Hk. induction k.
      * intros x e Hk. destruct e. 
        econstructor. rewrite tr2fun_al_2.
        intros k'; eapply Hk; eauto.
        lia.
      * intros x e Hk. destruct e.
        eapply (ev_later). eapply IHk.
        intros k' ?.
        specialize (Hk (S k')); simpl in Hk; eapply Hk.
        lia.
  Qed.

  Lemma tr2fun_ev_taken x (e: trace x) i:
    ev_taken x e i ↔ ∃ k, (snd (tr2fun e k)) = i.
  Proof.
    rewrite ev_taken_equiv.
    unfold ev_taken'. rewrite (tr2fun_ev _ _ (λ p, i = snd p)).
    split; intros (?&?); eauto.
  Qed.

  Lemma tr2fun_ae_taken x (e: trace x) i:
    ae_taken x e i ↔ ∀ k, ∃ k', k' ≥ k ∧ (snd (tr2fun e k')) = i.
  Proof.
    rewrite ae_taken_equiv.
    unfold ae_taken', ev_taken'.
    rewrite (tr2fun_al_ev _ _ (λ p, i = snd p)).
    split; intros Hp k; edestruct Hp as (?&?&?); eauto.
  Qed.
  
  Fixpoint inc_list (l: list nat) := 
    match l with
    | [] => True
    | n :: l' =>
      match l' with
      | [] => True
      | n' :: _ => n < n' ∧ inc_list l'
      end
    end.

  Lemma inc_list_tl:
    ∀ l a,  inc_list (a :: l) → inc_list l.
  Proof.
    induction l; auto; intros ? (?&?).
    destruct l; auto.
  Qed.
  
  Lemma inc_list_max_snoc:
    ∀ l k b, inc_list l → k > fold_left max l b → inc_list (l ++ [k]).
  Proof.
    induction l; auto.
    intros k b Hinc. simpl in *.
    destruct l; simpl in *. 
    - split; auto.
      eapply le_lt_trans. 
      eapply Nat.le_max_r.
      eauto.
    - destruct Hinc as (?&?). 
      split; auto. 
      eapply IHl; eauto.
  Qed.
  
  Lemma inc_list_map_S:
    ∀ l,  inc_list l → inc_list (map S l).
  Proof.
    induction l; auto.
    simpl. destruct l; auto.
    intros (?&?).
    simpl. split; [lia|].
    eapply IHl; eauto.
  Qed.

  Lemma inc_list_map_pred:
    ∀ l a,  inc_list (a :: l) → inc_list (map pred l).
  Proof.
    induction l; auto.
    intros a' (?&Hl).
    simpl. destruct l; auto.
    simpl. split. 
    * destruct a; [lia|].
      destruct Hl as (?&?).
      destruct n; [lia|].
      simpl. lia.
    * eapply IHl; eauto.
  Qed.

  Lemma inc_list_map_pred':
    ∀ l a,  inc_list (S a :: l) → inc_list (a :: map pred l).
  Proof.
    induction l; auto.
    intros a' (?&Hl).
    simpl. split. 
    * lia.
    * eapply IHl. destruct a; auto. lia.
  Qed.

  Lemma ev_taken_O (x: A) (e: trace x) i:
    ev_taken x e i ↔ ev_taken_k x e i O.
  Proof.
    split; by induction 1; econstructor.
  Qed.

  Lemma tr2fun_ev_taken_k x (e: trace x) i k:
    ev_taken_k x e i k ↔ ∃ l, inc_list l ∧ length l = (S k) ∧  Forall (λ n, snd (tr2fun e n) = i) l.
  Proof.
    split.
    - induction 1. 
      * exists [0]; simpl; eauto.
      * edestruct IHev_taken_k as (l&?&?&?). 
        exists (0 :: map S l). intros.
        split_and!.
        ** simpl. specialize (inc_list_map_S l); intros HS.
           destruct l; simpl; auto.
           split; [lia|].
           simpl in *. eapply HS. eauto.
        ** simpl. rewrite map_length. auto.
        ** econstructor; eauto. 
           eapply Forall_fmap.
           unfold compose; auto.
      * edestruct IHev_taken_k as (l&?&?&?). 
        exists (map S l). intros.
        split_and!.
        ** simpl. eapply inc_list_map_S; auto.
        ** simpl. rewrite map_length. auto.
        ** eapply Forall_fmap.
           unfold compose; auto.
    - intros (l&Hl). revert x e i l Hl.
      induction k as [|k]. 
      * destruct l; [ simpl in *; intros; lia|].
        intros (?&?&Hl).
        apply Forall_cons in Hl as (Hhd&Htl). 
        apply ev_taken_O. apply tr2fun_ev_taken.
        eauto.
      * intros x e i l (Hinc&Hlen&Hl).
        destruct l; [ simpl in *; intros; lia|].
        revert x e i l Hinc Hlen Hl.
        induction n; intros x e i l Hinc Hlen (Hhd&Htl)%Forall_cons.
        ** destruct e. simpl in Hhd; subst. econstructor.
           eapply (IHk _ _ _ (map pred l)).
           split_and!.
           *** eapply inc_list_map_pred; eauto.
           *** rewrite map_length. auto.
           *** rewrite Forall_fmap.
               unfold compose; auto.
               clear -Hinc Htl. induction l.
               **** econstructor.
               **** destruct a; [simpl in *; lia|]. 
                    simpl in *. 
                    eapply Forall_cons in Htl as (?&?).
                    eapply Forall_cons; split; eauto.
                    destruct Hinc as (?&Hinc').
                    eapply IHl; eauto.
                    destruct l; eauto.
                    destruct Hinc' as (?&?).
                    split; auto. lia.
        ** destruct e. 
           econstructor. simpl in Hhd.
           eapply (IHn _ _ _ (map pred l)).
           *** eapply inc_list_map_pred' in Hinc; auto.
           *** simpl. rewrite map_length. simpl in *. auto.
           *** eapply Forall_cons; split; auto. rewrite Forall_fmap.
               clear -Hinc Htl. induction l.
               **** econstructor.
               **** destruct a; [simpl in *; lia|]. 
                    simpl in *. 
                    eapply Forall_cons in Htl as (?&?).
                    eapply Forall_cons; split; eauto.
                    destruct Hinc as (?&Hinc').
                    eapply IHl; eauto.
                    destruct l; eauto.
                    destruct Hinc' as (?&?).
                    split; auto. lia.
  Qed.

  Lemma ae_taken_tr2fun_k x (e: trace x) i k:
    ae_taken x e i → ∃ l, inc_list l ∧ length l = k ∧ Forall (λ n, snd (tr2fun e n) = i) l.
  Proof.
    intros Hae.
    induction k.
    - exists nil. split_and!; simpl; eauto. 
    - edestruct IHk as (l&?&?&?).
      edestruct (tr2fun_ae_taken) as (Hbase&_).
      edestruct (Hbase Hae (S (fold_left max l 0))) as (k'&?&?).
      exists (l ++ [k']). split_and!.
      * eapply inc_list_max_snoc; eauto.
      * rewrite app_length. simpl; lia. 
      * eapply Forall_app; eauto.
  Qed.

  Lemma ae_taken_ev_taken_k (x: A) (e: trace x) i k:
    ae_taken x e i → ev_taken_k x e i k.
  Proof.
    intros Hae. 
    by apply tr2fun_ev_taken_k, ae_taken_tr2fun_k.
  Qed.

  Definition weak_fair (x: A) (e: trace x) : Prop :=
    ∀ i, ea_enabled x e i → ae_taken x e i.

  Lemma eq_ext_tr2fun (x: A) (e e': trace x):
    eq_ext e e' ↔ ∀ i, tr2fun e i = tr2fun e' i.
  Proof.
    split.
    - intros Heq i. revert x e e' Heq. 
      induction i.
      * intros; destruct Heq; auto.
      * intros. destruct Heq; eauto.
    - revert x e e'. cofix COFIX. 
      intros x e e' Hf. 
      destruct e as [i x y r e]. 
      destruct e' as [i' x y' r' e']. 
      specialize (tr2fun_succ _ (trace_step i x y r e) O). intros.
      specialize (tr2fun_succ _ (trace_step i' x y' r' e') O). intros.
      generalize (Hf O); intros HfO; simpl in HfO; inversion HfO; subst.
      generalize (Hf 1); intros Hf1; simpl in Hf1.
      destruct e, e'. simpl in *. 
      inversion Hf1. subst. econstructor.
      eapply COFIX. eauto.
      intros. specialize (Hf (S i)); eauto.
  Qed.
      
  Lemma suffix_tr2fun (x y: A) (e: trace x) (e': trace y):
    suffix e e' ↔ ∃ k, ∀ i, tr2fun e' i = tr2fun e (k + i).
  Proof.
    split.
    - induction 1.
      * exists O. symmetry. eapply eq_ext_tr2fun; auto.
      * edestruct IHsuffix as (k & ?).
        exists (S k). intros i'. simpl; eauto.
    - intros (k&Hf). revert x y e e' Hf. induction k.
      * intros. simpl in Hf.
        assert (x = y).
        {
          specialize (tr2fun_hd x e). specialize (tr2fun_hd y e').
          specialize (Hf O). rewrite Hf. congruence.
        }
        subst. 
        eapply suffix_eq.
        eapply eq_ext_tr2fun; auto.
      * intros. destruct e. econstructor. 
        eapply IHk. intros i'. eapply Hf.
  Qed.
  
  Lemma suffix_always (x y: A) (e: trace x) (e': trace y) P:
    suffix e e' → always (λ i x _ _ _, P (x, i)) _ e → always (λ i x _ _ _ , P (x, i)) _ e'.
  Proof.
    rewrite suffix_tr2fun.
    rewrite ?tr2fun_al.
    intros (k & Hshift) Hal i.
    rewrite Hshift. eauto.
  Qed.

  Lemma suffix_eventually (x y: A) (e: trace x) (e': trace y) P:
    suffix e e' → eventually (λ i x _ _ _, P (x, i)) _ e' → eventually (λ i x _ _ _ , P (x, i)) _ e.
  Proof.
    rewrite suffix_tr2fun.
    rewrite ?tr2fun_ev.
    intros (k & Hshift) (k' & Hev).
    exists (k + k').
    rewrite <-Hshift. eauto.
  Qed.
    
  Lemma suffix_al_ev P (x y: A) (e: trace x) (e': trace y) (HS: suffix e e'):
    always (λ i x y HR e, eventually (λ i x _ _ _, P (x, i)) x (trace_step _ _ _ HR e)) x e ↔
    always (λ i x y HR e, eventually (λ i x _ _ _, P (x, i)) x (trace_step _ _ _ HR e)) y e'.
  Proof.
    rewrite suffix_tr2fun in HS.
    rewrite ?tr2fun_al_ev.
    destruct HS as (kshift & Hshift).
    split; intros Hae.
    - intros k. 
      specialize (Hae (kshift + k)). 
      destruct Hae as (k' & ? & ?).
      assert (exists k'0, kshift + k'0 = k') as (k'0 & Heq).
      { exists (k' - kshift). lia. }
      exists k'0; split.
      * lia.
      * rewrite Hshift. rewrite Heq. auto.
    - intros k.
      specialize (Hae k).
      destruct Hae as (k' & ? & ?).
      exists (kshift + k'); split.
      * lia.
      * rewrite <-Hshift. auto.
  Qed.

  Lemma suffix_ev_al P (x y: A) (e: trace x) (e': trace y) (HS: suffix e e'):
    eventually (λ i x y HR e, always (λ i x _ _ _, P (x, i)) x (trace_step _ _ _ HR e)) x e ↔
    eventually (λ i x y HR e, always (λ i x _ _ _, P (x, i)) x (trace_step _ _ _ HR e)) y e'.
  Proof.
    rewrite suffix_tr2fun in HS.
    rewrite ?tr2fun_ev_al.
    destruct HS as (kshift & Hshift).
    split; intros (ke & Hev).
    - exists (kshift + ke). intros k' ?. 
      rewrite Hshift. eapply Hev. lia.
    - exists (kshift + ke). intros k' ?. 
      replace k' with (kshift + (k' - kshift)) by lia.
      rewrite <-Hshift. 
      eapply Hev. lia.
  Qed.

  Lemma suffix_weak_fair:
    ∀ x y (e: trace x) (e': trace y), 
      suffix e e' →
      weak_fair x e ↔ weak_fair y e'.
  Proof.
    unfold weak_fair. intros HS.
    split; intros Hwf i;
    specialize (Hwf i);
    rewrite ea_enabled_equiv in *;
    rewrite ae_taken_equiv in *;
    unfold ea_enabled', al_enabled' in *;
    unfold ae_taken', ev_taken' in *.
    - rewrite <-(suffix_ev_al (λ p, enabled (fst p) i)); eauto.
      rewrite <-(suffix_al_ev (λ p, i = snd p)); eauto.
    - rewrite (suffix_ev_al (λ p, enabled (fst p) i)); eauto.
      rewrite (suffix_al_ev (λ p, i = snd p)); eauto.
  Qed.

      
End definitions.

Arguments suffix {_ _ _ _} _ _.
Arguments weak_fair {_ _ _} _.
Arguments trace_step {_ _} _ _ _ _ _.
Arguments tr2fun {_ _ _} _ _.
Arguments al_enabled {_ _ _} _ _.
Arguments al_disabled {_ _ _} _ _.
Arguments ea_enabled {_ _ _} _ _.
Arguments ea_disabled {_ _ _} _ _.
Arguments ev_taken {_ _ _} _ _.
Arguments ev_taken_k {_ _ _} _ _ _.
Arguments ae_taken {_ _ _} _ _.
Arguments ae_taken_k {_ _ _} _ _ _.

(** * General theorems *)
Section weak.
  Context `{R : nat → relation A}.
  
  Hint Constructors nsteps isteps isteps_aux isteps_aux'.
  Definition R' : relation A := λ x y, ∃ i, R i x y.
  
  Lemma nsteps_isteps: 
    ∀ n x y, nsteps R' n x y → ∃ l, isteps R l x y ∧ length l = n.
  Proof.
    induction 1 as [| n' x y z (i&HR) Hn' (l&?&?)]; simpl; eauto.
    exists (i :: l); split; simpl; eauto.
  Qed.

  Lemma isteps_nsteps: 
    ∀ l x y, isteps R l x y → nsteps R' (length l) x y.
  Proof.
    induction 1 as [| i l' x y z HR Hl' IH]; simpl; eauto.
    econstructor; [exists i; eauto|]; auto.
  Qed.

  Lemma isteps_once i x y: R i x y ↔ isteps R [i] x y.
  Proof. 
    split; eauto.
    intros H. inversion H as [|? ? ? ? ? ? Hnil].
    inversion Hnil; subst. auto.
  Qed.

  Lemma isteps_app x y z l1 l2: isteps R l1 x y → isteps R l2 y z → isteps R (l1 ++ l2) x z.
  Proof. induction 1; simpl; eauto. Qed.

  Lemma isteps_aux_app x y z l1 l2: 
    isteps_aux R l1 x y → isteps_aux R l2 y z → isteps_aux R (l1 ++ l2) x z.
  Proof. induction 1; simpl; eauto. Qed.

  Lemma isteps_aux'_app x y z l1 l2: 
    isteps_aux' R l1 x y → isteps_aux' R l2 y z → isteps_aux' R (l1 ++ l2) x z.
  Proof. induction 1; simpl; eauto. Qed.

  Lemma isteps_r l i x y z : isteps R l x y → R i y z → isteps R (l ++ [i]) x z.
  Proof. induction 1; simpl; eauto. Qed.

  Lemma isteps_aux_r l i x y z : isteps_aux R l x y → R i y z → isteps_aux R (l ++ [(i, z)]) x z.
  Proof. induction 1; simpl; eauto. Qed.

  Lemma isteps_aux'_cons l l' b i b' i' x y: 
    isteps_aux' R (l ++ (b, i) :: (b', i') :: l') x y → 
    R i b b'.
  Proof.
    revert l' b i b' i' x y.
    induction l; intros l' b i b' i' x y.
    - rewrite app_nil_l. inversion 1 as [|? ? x' y' ? HS Histeps].
      subst. inversion Histeps. subst. eauto.
    - inversion 1. subst. eauto.
  Qed.
  
End weak.


Section cofair.

  Context `(R1: nat → relation A).
  Context `(R2: nat → relation B).
  Context (A_dec_eq: forall (x y: A), {x = y} + {x <> y}).
  Context (B_dec_eq: forall (x y: B), {x = y} + {x <> y}).

  Ltac existT_eq_elim :=
    repeat (match goal with 
              [ H: existT ?x ?e1 = existT ?x ?e2 |- _ ] =>
              apply Eqdep_dec.inj_pair2_eq_dec in H; eauto
            end).

  Lemma ae_taken_k_ev_taken_k (x: A) (e: trace R1 x) i k:
    ae_taken_k e i k → ev_taken_k e i k.
  Proof.
    destruct 1; auto.
  Qed.


  Definition fair_pres {x y} (e: trace R1 x) (e': trace R2 y) : Prop :=
    weak_fair e → weak_fair e'.

  CoInductive enabled_reflecting: forall {x y}, trace R1 x → trace R2 y → Prop :=
    | enabled_reflecting_hd x1 i1 x1' HR1 e1 x2 i2 x2' HR2 e2: 
        (∀ i, enabled R2 x2 i → enabled R1 x1 i) →
        enabled_reflecting e1 e2 →
        enabled_reflecting (trace_step i1 x1 x1' HR1 e1) (trace_step i2 x2 x2' HR2 e2).

  Lemma enabled_reflecting_al_enabled:
    ∀ {x y} (e: trace R1 x) (e': trace R2 y) i, 
      enabled_reflecting e e' → al_enabled e' i → al_enabled e i.
  Proof.
    cofix COFIX. intros x y e e' i Hco Hale.
    destruct Hco.
    inversion Hale; subst.
    econstructor. 
    * auto.
    * eapply COFIX; eauto.
      existT_eq_elim; subst; auto.
  Qed.

  Lemma enabled_reflecting_ea_enabled:
    ∀ {x y} (e: trace R1 x) (e': trace R2 y) i, 
      enabled_reflecting e e' → ea_enabled e' i → ea_enabled e i.
  Proof.
    intros x y e e' i Hcoe Hea.
    revert x e Hcoe. 
    induction Hea.
    * econstructor. eapply enabled_reflecting_al_enabled; eauto.
    * intros. inversion Hcoe.
      existT_eq_elim; subst.
      eapply ea_enabled_later; eauto.
  Qed.
  
  CoInductive co_step: forall {x y}, trace R1 x → trace R2 y → Prop :=
    | co_step_hd i x1 x1' HR1 e1 x2 x2' HR2 e2: 
        co_step e1 e2 →
        co_step (trace_step i x1 x1' HR1 e1) (trace_step i x2 x2' HR2 e2).

  Lemma co_step_ev_taken:
    ∀ {x y} (e: trace R1 x) (e': trace R2 y) i, 
      co_step e e' → ev_taken e i → ev_taken e' i.
  Proof.
    intros x y e e' i Hcoe Hea.
    revert y e' Hcoe. 
    induction Hea as [| ? ? ? ? ? ? ? IHHea].
    * intros. inversion Hcoe. repeat (existT_eq_elim; subst).
      econstructor.
    * intros. inversion Hcoe.
      existT_eq_elim; subst.
      eapply ev_taken_later; eauto.
  Qed.

  Lemma co_step_ae_taken:
    ∀ {x y} (e: trace R1 x) (e': trace R2 y) i, 
      co_step e e' → ae_taken e i → ae_taken e' i.
  Proof.
    cofix COFIX. intros x y e e' i Hco Hae.
    destruct Hco.
    inversion Hae; repeat (existT_eq_elim; subst).
    econstructor. 
    * eapply co_step_ev_taken; eauto.
      econstructor; eauto.
    * eapply COFIX; eauto.
  Qed.
                   
  Lemma co_se_fair_pres:
    forall {x y} (e: trace R1 x) (e': trace R2 y), 
      enabled_reflecting e e' → co_step e e' → fair_pres e e'.
  Proof.
    intros ? ? e e' Hcoe Hcos.
    unfold fair_pres, weak_fair; intro Hwf.
    intros i Hea. specialize (Hwf i).
    eapply co_step_ae_taken; eauto.
    eapply Hwf. 
    eapply enabled_reflecting_ea_enabled; eauto.
  Qed.

  (* It's maybe somewhat awkward to construct a trace then after the
     fact show that it was co-enabled/step with an original
     trace. Instead we define a way to coinductively build such a
     trace and establish these properties as you go. *)

  CoInductive co_se: forall {x1 x2}, trace R1 x1 → trace R2 x2 → Type :=
    | co_se_hd i x1 x1' HR1 e1 x2 x2' HR2 e2: 
        co_se e1 e2 →
        (∀ i, enabled R2 x2 i → enabled R1 x1 i) →
        co_se (trace_step i x1 x1' HR1 e1) (trace_step i x2 x2' HR2 e2).

  CoInductive co_se_trace: forall {x}, trace R1 x → B → Type :=
    | co_se_trace_hd i x1 x1' HR1 e1 x2 x2': 
        (∀ i, enabled R2 x2 i → enabled R1 x1 i) →
        R2 i x2 x2' →
        co_se_trace e1 x2' →
        co_se_trace (trace_step i x1 x1' HR1 e1) x2.

  Lemma co_se_elim {x1: A} {x2: B} (e1: trace R1 x1) (e2: trace R2 x2):
    co_se e1 e2 → co_step e1 e2 ∧ enabled_reflecting e1 e2.
  Proof.
    intros Hcose.
    split; revert x1 x2 e1 e2 Hcose; cofix COFIX.
    - intros. destruct Hcose. econstructor; eauto.
    - intros. destruct Hcose. econstructor; eauto.
  Qed.
  
  CoFixpoint co_se_trace_extract {x1: A} {e: trace R1 x1} {x2: B} (ST: co_se_trace e x2) : trace R2 x2.
    destruct ST. econstructor; eauto.
  Defined.

  Lemma co_se_trace_valid {x1} {e: trace R1 x1} {x2} (ST: co_se_trace e x2):
    co_se e (co_se_trace_extract ST).
  Proof.
    revert x1 e x2 ST; cofix COFIX; intros.
    destruct ST; subst. 
    unfold co_se_trace_extract.
    rewrite trace_aux_id; simpl.
    econstructor; eauto.
  Qed.
  
  Lemma co_se_trace_fair_pres: 
    forall x1 (e: trace R1 x1) x2, 
      co_se_trace e x2 →
      exists (e': trace R2 x2), fair_pres e e'.
  Proof.
    intros x1 e x2 ST.
    generalize (co_se_elim e _ (co_se_trace_valid ST)).
    intros (?&?).
    exists (co_se_trace_extract ST).
    eapply co_se_fair_pres; auto.
  Qed.
  
  Section erasure.
  Import ClassicalEpsilon.
  Context (erasure: A → B).
  Context (enabled_reflecting: ∀ i a, enabled R2 (erasure a) i → enabled R1 a i).
  Context (estep_dec: ∀ `(HR: R1 i a b),  {R2 i (erasure a) (erasure b)} + 
                                          {¬ (R2 i (erasure a) (erasure b)) ∧
                                           erasure a = erasure b}).
  
  Definition estep `(HR: R1 i a b) := R2 i (erasure a) (erasure b).
  
  Inductive ev_estep: forall (x : A), trace R1 x → nat → Prop :=
    | ev_estep_now i x y e (HR: R1 i x y): estep HR → ev_estep x (trace_step i x y HR e) i
    | ev_estep_later i i' x y e (HR: R1 i' x y): 
        ev_estep y e i -> ev_estep x (trace_step i' x y HR e) i.

  Definition ev_estep' x (e: trace R1 x) i := 
    eventually R1 (λ i' x y _ _, i = i' ∧ R2 i (erasure x) (erasure y)) _ e.

  Definition ae_estep x (e: trace R1 x) i := 
    always R1 (λ _ x y HR e, ev_estep' x (trace_step _ _ _ HR e) i) x e.

   Lemma ev_estep_equiv: ∀ x e i, 
       ev_estep x e i ↔ ev_estep' x e i.
   Proof.
     unfold ev_estep'.
     split.
     - revert x e i. induction 1.
       * destruct e.
         eapply ev_now; auto.
       * destruct e.
         eapply ev_later; eauto.
     - revert x e i. induction 1 as [ ? ? ? ? ? (?&?) | ?].
       * subst. destruct e.
         eapply ev_estep_now; eauto.
       * destruct e.
         eapply ev_estep_later.
         eauto.
   Qed.

   
  CoInductive ae_ev_estep: forall (x : A), trace R1 x → Prop :=
    | ae_ev_estep_hd i x y e (HR: R1 i x y): 
        (∀ i', ae_taken (trace_step i x y HR e) i' → ae_estep x (trace_step i x y HR e) i') →
        (∃ i', ev_estep x (trace_step i x y HR e) i') →
        ae_ev_estep y e →
        ae_ev_estep x (trace_step i x y HR e).
  
  Lemma ae_ev_estep_intro_all:
     (∀ x (e: trace R1 x) i', ae_taken e i' → ae_estep x e i') →
     (∀ x e, ∃ i', ev_estep x e i') →
     (∀ x e, ae_ev_estep x e).
  Proof.
    cofix COFIX. intros. destruct e; econstructor; [| | eauto]; eauto.
  Qed.
    
  Lemma ae_ev_estep_destr_clean x e:
    ae_ev_estep x e →
    ∃ i, ev_estep x e i.
  Proof.
    intros Hae. destruct Hae. auto.
  Qed.

  
  Lemma ae_ev_estep_yield_fun_init x (e: trace R1 x): 
    ae_ev_estep x e →
    ∃ k', k' ≥ 0 ∧ R2 (snd (tr2fun e k')) 
                             (erasure (fst (tr2fun e k'))) 
                             (erasure (fst (tr2fun e (S k'))))
               ∧ (∀ k'', k'' ≥ 0 ∧ k'' < k' →  
                         (¬ R2 (snd (tr2fun e k''))
                            (erasure (fst (tr2fun e k'')))
                            (erasure (fst (tr2fun e (S k''))))))
               ∧ (∀ k'', k'' ≥ 0 ∧ k'' ≤ k' →  
                         erasure (fst (tr2fun e 0)) = erasure (fst (tr2fun e k''))).
  Proof.
    intros Hae.
    generalize (ae_ev_estep_destr_clean _ _ Hae).
    intros (i & Hev).
    revert Hae.
    induction Hev.
    * intros. exists O. 
      split_and!; auto. 
      ** simpl. destruct e; auto.
      ** intros; lia.
      ** intros. assert (k'' = 0) by lia. subst. auto.
    * intros.
      (* Two cases: either the current head is an estep, in which case
         take O as our index. If not, use the IH *)
      destruct (estep_dec _ _ _ HR) as [? | (?&?)].
      ** exists 0. split_and!; auto.
         *** simpl. destruct e; auto.
         *** intros. lia.
         *** intros. assert (k'' = 0) by lia. subst. auto.
      ** edestruct IHHev as (k' & ? & Hstep &  Hnstep & Heq). 
         { inversion Hae. subst. existT_eq_elim. }
         exists (S k'). split_and!; auto.
         *** intros. 
             destruct k''.
             **** simpl. destruct e; auto.
             **** simpl. destruct e. simpl in *; eapply Hnstep. lia.
         *** intros. 
             destruct k''.
             **** simpl. destruct e; auto.
             **** simpl. destruct e. rewrite <-Heq; simpl; auto.
                  lia.
  Qed.

  Lemma ae_ev_estep_shift x (e: trace R1 x): 
    ae_ev_estep x e →
    ∀ k, ∃ x' e', ae_ev_estep x' e' ∧ (∀ i, tr2fun e' i = tr2fun e (k + i)).
  Proof.
    intros Hae k.
    revert x e Hae.
    induction k; eauto.
    intros. 
    destruct Hae. edestruct (IHk _ _ Hae) as (y' & e' & ? & ?).
    exists y', e'; split_and!; auto.
  Qed.

  Lemma ae_ev_estep_yield_fun x (e: trace R1 x): 
    ae_ev_estep x e →
    ∀ k, ∃ k', k' ≥ k ∧ R2 (snd (tr2fun e k')) 
                             (erasure (fst (tr2fun e k'))) 
                             (erasure (fst (tr2fun e (S k'))))
               ∧ (∀ k'', k'' ≥ k ∧ k'' < k' →  
                         (¬ R2 (snd (tr2fun e k''))
                            (erasure (fst (tr2fun e k'')))
                            (erasure (fst (tr2fun e (S k''))))))
               ∧ (∀ k'', k'' ≥ k ∧ k'' ≤ k' →  
                         erasure (fst (tr2fun e k)) = erasure (fst (tr2fun e k''))).
  Proof.
    intros Hae k.
    edestruct (ae_ev_estep_shift _ _ Hae k) as (e' & ? & Hae' & Hshifteq).
    eapply ae_ev_estep_yield_fun_init in Hae'.
    destruct Hae' as (k' & ? & Hs & Hns & Heq).
    exists (k + k').
    split_and!; intros; rewrite <-?Hshifteq.
    - lia.
    -  replace (S (k + k')) with (k + S k') by lia. rewrite <-Hshifteq.
       auto.
    - replace (k'') with (k + (k'' - k)) by lia.
      replace (S (k + (k'' - k))) with (k + S (k'' - k)) by lia. 
      rewrite <-?Hshifteq. eapply Hns. lia. 
    - replace (k) with (k + 0) by lia.
      replace (k'') with (k + (k'' - k)) by lia.
      rewrite <-?Hshifteq. eapply Heq. lia.
  Qed.
  
  Definition aes_fun {x} (e: trace R1 x) (Hae: ae_ev_estep x e) :=
    λ k, proj1_sig (constructive_indefinite_description _ (ae_ev_estep_yield_fun x e Hae k)).

  Fixpoint skip_fun' {x} (e: trace R1 x) (Hae: ae_ev_estep x e) k :=
    match k with
      | O => aes_fun e Hae 0
      | S k' => aes_fun e Hae (S (skip_fun' e Hae k'))
    end.
  
  Definition skip_fun {x} (e: trace R1 x) (Hae: ae_ev_estep x e) k :=
    (erasure (fst (tr2fun e (skip_fun' e Hae k))), snd (tr2fun e (skip_fun' e Hae k))).

  Lemma skip_fun_tfun: ∀ {x} (e: trace R1 x) Hae, tfun R2 (skip_fun e Hae).
  Proof.
    unfold tfun. intros.
    (* N.B. the destruct k here is just to force some things to simplify/unroll,
       I don't think it's actually necessary *)
    destruct k.
    - unfold skip_fun, skip_fun'. 
      unfold aes_fun.
      destruct (constructive_indefinite_description) as (k1&Hge1&Hestep1&Hnext1&Heq1).
      destruct (constructive_indefinite_description) as (k2&Hge2&Hestep2&Hnext2&Heq2).
      simpl proj1_sig in *.
      simpl. rewrite <-(Heq2 k2); eauto.
    - simpl.
      unfold aes_fun.
      destruct (constructive_indefinite_description) as (k1&Hge1&Hestep1&Hnext1&Heq1).
      destruct (constructive_indefinite_description) as (k2&Hge2&Hestep2&Hnext2&Heq2).
      simpl proj1_sig in *.
      simpl. rewrite <-(Heq2 k2); eauto.
  Qed.
    

  Lemma skip_fun'_strict_S {x} (e: trace R1 x) Hae: 
    ∀ k, skip_fun' e Hae k < skip_fun' e Hae (S k).
  Proof.
    intros. simpl.
    unfold aes_fun at 1.
    destruct (constructive_indefinite_description) as (k1&Hge1&Hestep1&Hnext1&Heq1).
    simpl. lia.
  Qed.


  Lemma skip_fun'_strict_monotone {x} (e: trace R1 x) Hae: 
    ∀ k k', k < k' → skip_fun' e Hae k < skip_fun' e Hae k'.
  Proof.
    induction 1; eauto using skip_fun'_strict_S.
    etransitivity; eauto; eapply skip_fun'_strict_S.
  Qed.

  Lemma skip_fun'_order_embedding {x} (e: trace R1 x) Hae: 
    ∀ k k', skip_fun' e Hae k ≤ skip_fun' e Hae k' → k ≤ k'.
  Proof.
    intros; assert (k ≤ k' ∨ k' < k) as [?|Hlt] by lia; auto.
    eapply (skip_fun'_strict_monotone e Hae _ _) in Hlt.
    lia.
  Qed.

  Lemma skip_fun'_between {x} (e: trace R1 x) Hae: 
    ∀ k, ∃ k', 
        skip_fun' e Hae k' ≥ k ∧ 
        match k' with
          | 0 => True
          | S k0 => skip_fun' e Hae k0 < k
        end.
  Proof.
    induction k. 
    - exists O. simpl. split.
      * lia.
      * auto.
    - destruct IHk as (k' & ? & ?).
      assert (skip_fun' e Hae k' ≥ S k ∨ skip_fun' e Hae k' = k) as [? | ?] by lia.
      * exists k'. split; auto.
        destruct k'; auto.
      * exists (S k'). split; auto. 
        ** simpl. unfold aes_fun.
           destruct (constructive_indefinite_description) as (k1&Hge1&Hestep1&Hnext1&Heq1).
           simpl. lia.
        ** subst; auto.
  Qed.
        
  Lemma skip_fun'_hits_all {x} (e: trace R1 x) Hae: 
    ∀ k, R2 (snd (tr2fun e k))
             (erasure (fst (tr2fun e k)))
             (erasure (fst (tr2fun e (S k))))
             → ∃ k', skip_fun' e Hae k' = k.
  Proof.
    intros k Hestep.
    edestruct (skip_fun'_between _ Hae k) as (k'&Hk').
   
    destruct Hk' as (Hge&Hlt).
    assert (skip_fun' e Hae k' = k ∨ skip_fun' e Hae k' > k) as [?|Hgt] by lia; eauto.
    destruct k'; simpl in Hgt; unfold aes_fun in Hgt;
    destruct (constructive_indefinite_description) as (k1&Hge1&Hestep1&Hnext1&Heq1);
    simpl in Hgt;
    exfalso; eapply (Hnext1 k); eauto; lia.
  Qed.

  Lemma skip_fun'_infl_matching {x} (e: trace R1 x) Hae: 
    ∀ k, ∃ j, skip_fun' e Hae j ≥ k ∧ 
              (∀ k', skip_fun' e Hae j ≥ k' → k' ≥ k →
                     erasure (fst (tr2fun e (skip_fun' e Hae j))) =
                     erasure (fst (tr2fun e k'))).
  Proof.
    induction k.
    - exists 0. simpl skip_fun'. unfold aes_fun.
      destruct (constructive_indefinite_description) as (k1&Hge1&Hestep1&Hnext1&Heq1).
      split_and!; auto.
      symmetry. simpl. rewrite <-Heq1; [| simpl in *; lia].
      eapply Heq1; lia. 
    - edestruct IHk as (j0&?&Heq).
      assert (skip_fun' e Hae j0 ≥ S k ∨ skip_fun' e Hae j0 = k) as [?|?] by lia.
      * exists j0; split_and!; eauto.
        intros. eapply Heq; auto.
        lia.
      * exists (S j0).
        simpl; unfold aes_fun.
        destruct (constructive_indefinite_description) as (k1&Hge1&Hestep1&Hnext1&Heq1).
        simpl. 
        split_and!.
        ** lia.
        ** intros. rewrite <-Heq1; [| simpl in *; lia].
           eapply Heq1; lia. 
  Qed.
  
  Lemma ae_ev_estep_yield_fun_fair x (e: trace R1 x):
      ae_ev_estep x e →
      exists (e': trace R2 (erasure x)), fair_pres e e'.
  Proof.
    intros Hae.
    pose (f := fun2tr R2 O (skip_fun e Hae) (skip_fun_tfun _ _)).
    assert (Hhd: (skip_fun e Hae 0).1 = erasure x).
    { simpl.
      unfold aes_fun.
      destruct (constructive_indefinite_description) as (k1&Hge1&Hestep1&Hnext1&Heq1).
      simpl. rewrite <-Heq1; [|lia]. simpl; destruct e; auto.
    }
    eauto.
    destruct Hhd.
    exists f.
    
    unfold fair_pres, weak_fair.
    intros Hfair i Hea_f.
    assert (ea_enabled e i) as Hea_e.
    {
      eapply ea_enabled_equiv.
      eapply ea_enabled_equiv in Hea_f.
      unfold ea_enabled', al_enabled' in *.
      eapply (tr2fun_ev_al _ _ _ (λ p, enabled R1 (fst p) i)).
      eapply (tr2fun_ev_al _ _ _ (λ p, enabled R2 (fst p) i)) in Hea_f.
      unfold f in Hea_f. simpl in Hea_f.
      setoid_rewrite (tr2fun_fun2tr R2 0 (skip_fun e Hae) (skip_fun_tfun e Hae)) in Hea_f.
      setoid_rewrite plus_0_l in Hea_f.
      unfold skip_fun in Hea_f. simpl in Hea_f.

      destruct Hea_f as (k&Hea_f).
      exists (skip_fun' e Hae k). intros k' Hge.
      edestruct (skip_fun'_infl_matching _ Hae k') as (k'' & ? & Heq).
      eapply enabled_reflecting.
      erewrite <-Heq; eauto.
      eapply Hea_f.
      cut (k ≤ k''); auto.
      eapply (skip_fun'_order_embedding e Hae). lia.
    }
    generalize (Hfair i Hea_e) as Hae_e; intros.
    

      eapply ae_taken_equiv.
      unfold ae_taken', al_enabled' in *.
      rewrite (tr2fun_al_ev _ _ _ (λ p, i = snd p)).
      
      assert (ae_estep x e i) as Hae_estep by (destruct Hae; eauto).

      unfold ae_estep, ev_estep' in Hae_estep.
      rewrite
        (tr2fun_al_ev_2 R1 _ e (λ p y, i = snd p ∧ R2 i (erasure (fst p)) (erasure y))) in Hae_estep.
      intros k. 
      specialize (Hae_estep (skip_fun' e Hae k)).
      destruct Hae_estep as (k' & ? & ? & HR2).
      subst.
      edestruct (skip_fun'_hits_all e Hae) as (kchoice & ?); eauto.
      exists kchoice. unfold f.
      rewrite tr2fun_fun2tr. rewrite plus_0_l.
      split; [|subst; auto].
      cut (k ≤ kchoice); auto.
      eapply (skip_fun'_order_embedding e Hae). lia.
  Qed.
  End erasure.

  Section block.
    Import stdpp.propset ClassicalEpsilon.
    Context (flatten: A → B * (nat → propset nat)).
    Context (flatten_spec_step:
               ∀ i a a',  R1 i a a' → 
                          (∃ l, isteps R2 l ((flatten a).1) ((flatten a').1)
                           ∧ ∀ j, j ∈ (flatten a).2 i → 
                                  j ∈ l ∨ ¬ enabled R2 ((flatten a').1) j)). 
    Context (flatten_spec_cover:
               ∀ a i, enabled R2 ((flatten a).1) i →
                      ∃ j,  i ∈ (flatten a).2 j).
    Context (flatten_spec_enabled_reflect: 
               ∀ a i j, enabled R2 ((flatten a).1) i ∧ i ∈ (flatten a).2 j → 
                        enabled R1 a j).
    Context (flatten_spec_enabled_some:
               ∀ a i, enabled R1 a i → ∃ j, j ∈ (flatten a).2 i ∧ enabled R2 ((flatten a).1) j). 
    Context (flatten_spec_nonstep_mono:
               ∀ a a' i, R1 i a a' →
                         (∀ j, j ≠ i → ((flatten a).2) j ⊆ ((flatten a').2) j)).

    (*
    Lemma isteps_lookup: ∀ l b1 b2 n,  isteps R2 l b1 b2 → n < length l → 
                                       ∃ l1 i l2 b, l = l1 ++ i :: l2 ∧ 
                                                    isteps R2 (l1 ++ [i]) b1 b ∧
                                                    isteps R2 l2 b b2.
    Proof.
      intros l b1 b2 n Hsteps. revert n. induction Hsteps.
      - simpl. intros; lia.
      - simpl. intros. 
        destruct n. 
        * exists [], i, l. eexists. split_and!; rewrite ?app_nil_l; eauto.
          eapply isteps_once; eauto.
        * edestruct (IHHsteps n) as (l1&i'&l2&b&?&?&?).
          ** lia.
          ** exists (i :: l1), i', l2, b; subst.
             split_and!; eauto.
             rewrite <-app_comm_cons.
             econstructor; eauto.
    Qed.
    *)

    Lemma isteps_lookup: ∀ l b1 b2 n,  isteps R2 l b1 b2 → n < length l → 
                                       ∃ bi l1 l2, l = l1 ++ (bi.2) :: l2 ∧ 
                                                    isteps R2 l1 b1 (bi.1) ∧
                                                    isteps R2 (bi.2 :: l2) (bi.1) b2 ∧
                                                    length l1 = n. 
    Proof.
      intros l b1 b2 n Hsteps. revert n. induction Hsteps.
      - simpl. intros; lia.
      - simpl. intros. 
        destruct n. 
        * exists (x, i). 
          exists [], l. split_and!; rewrite ?app_nil_l; eauto; try econstructor; eauto.
        * edestruct (IHHsteps n) as ((b&i')&l1&l2&?&?&?&?).
          ** lia.
          ** exists (b, i'), (i :: l1), l2; subst.
             split_and!; eauto.
             econstructor; eauto.
    Qed.

    
    (* Alternate idea: first write a function "glue", which given
       trace e and nat n, glues together the blocks from the first n steps of
       e, which yields an istep list of length at least n. Then the trace fun
       does (roughly) isteps_lookup of k on the combined block from glue k*)
    (* This is quite fine if istep is only non-det up to the idx. But, 
       otherwise we sort of need to retain more information... write something stronger than
       isteps that actually carries the list of all the states as well in addition to the indices that
       stepped. then glue these together, and then the function just gets the nth of these *)

    Definition glue (a: A) (e: trace R1 a) (n: nat) : list (B * nat).
    Proof.
      revert a e.
      induction n; intros.
      - exact [].
      - destruct e as [i x y HS e'].
        destruct (constructive_indefinite_description _ (flatten_spec_step _ _ _ HS)) as (l&Histeps&?).
        destruct (constructive_indefinite_description _ (isteps_augment _ _ _ _ Histeps)) as (l'&?&?).
        eapply (l' ++ IHn _ e').
    Defined.
    
    Lemma glue_length: ∀ a e n, length (glue a e n) ≥ n.
    Proof.
      intros a e n. revert a e; induction n; intros; [lia|].
      simpl. destruct e as [i x y HS e']. destruct constructive_indefinite_description 
        as (l & Histeps & Hdisj).
      destruct constructive_indefinite_description as (l' &?&?).
      rewrite app_length. 
      destruct l. 
      - exfalso. edestruct (flatten_spec_enabled_some x i) as (j&?&?).
          { econstructor; eauto. }
          destruct (Hdisj j) as [?|Hne]; eauto.
          ** set_solver.
          ** eapply Hne. inversion Histeps. subst. eauto.
      - simpl in *. specialize (IHn _ e'). 
        destruct l'; [simpl in *; congruence|].
        simpl. lia.
    Qed.

    Lemma glue_length_S: ∀ a e n, n < length (glue a e (S n)).
    Proof.
      intros.
      eapply (lt_le_trans n (S n)); auto.
      apply glue_length.
    Qed.
      
    Lemma glue_prefix_S: ∀ a e n, prefix (glue a e n) (glue a e (S n)).
    Proof.
      intros a e n. revert a e; induction n; intros.
      - simpl; eexists; eauto.
      - destruct e as [i x y HS e'].
        specialize (IHn _ e').
        simpl in *. destruct constructive_indefinite_description 
          as (l & Histeps & Hdisj).
        destruct constructive_indefinite_description as (l'&?&?).
        eapply prefix_app; eauto.
    Qed.

    Lemma glue_prefix_le: ∀ a e n n', n ≤ n' → prefix (glue a e n) (glue a e n').
    Proof.
      intros a e n n' Hle. revert a e; induction Hle; intros; auto.
      etransitivity. eauto. eapply glue_prefix_S.
    Qed.
    
    Lemma glue_cons: ∀ i x y HS e n, glue _ (trace_step i x y HS e) (S n) =
                                      glue _ (trace_step i x y HS e) 1 ++
                                           glue _ e n.
    Proof.
      intros i x y HS e n. revert i x y HS e.
      simpl.
      destruct e.
      destruct constructive_indefinite_description as (l & Histeps & Hdisj).
      destruct constructive_indefinite_description as (l' & ? & ?).
      rewrite ?app_nil_r. auto.
    Qed.
      
    Lemma glue_isteps:
      ∀ a e n, isteps_aux' R2 (glue a e n) ((flatten a).1) ((flatten ((tr2fun e n).1)).1).
    Proof.
      intros a e n. revert a e; induction n; intros.
      - simpl. destruct e. simpl. econstructor.
      - destruct e as [i x y HS e'].
        specialize (IHn _ e').
        simpl in *. destruct constructive_indefinite_description 
          as (l & Histeps & Hdisj).
        destruct constructive_indefinite_description as (l'&?&?).
        eapply isteps_aux'_app; eauto.
    Qed.
    
    Definition glue_lookup a e (n: nat) : { bi: B * nat | ∃ l1 l2, glue a e (S n) = l1 ++ bi :: l2 
                                                                   ∧ length l1 = n }.
    Proof.
      specialize (nth_error_None (glue a e (S n)) n). intros Herr.
      specialize (nth_error_split (glue a e (S n)) n). intros Hsplit.
      destruct nth_error as [p|].
      - exists p.
        eapply Hsplit; eauto.
      - exfalso. destruct Herr as (Hfalse&?). specialize (Hfalse eq_refl).
        specialize (glue_length_S a e n). intros. lia.
    Qed.

    Definition flatten_trace_fun (a: A) (e: trace R1 a) (n: nat) : B * nat :=
      proj1_sig (glue_lookup a e n).

    Lemma flatten_trace_tfun: ∀ a (e: trace R1 a), tfun R2 (flatten_trace_fun a e).
    Proof.
      intros a e n.
      unfold flatten_trace_fun.
      destruct glue_lookup as ((b&i)&l1&l2&Heq&Hlen).
      destruct glue_lookup as ((b'&i')&l1'&l2'&Heq'&Hlen').
      simpl. 
      destruct (glue_prefix_S a e (S n)) as (gext&Hext).
      specialize (glue_isteps a e (S (S n))). intros Histeps.
      assert (Heq'': l1 ++ [(b, i)] = l1').
      {
        rewrite Hext in Heq'.
        rewrite Heq in Heq'.
        replace (l1 ++ (b, i) :: l2) with ((l1 ++ [(b, i)]) ++ l2) in Heq' 
          by (simpl; rewrite <-app_assoc; auto).
        rewrite <-app_assoc in Heq'.
        apply app_inj_1 in Heq' as (?&?); auto.
        rewrite app_length. simpl; lia.
      }
      rewrite <-Heq'' in Heq'.
      rewrite Heq' in Histeps.
      replace (((l1 ++ [(b, i)]) ++ (b', i') :: l2')) with
              (l1 ++ (b, i) :: (b', i') :: l2') in Histeps
                by (rewrite <-?app_assoc; simpl; auto). 
      eapply isteps_aux'_cons; eauto.
    Qed.

    Lemma al_ev_classical P:
      ¬ (∃ k, ∀ k',  k' ≥ k → ¬ P k') → (∀ k, ∃ k', k' ≥ k ∧ P k'). 
    Proof.
      intros Hneg k. eapply not_all_not_ex. intros Hf.
      eapply Hneg. exists k. intros k' Hge.
      specialize (Hf k'). apply not_and_or in Hf.
      destruct Hf; [lia|]. 
      auto.
    Qed.

    Lemma flatten_trace_fun_cons_offset: 
      ∀ i x y HS e, ∃ k', k' > 0 ∧ ∀ k,
            flatten_trace_fun _ (trace_step i x y HS e) (k + k') = 
            flatten_trace_fun _ e k.
    Proof.
      intros. unfold flatten_trace_fun.
      simpl.
      exists (length (glue _ (trace_step i x y HS e) 1)).
      split.
      -  specialize (glue_length x (trace_step i x y HS e) 1).
         lia.
      - intros k. 
        destruct glue_lookup as ((b'&i')&l1&l2&Heq&Hlen). 
        destruct glue_lookup as ((b''&i'')&l1'&l2'&Heq'&Hlen'). 
        simpl. 
        rewrite glue_cons in Heq. 
        assert (S k ≤ k + length (glue x (trace_step i x y HS e) 1)) as Hle.
        {
          specialize (glue_length _ (trace_step i x y HS e) 1). 
          lia.
        }
        edestruct (glue_prefix_le _ e _ _ Hle) as (lext&Hext).
        rewrite Hext in Heq. rewrite Heq' in Heq.
        rewrite <-(app_assoc _ _ lext) in Heq.
        rewrite (app_assoc _  l1') in Heq.
        apply app_inj_1 in Heq as (_&Heq); auto.
        * rewrite <-app_comm_cons in Heq. inversion Heq. auto.
        * rewrite ?app_length.
          lia.
    Qed.
 
    Lemma flatten_trace_match: 
      ∀ a e k, ∃ k', k  ≤ k' ∧ (flatten ((tr2fun e k).1)).1 = (flatten_trace_fun a e k').1
                     ∧ ∃ l, isteps_aux' R2 l ((flatten ((tr2fun e k).1)).1)
                                   ((flatten ((tr2fun e (S k)).1)).1)
                           ∧ (∀ j, j ∈ (flatten ((tr2fun e k).1)).2 ((tr2fun e k).2) → 
                                  j ∈ (map snd l) ∨ ¬ enabled R2 ((flatten ((tr2fun e (S k)).1)).1) j)
                           ∧ (∀ idx, idx < length l → 
                                     nth_error l idx = Some (flatten_trace_fun a e (k' + idx))).
    Proof. 
      intros a e k. revert a e.
      induction k.
      - exists 0. split; auto. 
        unfold flatten_trace_fun at 1.
        destruct glue_lookup as ((b&i)&l1&l2&Heq&Hlen). simpl. destruct e as [i' x y HS e]. 
        destruct l1; [| simpl in *; lia].
        simpl.
        assert ((flatten x).1 = b) as ->.
        {
          simpl in *. destruct constructive_indefinite_description as (?&Histep&?).
          destruct constructive_indefinite_description as (?&?&Histep').
          rewrite ?app_nil_l, ?app_nil_r in Heq.
          subst. inversion Histep'; subst. auto.
        }
        split; auto.
        exists ((b, i) :: l2).
        destruct e as [i'' x' y' HS' e]. simpl. 
        split_and!; auto.
        ** simpl in *. destruct constructive_indefinite_description as (?&Histep&?).
             destruct constructive_indefinite_description as (?&?&Histep').
             rewrite ?app_nil_l, ?app_nil_r in Heq.
             subst. inversion Histep'; subst. auto.
        ** simpl in *. destruct constructive_indefinite_description as (?&Histep&?).
             destruct constructive_indefinite_description as (?&?&Histep').
             rewrite ?app_nil_l, ?app_nil_r in Heq.
             subst. inversion Histep'; subst. auto.
        ** intros idx Hlt.
           unfold flatten_trace_fun.
           destruct glue_lookup as ((b2&i2)&l1'&l2'&Heq'&Hlen'). simpl. 
           edestruct (glue_prefix_le _ (trace_step i'  x x' HS (trace_step i'' x' y' HS' e))
                                     1 (S idx)) as (gext & Hext); [lia|].
             rewrite Hext in Heq'.
             rewrite Heq in Heq'.
             rewrite ?app_nil_l, ?app_nil_r in Heq'.

             rewrite <-(nth_error_app1 _ gext); [| simpl; lia].
             rewrite Heq'.
             rewrite nth_error_app2; [| simpl; lia].
             replace (idx - length l1') with 0 by lia.
             auto.
      - intros.
        destruct e as [i x y HS e]. destruct (IHk _ e) as (k' & ? & Hk' & ? & ? & ? & ?).
        destruct (flatten_trace_fun_cons_offset _ _ _ HS e) as (k''&?&Hk'').
        exists (k' + k''). split; [lia|].
        split.
        * simpl. rewrite Hk'. symmetry; f_equal; eauto.
        * eexists; split_and!; eauto.
          intros. replace (k' + k'' + idx) with ((k' + idx) + k'') by lia.
          erewrite Hk''; eauto.
    Qed.


    
    (*
    Definition ft_match a e k k' : Prop := k 

    Lemma flatten_trace_match_min
      ∀ a e k, ∃ k', k  ≤ k' ∧ (flatten ((tr2fun e k).1)).1 = (flatten_trace_fun a e k').1
                     ∧ ∀ k'',  k ≤ k'' ∧ (flatten ((tr2fun e k).1)).1 = (flatten_trace_fun a e k').1
                                                                                                  
    Proof. 
      intros a e k. revert a e.
      induction k.
      - exists 0. split; auto. unfold flatten_trace_fun.
        destruct glue_lookup as ((b&i)&l1&l2&Heq&Hlen). simpl. destruct e. simpl in *.
        destruct constructive_indefinite_description as (?&?&?).
        destruct constructive_indefinite_description as (?&?&?).
        destruct l1; [| simpl in *; lia].
        rewrite app_nil_l, app_nil_r in Heq.
        subst. inversion i2. auto.
      - intros.
        destruct e as [i x y HS e]. destruct (IHk _ e) as (k' & ? & Hk').
        destruct (flatten_trace_fun_cons_offset _ _ _ HS e) as (k''&?&Hk'').
        simpl.
        rewrite Hk'.
        exists (k' + k''). split; [lia|].
        symmetry; f_equal; eauto.
    Qed.

    Lemma flatten_trace_match_gt:
      ∀ a e k1 k1', (flatten ((tr2fun e k1).1)).1 = (flatten_trace_fun a e k1').1 →
                   ∃ k2 k2', k1 < k2 ∧ k1' < k2' ∧
                          (flatten ((tr2fun e k2).1)).1 = (flatten_trace_fun a e k2').1.
    Proof. 
      intros a e k1 k1'.
      

revert a e. induction k.
      - intros. 
        

simpl.
        destruct e. simpl in *.


      intros a e k k1' Heq1.
      destruct e as [i x y HS e].
      simpl in Heq1.
      destruct (flatten_trace_fun_cons_offset i x y HS e) as (koff&?&Hoff).
      simpl. 
      rewrite (Hoff k)
      re
      unfold flatten_trace_fun; intros a e k k1' k2' Hmatch1 Hmatch2.
      destruct glue_lookup as ((b1&i1)&l1a&l1b&Heq1&Hlen1). 
      destruct glue_lookup as ((b2&i2)&l2a&l2b&Heq2&Hlen2). 
      simpl in Hmatch1, Hmatch2.
      destruct e.
      simpl in *. 
      destruct glue_lookup as ((b&i)&l1&l2&Heq&Hlen). 

simpl. destruct e. simpl in *.
      intros. 
      




      intros a e k k1' k2' Heq1 Heq2.
      destruct e as [i x y HS e].
      simpl in Heq2.
      destruct (flatten_trace_fun_cons_offset i x y HS e) as (koff&?&Hoff).
      rewrite (Hoff k)
      re
      unfold flatten_trace_fun; intros a e k k1' k2' Hmatch1 Hmatch2.
      destruct glue_lookup as ((b1&i1)&l1a&l1b&Heq1&Hlen1). 
      destruct glue_lookup as ((b2&i2)&l2a&l2b&Heq2&Hlen2). 
      simpl in Hmatch1, Hmatch2.
      destruct e.
      simpl in *. 
      destruct glue_lookup as ((b&i)&l1&l2&Heq&Hlen). 

simpl. destruct e. simpl in *.
      intros. 
        
    Lemma flatten_trace_eventually_match: 
      ∀ a e k1 k2, ∃ k1' k2', k1' ≥ k1 → k2' ≥ k2 → 
                              (flatten ((tr2fun e k1').1)).1 = (flatten_trace_fun a e k2').1.
    Proof. 
      intros a e k1 k2. revert a e.
      induction k.
      - exists 0. unfold flatten_trace_fun.
        destruct glue_lookup as ((b&i)&l1&l2&Heq&Hlen). simpl. destruct e. simpl in *.
        destruct constructive_indefinite_description as (?&?&?).
        destruct constructive_indefinite_description as (?&?&?).
        destruct l1; [| simpl in *; lia].
        rewrite app_nil_l, app_nil_r in Heq.
        subst. inversion i2. auto.
      - intros.
        destruct e as [i x y HS e]. destruct (IHk _ e) as (k' & Hk').
        destruct (flatten_trace_fun_cons_offset _ _ _ HS e) as (k''&Hk'').
        simpl.
        rewrite Hk'.
        exists (k' + k''). symmetry; f_equal; eauto.
    Qed.
    *)

    Lemma flatten_fair_pres (a: A) (e: trace R1 a):
      exists (e': trace R2 ((flatten a).1)), fair_pres e e'.
    Proof.
      pose (f := fun2tr R2 O (flatten_trace_fun a e) (flatten_trace_tfun _ _)).
      assert (Hhd: (flatten_trace_fun a e 0).1 = (flatten a).1).
      {
        unfold flatten_trace_fun. destruct glue_lookup as (?&l1&l2&Heq&?); simpl.
        specialize (glue_isteps a e 1); intros Histeps.
        destruct l1; [| simpl in *; lia].
        rewrite Heq in Histeps. rewrite app_nil_l in Histeps. inversion Histeps.
        auto.
      }
      destruct Hhd.
      exists f.
      
      unfold fair_pres, weak_fair.
      intros Hfair i Hea_f.
      (* if i is eventually always enabled, but does not eventually always take a step,
         then there exists some k, such that for all n ≥ k, i does not take a step at n.
         Consider the first n' ≥ k at which f is the flatten of some a in e. 
         Since i is enabled, by flatten_spec_cover, there is some j which is enabled in a
         and contains i. Now, it must be the case that j does not step, else i would step 
         in the corresponding flattening. By flatten_spec_nonstep_mono, at the successor of a 
         j continues to contain a, and is still enabled. Continuing, we have that j is ea_enabled,
         so by fairness, it must step, and then we will get the desired step of i in a block *)
               
      
      eapply ae_taken_equiv.
      unfold ae_taken', ev_taken'.
      rewrite (tr2fun_al_ev _ _ _ (λ p, i = snd p)).
      eapply al_ev_classical. intros (k1&Hk1).
      eapply ea_enabled_equiv in Hea_f.
      unfold ea_enabled', al_enabled' in Hea_f.
      eapply (tr2fun_ev_al _ _ _ (λ p, enabled R2 (fst p) i)) in Hea_f.
      destruct Hea_f as (k2&Hk2).
      destruct (flatten_trace_match a e (k1 + k2)) as (kb&Hlt&Hmatch&l&?&Hinblock&Hoffset).
      edestruct (flatten_spec_cover ((tr2fun e (k1 + k2)).1) i) as (j&Hin).
      { rewrite Hmatch. 
        specialize (Hk2 kb).
        unfold f in Hk2. rewrite tr2fun_fun2tr in Hk2.
        simpl in Hk2. eauto.
        eapply Hk2. lia.
      }
      assert (enabled R1 (tr2fun e (k1 + k2)).1 j) as Henj.
      {
        eapply flatten_spec_enabled_reflect.
        rewrite Hmatch.
        specialize (Hk2 kb).
        unfold f in Hk2. rewrite tr2fun_fun2tr in Hk2.
        simpl in Hk2. split; [eapply Hk2; lia|].
        eauto.
      }
      assert (∀ ka', (k1 + k2) ≤ ka' → i ∈ ((flatten ((tr2fun e ka').1)).2) j →
                                       (tr2fun e ka').2 ≠ j) as Hin_ns.
      {
        intros ka' Hle Hin' Heq.
          subst.
          destruct (flatten_trace_match a e ka') as (kb'&Hlt'&Hmatch'&l'&?&Hinblock'&Hoffset').
          specialize (Hinblock' i Hin'). destruct Hinblock' as [Hi_step | Hi_ne].
          * assert (In i (map snd l')) as Hi_step'.
            {
              clear -Hi_step. induction l'.
              - simpl in *. inversion Hi_step. 
              - inversion Hi_step; subst; auto. 
                * left; auto.
                * right. auto.
            }
            eapply in_map_iff in Hi_step'.
            destruct Hi_step' as ((b&i')&Heqi&Hin'').
            simpl in Heqi; subst i'.
            eapply In_nth_error in Hin'' as (n&Hnth_some).
            assert (flatten_trace_fun a e (kb' + n) = (b, i)) as Heqkbn.
            {
              specialize (Hoffset' n).
              rewrite Hnth_some in Hoffset'.
              assert (n < length l') as Hlt''.
              { eapply nth_error_Some. congruence. }
              specialize (Hoffset' Hlt''). inversion Hoffset'. auto.
            }
            eapply (Hk1 (kb' + n)); [lia|].
            unfold f. rewrite tr2fun_fun2tr. simpl. rewrite Heqkbn. auto.
          * destruct (flatten_trace_match a e (S ka')) as (kb''&Hl''&Hmatch''&_).
            eapply Hi_ne. rewrite Hmatch''. unfold f in Hk2. 
            specialize (Hk2 kb'').   
            rewrite tr2fun_fun2tr in Hk2. eapply Hk2. lia.
      }
      assert (∀ ka', (k1 + k2) ≤ ka' → enabled R1 ((tr2fun e ka').1) j ∧
                                       i ∈ ((flatten ((tr2fun e ka').1)).2) j) as Hen_in.
      {
        induction 1 as [| m Hle (IHen&IHin)].
        - split_and!; auto.
        - destruct (flatten_trace_match a e (S m)) as (kb'&Hlt'&Hmatch'&l'&?&Hinblock'&Hoffset').
          specialize (tr2fun_succ _ _ e m). intros Hsucc_step.
          assert (j ≠ (tr2fun e m).2) as IHns'. 
          {
            intros Heq. subst. eapply (Hin_ns m); auto.
          }
          generalize (flatten_spec_nonstep_mono _ _ _ Hsucc_step j IHns'); intros Hmono.
          split_and!.
          * eapply flatten_spec_enabled_reflect. split.
            ** rewrite Hmatch'. specialize (Hk2 kb'). unfold f in Hk2. rewrite tr2fun_fun2tr in Hk2. 
               simpl in Hk2. eapply Hk2.
               lia.
            ** set_solver.
          * set_solver.
      }

      assert (ea_enabled e j) as Hea.
      {
        eapply ea_enabled_equiv.
        unfold ea_enabled', al_enabled'.
        eapply (tr2fun_ev_al _ _ _ (λ p, enabled R1 (fst p) j)). 
        exists (k1+k2). intros k' Hgt. 
        edestruct Hen_in; eauto.
      }
      specialize (Hfair j Hea). 
      eapply ae_taken_equiv in Hfair.
      unfold ae_taken', ev_taken' in Hfair.
      rewrite (tr2fun_al_ev _ _ _ (λ p, j = snd p)) in Hfair.
      destruct (Hfair (k1 + k2)) as (kstep&Hgt&Hstep).
      eapply (Hin_ns kstep). 
      - lia.
      - eapply Hen_in. lia. 
      - auto.
    Qed.
    End block.
  
End cofair.
