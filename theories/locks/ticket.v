From fri.heap_lang Require Export lang notation.

(* Code is based on the ticket_lock found in upstream Iris contributed
   by Zhen Zhang. Slightly different because we have added primitive
   FAI to heap_lang. *)

Definition wait_loop: val :=
  rec: "wait_loop" "x" "lk" :=
    let: "o" := !(Fst "lk") in
    if: "x" = "o"
      then #() (* my turn *)
      else "wait_loop" "x" "lk".

Definition newlock : val := λ: <>, ((* owner *) ref #0, (* next *) ref #0).

Definition acquire : val :=
  λ: "lk",
    let: "n" := FAI (Snd "lk") in
    wait_loop "n" "lk".

Definition release : val := λ: "lk",
  (Fst "lk") <- !(Fst "lk") + #1.