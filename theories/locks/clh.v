From fri.heap_lang Require Export lang notation.

(* This is a version of the Craig-Landin-Hagersten (CLH) queue
   lock [1, 4].

   To motivate the CLH lock, recall how the ticket lock works. In the
   ticket lock, every thread waiting to acquire the lock is spinning
   on the same memory location: the "now serving" field.  While the lock
   is held, this location is probably cached. But, when the lock is
   released, every waiter's cache gets invalidated.

   In the CLH lock, rather than having a now serving counter, there is
   an implicit linked list connecting all of the threads waiting for
   the lock, in the order that they tried to acquire the lock. Each
   node in this linked list has a boolean field. If the field is true,
   then the thread in that position is either holding the lock or
   waiting for it.  If it is false, then the thread has released the
   lock. To acquire the lock, a thread adds a new node to the tail of
   the waiting queue list. It then looks at its predecessor's node,
   and spins waiting till that node's field indicates the predecessor
   has released the lock.  At that point, the waiting thread has
   acquired the lock and enters the critical section.

   To release the lock, the thread sets the field in its node to true
   to indicate it is done.

   Thus each waiting thread waits on a different location, and when a
   lock is released it only modifies one of these locations, so it
   should not invalidate other cache entries.

   There is one important difference between our implementation and
   typical versions. Usually, the API is designed so that the acquire
   method take two parameters: (1) the lock, and (2) the new node
   to be added to the list, while the release method takes only the releaser's node.

   This means it is not a drop-in replacement for the ticket-lock,
   because in the ticket lock, both acquire/release take the lock
   structure as their only parameter.

   To work around this, the lock structure has a pointer to the
   current "head" of the list, which is the node of the current lock
   holder.

   This is a little bit less efficient I think, but it seems to still
   be faithful to the underlying idea. (And, in some cases this extra
   "head" pointer is useful in its own regard [3]).

   [1] Travis S. Craig:
       Building FIFO and priority-queueing spin locks from atomic swap. 
       Technical Report TR 93-02-02, Univ. of Washington, Dept of Computer Science (1993).

   [2] Maurice Herlihy, Nir Shavit:
       The Art of Multiprocessor Programming, Revised 1st Ed.
       Morgan Kaufmann Publishers Inc (2012)

   [3] Doug Lea:
       The java.util.concurrent synchronizer framework. 
       Sci. Comput. Program. 58(3): 293-309 (2005)

   [4] Peter S. Magnusson, Anders Landin, Erik Hagersten:
       Queue Locks on Cache Coherent Multiprocessors. 
       IPPS 1994: 165-171
*)

Definition newlock : val := 
  λ: <>, 
       let: "dummy" := ref #false in
       ((* head *) ref "dummy", (* tail *) ref "dummy").

Definition wait_loop: val :=
  rec: "wait_loop" "me" "prev" "lk" :=
    let: "wait" := !"prev" in
    if: "wait"
      then "wait_loop" "me" "prev" "lk"
      else (Fst "lk") <- "me" (* my turn; update hd to prepare for release *).

Definition acquire : val :=
  λ: "lk",
    let: "me" := ref #true in
    let: "prev" := Swap (Snd "lk") "me" in
    wait_loop "me" "prev" "lk".

Definition release : val := λ: "lk",
  !(Fst "lk") <- #false.
