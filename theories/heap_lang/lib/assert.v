From iris.proofmode Require Import tactics.
From fri.heap_lang Require Export wp_tactics heap refine_heap notation proofmode.

Definition assert : val :=
  λ: "v", if: "v" #() then #() else #0 #0. (* #0 #0 is unsafe *)
(* just below ;; *)
Notation "'assert:' e" := (assert (λ: <>, e))%E (at level 99) : expr_scope.

Lemma wp_assert {Σ} E (Φ : val → iProp heap_lang Σ) e `{!Closed [] e} :
  (|={E}=>> |={E}=>> WP e @ E {{ v, ⧆(bi_pure (v = #true)) ★ ▷ |={E}=>> Φ #() }})
    ⊢ WP (assert: e) @ E {{ Φ }}.
Proof.
  iIntros "HΦ". wp_let. iPsvs "HΦ". iModIntro.
  wp_seq. iPsvs "HΦ". iModIntro.
  iApply wp_wand_r; iFrame "HΦ". iAlways. iIntros (v) "[% HΦ']"; subst.
  wp_if. iPsvs "HΦ'". iModIntro. wp_value; iModIntro. done.
Qed.

Global Opaque assert.